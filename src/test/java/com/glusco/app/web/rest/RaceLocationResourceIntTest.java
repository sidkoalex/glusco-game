package com.glusco.app.web.rest;

import com.glusco.app.GluscoApp;

import com.glusco.app.domain.RaceLocation;
import com.glusco.app.repository.RaceLocationRepository;
import com.glusco.app.service.RaceLocationService;
import com.glusco.app.service.dto.RaceLocationDTO;
import com.glusco.app.service.mapper.RaceLocationMapper;
import com.glusco.app.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;


import static com.glusco.app.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the RaceLocationResource REST controller.
 *
 * @see RaceLocationResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = GluscoApp.class)
public class RaceLocationResourceIntTest {

    private static final String DEFAULT_TYPE = "AAAAAAAAAA";
    private static final String UPDATED_TYPE = "BBBBBBBBBB";

    @Autowired
    private RaceLocationRepository raceLocationRepository;


    @Autowired
    private RaceLocationMapper raceLocationMapper;
    

    @Autowired
    private RaceLocationService raceLocationService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restRaceLocationMockMvc;

    private RaceLocation raceLocation;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final RaceLocationResource raceLocationResource = new RaceLocationResource(raceLocationService);
        this.restRaceLocationMockMvc = MockMvcBuilders.standaloneSetup(raceLocationResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static RaceLocation createEntity(EntityManager em) {
        RaceLocation raceLocation = new RaceLocation()
            .type(DEFAULT_TYPE);
        return raceLocation;
    }

    @Before
    public void initTest() {
        raceLocation = createEntity(em);
    }

    @Test
    @Transactional
    public void createRaceLocation() throws Exception {
        int databaseSizeBeforeCreate = raceLocationRepository.findAll().size();

        // Create the RaceLocation
        RaceLocationDTO raceLocationDTO = raceLocationMapper.toDto(raceLocation);
        restRaceLocationMockMvc.perform(post("/api/race-locations")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(raceLocationDTO)))
            .andExpect(status().isCreated());

        // Validate the RaceLocation in the database
        List<RaceLocation> raceLocationList = raceLocationRepository.findAll();
        assertThat(raceLocationList).hasSize(databaseSizeBeforeCreate + 1);
        RaceLocation testRaceLocation = raceLocationList.get(raceLocationList.size() - 1);
        assertThat(testRaceLocation.getType()).isEqualTo(DEFAULT_TYPE);
    }

    @Test
    @Transactional
    public void createRaceLocationWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = raceLocationRepository.findAll().size();

        // Create the RaceLocation with an existing ID
        raceLocation.setId(1L);
        RaceLocationDTO raceLocationDTO = raceLocationMapper.toDto(raceLocation);

        // An entity with an existing ID cannot be created, so this API call must fail
        restRaceLocationMockMvc.perform(post("/api/race-locations")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(raceLocationDTO)))
            .andExpect(status().isBadRequest());

        // Validate the RaceLocation in the database
        List<RaceLocation> raceLocationList = raceLocationRepository.findAll();
        assertThat(raceLocationList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkTypeIsRequired() throws Exception {
        int databaseSizeBeforeTest = raceLocationRepository.findAll().size();
        // set the field null
        raceLocation.setType(null);

        // Create the RaceLocation, which fails.
        RaceLocationDTO raceLocationDTO = raceLocationMapper.toDto(raceLocation);

        restRaceLocationMockMvc.perform(post("/api/race-locations")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(raceLocationDTO)))
            .andExpect(status().isBadRequest());

        List<RaceLocation> raceLocationList = raceLocationRepository.findAll();
        assertThat(raceLocationList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllRaceLocations() throws Exception {
        // Initialize the database
        raceLocationRepository.saveAndFlush(raceLocation);

        // Get all the raceLocationList
        restRaceLocationMockMvc.perform(get("/api/race-locations?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(raceLocation.getId().intValue())))
            .andExpect(jsonPath("$.[*].type").value(hasItem(DEFAULT_TYPE.toString())));
    }
    

    @Test
    @Transactional
    public void getRaceLocation() throws Exception {
        // Initialize the database
        raceLocationRepository.saveAndFlush(raceLocation);

        // Get the raceLocation
        restRaceLocationMockMvc.perform(get("/api/race-locations/{id}", raceLocation.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(raceLocation.getId().intValue()))
            .andExpect(jsonPath("$.type").value(DEFAULT_TYPE.toString()));
    }
    @Test
    @Transactional
    public void getNonExistingRaceLocation() throws Exception {
        // Get the raceLocation
        restRaceLocationMockMvc.perform(get("/api/race-locations/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateRaceLocation() throws Exception {
        // Initialize the database
        raceLocationRepository.saveAndFlush(raceLocation);

        int databaseSizeBeforeUpdate = raceLocationRepository.findAll().size();

        // Update the raceLocation
        RaceLocation updatedRaceLocation = raceLocationRepository.findById(raceLocation.getId()).get();
        // Disconnect from session so that the updates on updatedRaceLocation are not directly saved in db
        em.detach(updatedRaceLocation);
        updatedRaceLocation
            .type(UPDATED_TYPE);
        RaceLocationDTO raceLocationDTO = raceLocationMapper.toDto(updatedRaceLocation);

        restRaceLocationMockMvc.perform(put("/api/race-locations")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(raceLocationDTO)))
            .andExpect(status().isOk());

        // Validate the RaceLocation in the database
        List<RaceLocation> raceLocationList = raceLocationRepository.findAll();
        assertThat(raceLocationList).hasSize(databaseSizeBeforeUpdate);
        RaceLocation testRaceLocation = raceLocationList.get(raceLocationList.size() - 1);
        assertThat(testRaceLocation.getType()).isEqualTo(UPDATED_TYPE);
    }

    @Test
    @Transactional
    public void updateNonExistingRaceLocation() throws Exception {
        int databaseSizeBeforeUpdate = raceLocationRepository.findAll().size();

        // Create the RaceLocation
        RaceLocationDTO raceLocationDTO = raceLocationMapper.toDto(raceLocation);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException 
        restRaceLocationMockMvc.perform(put("/api/race-locations")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(raceLocationDTO)))
            .andExpect(status().isBadRequest());

        // Validate the RaceLocation in the database
        List<RaceLocation> raceLocationList = raceLocationRepository.findAll();
        assertThat(raceLocationList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteRaceLocation() throws Exception {
        // Initialize the database
        raceLocationRepository.saveAndFlush(raceLocation);

        int databaseSizeBeforeDelete = raceLocationRepository.findAll().size();

        // Get the raceLocation
        restRaceLocationMockMvc.perform(delete("/api/race-locations/{id}", raceLocation.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<RaceLocation> raceLocationList = raceLocationRepository.findAll();
        assertThat(raceLocationList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(RaceLocation.class);
        RaceLocation raceLocation1 = new RaceLocation();
        raceLocation1.setId(1L);
        RaceLocation raceLocation2 = new RaceLocation();
        raceLocation2.setId(raceLocation1.getId());
        assertThat(raceLocation1).isEqualTo(raceLocation2);
        raceLocation2.setId(2L);
        assertThat(raceLocation1).isNotEqualTo(raceLocation2);
        raceLocation1.setId(null);
        assertThat(raceLocation1).isNotEqualTo(raceLocation2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(RaceLocationDTO.class);
        RaceLocationDTO raceLocationDTO1 = new RaceLocationDTO();
        raceLocationDTO1.setId(1L);
        RaceLocationDTO raceLocationDTO2 = new RaceLocationDTO();
        assertThat(raceLocationDTO1).isNotEqualTo(raceLocationDTO2);
        raceLocationDTO2.setId(raceLocationDTO1.getId());
        assertThat(raceLocationDTO1).isEqualTo(raceLocationDTO2);
        raceLocationDTO2.setId(2L);
        assertThat(raceLocationDTO1).isNotEqualTo(raceLocationDTO2);
        raceLocationDTO1.setId(null);
        assertThat(raceLocationDTO1).isNotEqualTo(raceLocationDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(raceLocationMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(raceLocationMapper.fromId(null)).isNull();
    }
}
