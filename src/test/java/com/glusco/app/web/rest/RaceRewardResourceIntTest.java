package com.glusco.app.web.rest;

import com.glusco.app.GluscoApp;

import com.glusco.app.domain.RaceReward;
import com.glusco.app.domain.Race;
import com.glusco.app.domain.Reward;
import com.glusco.app.repository.RaceRewardRepository;
import com.glusco.app.service.RaceRewardService;
import com.glusco.app.service.dto.RaceRewardDTO;
import com.glusco.app.service.mapper.RaceRewardMapper;
import com.glusco.app.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;


import static com.glusco.app.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the RaceRewardResource REST controller.
 *
 * @see RaceRewardResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = GluscoApp.class)
public class RaceRewardResourceIntTest {

    private static final String DEFAULT_REWARD_QR = "AAAAAAAAAA";
    private static final String UPDATED_REWARD_QR = "BBBBBBBBBB";

    @Autowired
    private RaceRewardRepository raceRewardRepository;


    @Autowired
    private RaceRewardMapper raceRewardMapper;
    

    @Autowired
    private RaceRewardService raceRewardService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restRaceRewardMockMvc;

    private RaceReward raceReward;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final RaceRewardResource raceRewardResource = new RaceRewardResource(raceRewardService);
        this.restRaceRewardMockMvc = MockMvcBuilders.standaloneSetup(raceRewardResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static RaceReward createEntity(EntityManager em) {
        RaceReward raceReward = new RaceReward()
            .rewardQr(DEFAULT_REWARD_QR);
        // Add required entity
        Race race = RaceResourceIntTest.createEntity(em);
        em.persist(race);
        em.flush();
        raceReward.setRace(race);
        // Add required entity
        Reward reward = RewardResourceIntTest.createEntity(em);
        em.persist(reward);
        em.flush();
        raceReward.setReward(reward);
        return raceReward;
    }

    @Before
    public void initTest() {
        raceReward = createEntity(em);
    }

    @Test
    @Transactional
    public void createRaceReward() throws Exception {
        int databaseSizeBeforeCreate = raceRewardRepository.findAll().size();

        // Create the RaceReward
        RaceRewardDTO raceRewardDTO = raceRewardMapper.toDto(raceReward);
        restRaceRewardMockMvc.perform(post("/api/race-rewards")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(raceRewardDTO)))
            .andExpect(status().isCreated());

        // Validate the RaceReward in the database
        List<RaceReward> raceRewardList = raceRewardRepository.findAll();
        assertThat(raceRewardList).hasSize(databaseSizeBeforeCreate + 1);
        RaceReward testRaceReward = raceRewardList.get(raceRewardList.size() - 1);
        assertThat(testRaceReward.getRewardQr()).isEqualTo(DEFAULT_REWARD_QR);
    }

    @Test
    @Transactional
    public void createRaceRewardWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = raceRewardRepository.findAll().size();

        // Create the RaceReward with an existing ID
        raceReward.setId(1L);
        RaceRewardDTO raceRewardDTO = raceRewardMapper.toDto(raceReward);

        // An entity with an existing ID cannot be created, so this API call must fail
        restRaceRewardMockMvc.perform(post("/api/race-rewards")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(raceRewardDTO)))
            .andExpect(status().isBadRequest());

        // Validate the RaceReward in the database
        List<RaceReward> raceRewardList = raceRewardRepository.findAll();
        assertThat(raceRewardList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllRaceRewards() throws Exception {
        // Initialize the database
        raceRewardRepository.saveAndFlush(raceReward);

        // Get all the raceRewardList
        restRaceRewardMockMvc.perform(get("/api/race-rewards?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(raceReward.getId().intValue())))
            .andExpect(jsonPath("$.[*].rewardQr").value(hasItem(DEFAULT_REWARD_QR.toString())));
    }
    

    @Test
    @Transactional
    public void getRaceReward() throws Exception {
        // Initialize the database
        raceRewardRepository.saveAndFlush(raceReward);

        // Get the raceReward
        restRaceRewardMockMvc.perform(get("/api/race-rewards/{id}", raceReward.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(raceReward.getId().intValue()))
            .andExpect(jsonPath("$.rewardQr").value(DEFAULT_REWARD_QR.toString()));
    }
    @Test
    @Transactional
    public void getNonExistingRaceReward() throws Exception {
        // Get the raceReward
        restRaceRewardMockMvc.perform(get("/api/race-rewards/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateRaceReward() throws Exception {
        // Initialize the database
        raceRewardRepository.saveAndFlush(raceReward);

        int databaseSizeBeforeUpdate = raceRewardRepository.findAll().size();

        // Update the raceReward
        RaceReward updatedRaceReward = raceRewardRepository.findById(raceReward.getId()).get();
        // Disconnect from session so that the updates on updatedRaceReward are not directly saved in db
        em.detach(updatedRaceReward);
        updatedRaceReward
            .rewardQr(UPDATED_REWARD_QR);
        RaceRewardDTO raceRewardDTO = raceRewardMapper.toDto(updatedRaceReward);

        restRaceRewardMockMvc.perform(put("/api/race-rewards")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(raceRewardDTO)))
            .andExpect(status().isOk());

        // Validate the RaceReward in the database
        List<RaceReward> raceRewardList = raceRewardRepository.findAll();
        assertThat(raceRewardList).hasSize(databaseSizeBeforeUpdate);
        RaceReward testRaceReward = raceRewardList.get(raceRewardList.size() - 1);
        assertThat(testRaceReward.getRewardQr()).isEqualTo(UPDATED_REWARD_QR);
    }

    @Test
    @Transactional
    public void updateNonExistingRaceReward() throws Exception {
        int databaseSizeBeforeUpdate = raceRewardRepository.findAll().size();

        // Create the RaceReward
        RaceRewardDTO raceRewardDTO = raceRewardMapper.toDto(raceReward);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException 
        restRaceRewardMockMvc.perform(put("/api/race-rewards")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(raceRewardDTO)))
            .andExpect(status().isBadRequest());

        // Validate the RaceReward in the database
        List<RaceReward> raceRewardList = raceRewardRepository.findAll();
        assertThat(raceRewardList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteRaceReward() throws Exception {
        // Initialize the database
        raceRewardRepository.saveAndFlush(raceReward);

        int databaseSizeBeforeDelete = raceRewardRepository.findAll().size();

        // Get the raceReward
        restRaceRewardMockMvc.perform(delete("/api/race-rewards/{id}", raceReward.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<RaceReward> raceRewardList = raceRewardRepository.findAll();
        assertThat(raceRewardList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(RaceReward.class);
        RaceReward raceReward1 = new RaceReward();
        raceReward1.setId(1L);
        RaceReward raceReward2 = new RaceReward();
        raceReward2.setId(raceReward1.getId());
        assertThat(raceReward1).isEqualTo(raceReward2);
        raceReward2.setId(2L);
        assertThat(raceReward1).isNotEqualTo(raceReward2);
        raceReward1.setId(null);
        assertThat(raceReward1).isNotEqualTo(raceReward2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(RaceRewardDTO.class);
        RaceRewardDTO raceRewardDTO1 = new RaceRewardDTO();
        raceRewardDTO1.setId(1L);
        RaceRewardDTO raceRewardDTO2 = new RaceRewardDTO();
        assertThat(raceRewardDTO1).isNotEqualTo(raceRewardDTO2);
        raceRewardDTO2.setId(raceRewardDTO1.getId());
        assertThat(raceRewardDTO1).isEqualTo(raceRewardDTO2);
        raceRewardDTO2.setId(2L);
        assertThat(raceRewardDTO1).isNotEqualTo(raceRewardDTO2);
        raceRewardDTO1.setId(null);
        assertThat(raceRewardDTO1).isNotEqualTo(raceRewardDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(raceRewardMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(raceRewardMapper.fromId(null)).isNull();
    }
}
