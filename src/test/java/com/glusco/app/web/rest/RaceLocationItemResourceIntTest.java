package com.glusco.app.web.rest;

import com.glusco.app.GluscoApp;

import com.glusco.app.domain.RaceLocationItem;
import com.glusco.app.domain.RaceLocation;
import com.glusco.app.domain.RaceItem;
import com.glusco.app.repository.RaceLocationItemRepository;
import com.glusco.app.service.RaceLocationItemService;
import com.glusco.app.service.dto.RaceLocationItemDTO;
import com.glusco.app.service.mapper.RaceLocationItemMapper;
import com.glusco.app.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;


import static com.glusco.app.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the RaceLocationItemResource REST controller.
 *
 * @see RaceLocationItemResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = GluscoApp.class)
public class RaceLocationItemResourceIntTest {

    @Autowired
    private RaceLocationItemRepository raceLocationItemRepository;


    @Autowired
    private RaceLocationItemMapper raceLocationItemMapper;
    

    @Autowired
    private RaceLocationItemService raceLocationItemService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restRaceLocationItemMockMvc;

    private RaceLocationItem raceLocationItem;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final RaceLocationItemResource raceLocationItemResource = new RaceLocationItemResource(raceLocationItemService);
        this.restRaceLocationItemMockMvc = MockMvcBuilders.standaloneSetup(raceLocationItemResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static RaceLocationItem createEntity(EntityManager em) {
        RaceLocationItem raceLocationItem = new RaceLocationItem();
        // Add required entity
        RaceLocation raceLocation = RaceLocationResourceIntTest.createEntity(em);
        em.persist(raceLocation);
        em.flush();
        raceLocationItem.setRaceLocation(raceLocation);
        // Add required entity
        RaceItem raceItem = RaceItemResourceIntTest.createEntity(em);
        em.persist(raceItem);
        em.flush();
        raceLocationItem.setRaceItem(raceItem);
        return raceLocationItem;
    }

    @Before
    public void initTest() {
        raceLocationItem = createEntity(em);
    }

    @Test
    @Transactional
    public void createRaceLocationItem() throws Exception {
        int databaseSizeBeforeCreate = raceLocationItemRepository.findAll().size();

        // Create the RaceLocationItem
        RaceLocationItemDTO raceLocationItemDTO = raceLocationItemMapper.toDto(raceLocationItem);
        restRaceLocationItemMockMvc.perform(post("/api/race-location-items")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(raceLocationItemDTO)))
            .andExpect(status().isCreated());

        // Validate the RaceLocationItem in the database
        List<RaceLocationItem> raceLocationItemList = raceLocationItemRepository.findAll();
        assertThat(raceLocationItemList).hasSize(databaseSizeBeforeCreate + 1);
        RaceLocationItem testRaceLocationItem = raceLocationItemList.get(raceLocationItemList.size() - 1);
    }

    @Test
    @Transactional
    public void createRaceLocationItemWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = raceLocationItemRepository.findAll().size();

        // Create the RaceLocationItem with an existing ID
        raceLocationItem.setId(1L);
        RaceLocationItemDTO raceLocationItemDTO = raceLocationItemMapper.toDto(raceLocationItem);

        // An entity with an existing ID cannot be created, so this API call must fail
        restRaceLocationItemMockMvc.perform(post("/api/race-location-items")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(raceLocationItemDTO)))
            .andExpect(status().isBadRequest());

        // Validate the RaceLocationItem in the database
        List<RaceLocationItem> raceLocationItemList = raceLocationItemRepository.findAll();
        assertThat(raceLocationItemList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllRaceLocationItems() throws Exception {
        // Initialize the database
        raceLocationItemRepository.saveAndFlush(raceLocationItem);

        // Get all the raceLocationItemList
        restRaceLocationItemMockMvc.perform(get("/api/race-location-items?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(raceLocationItem.getId().intValue())));
    }
    

    @Test
    @Transactional
    public void getRaceLocationItem() throws Exception {
        // Initialize the database
        raceLocationItemRepository.saveAndFlush(raceLocationItem);

        // Get the raceLocationItem
        restRaceLocationItemMockMvc.perform(get("/api/race-location-items/{id}", raceLocationItem.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(raceLocationItem.getId().intValue()));
    }
    @Test
    @Transactional
    public void getNonExistingRaceLocationItem() throws Exception {
        // Get the raceLocationItem
        restRaceLocationItemMockMvc.perform(get("/api/race-location-items/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateRaceLocationItem() throws Exception {
        // Initialize the database
        raceLocationItemRepository.saveAndFlush(raceLocationItem);

        int databaseSizeBeforeUpdate = raceLocationItemRepository.findAll().size();

        // Update the raceLocationItem
        RaceLocationItem updatedRaceLocationItem = raceLocationItemRepository.findById(raceLocationItem.getId()).get();
        // Disconnect from session so that the updates on updatedRaceLocationItem are not directly saved in db
        em.detach(updatedRaceLocationItem);
        RaceLocationItemDTO raceLocationItemDTO = raceLocationItemMapper.toDto(updatedRaceLocationItem);

        restRaceLocationItemMockMvc.perform(put("/api/race-location-items")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(raceLocationItemDTO)))
            .andExpect(status().isOk());

        // Validate the RaceLocationItem in the database
        List<RaceLocationItem> raceLocationItemList = raceLocationItemRepository.findAll();
        assertThat(raceLocationItemList).hasSize(databaseSizeBeforeUpdate);
        RaceLocationItem testRaceLocationItem = raceLocationItemList.get(raceLocationItemList.size() - 1);
    }

    @Test
    @Transactional
    public void updateNonExistingRaceLocationItem() throws Exception {
        int databaseSizeBeforeUpdate = raceLocationItemRepository.findAll().size();

        // Create the RaceLocationItem
        RaceLocationItemDTO raceLocationItemDTO = raceLocationItemMapper.toDto(raceLocationItem);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException 
        restRaceLocationItemMockMvc.perform(put("/api/race-location-items")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(raceLocationItemDTO)))
            .andExpect(status().isBadRequest());

        // Validate the RaceLocationItem in the database
        List<RaceLocationItem> raceLocationItemList = raceLocationItemRepository.findAll();
        assertThat(raceLocationItemList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteRaceLocationItem() throws Exception {
        // Initialize the database
        raceLocationItemRepository.saveAndFlush(raceLocationItem);

        int databaseSizeBeforeDelete = raceLocationItemRepository.findAll().size();

        // Get the raceLocationItem
        restRaceLocationItemMockMvc.perform(delete("/api/race-location-items/{id}", raceLocationItem.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<RaceLocationItem> raceLocationItemList = raceLocationItemRepository.findAll();
        assertThat(raceLocationItemList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(RaceLocationItem.class);
        RaceLocationItem raceLocationItem1 = new RaceLocationItem();
        raceLocationItem1.setId(1L);
        RaceLocationItem raceLocationItem2 = new RaceLocationItem();
        raceLocationItem2.setId(raceLocationItem1.getId());
        assertThat(raceLocationItem1).isEqualTo(raceLocationItem2);
        raceLocationItem2.setId(2L);
        assertThat(raceLocationItem1).isNotEqualTo(raceLocationItem2);
        raceLocationItem1.setId(null);
        assertThat(raceLocationItem1).isNotEqualTo(raceLocationItem2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(RaceLocationItemDTO.class);
        RaceLocationItemDTO raceLocationItemDTO1 = new RaceLocationItemDTO();
        raceLocationItemDTO1.setId(1L);
        RaceLocationItemDTO raceLocationItemDTO2 = new RaceLocationItemDTO();
        assertThat(raceLocationItemDTO1).isNotEqualTo(raceLocationItemDTO2);
        raceLocationItemDTO2.setId(raceLocationItemDTO1.getId());
        assertThat(raceLocationItemDTO1).isEqualTo(raceLocationItemDTO2);
        raceLocationItemDTO2.setId(2L);
        assertThat(raceLocationItemDTO1).isNotEqualTo(raceLocationItemDTO2);
        raceLocationItemDTO1.setId(null);
        assertThat(raceLocationItemDTO1).isNotEqualTo(raceLocationItemDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(raceLocationItemMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(raceLocationItemMapper.fromId(null)).isNull();
    }
}
