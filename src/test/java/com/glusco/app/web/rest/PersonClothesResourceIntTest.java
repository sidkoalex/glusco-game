package com.glusco.app.web.rest;

import com.glusco.app.GluscoApp;

import com.glusco.app.domain.PersonClothes;
import com.glusco.app.domain.Person;
import com.glusco.app.domain.Clothes;
import com.glusco.app.repository.PersonClothesRepository;
import com.glusco.app.service.PersonClothesService;
import com.glusco.app.service.dto.PersonClothesDTO;
import com.glusco.app.service.mapper.PersonClothesMapper;
import com.glusco.app.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;


import static com.glusco.app.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the PersonClothesResource REST controller.
 *
 * @see PersonClothesResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = GluscoApp.class)
public class PersonClothesResourceIntTest {

    @Autowired
    private PersonClothesRepository personClothesRepository;


    @Autowired
    private PersonClothesMapper personClothesMapper;
    

    @Autowired
    private PersonClothesService personClothesService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restPersonClothesMockMvc;

    private PersonClothes personClothes;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final PersonClothesResource personClothesResource = new PersonClothesResource(personClothesService);
        this.restPersonClothesMockMvc = MockMvcBuilders.standaloneSetup(personClothesResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static PersonClothes createEntity(EntityManager em) {
        PersonClothes personClothes = new PersonClothes();
        // Add required entity
        Person person = PersonResourceIntTest.createEntity(em);
        em.persist(person);
        em.flush();
        personClothes.setPerson(person);
        // Add required entity
        Clothes clothes = ClothesResourceIntTest.createEntity(em);
        em.persist(clothes);
        em.flush();
        personClothes.setClothes(clothes);
        return personClothes;
    }

    @Before
    public void initTest() {
        personClothes = createEntity(em);
    }

    @Test
    @Transactional
    public void createPersonClothes() throws Exception {
        int databaseSizeBeforeCreate = personClothesRepository.findAll().size();

        // Create the PersonClothes
        PersonClothesDTO personClothesDTO = personClothesMapper.toDto(personClothes);
        restPersonClothesMockMvc.perform(post("/api/person-clothes")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(personClothesDTO)))
            .andExpect(status().isCreated());

        // Validate the PersonClothes in the database
        List<PersonClothes> personClothesList = personClothesRepository.findAll();
        assertThat(personClothesList).hasSize(databaseSizeBeforeCreate + 1);
        PersonClothes testPersonClothes = personClothesList.get(personClothesList.size() - 1);
    }

    @Test
    @Transactional
    public void createPersonClothesWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = personClothesRepository.findAll().size();

        // Create the PersonClothes with an existing ID
        personClothes.setId(1L);
        PersonClothesDTO personClothesDTO = personClothesMapper.toDto(personClothes);

        // An entity with an existing ID cannot be created, so this API call must fail
        restPersonClothesMockMvc.perform(post("/api/person-clothes")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(personClothesDTO)))
            .andExpect(status().isBadRequest());

        // Validate the PersonClothes in the database
        List<PersonClothes> personClothesList = personClothesRepository.findAll();
        assertThat(personClothesList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllPersonClothes() throws Exception {
        // Initialize the database
        personClothesRepository.saveAndFlush(personClothes);

        // Get all the personClothesList
        restPersonClothesMockMvc.perform(get("/api/person-clothes?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(personClothes.getId().intValue())));
    }
    

    @Test
    @Transactional
    public void getPersonClothes() throws Exception {
        // Initialize the database
        personClothesRepository.saveAndFlush(personClothes);

        // Get the personClothes
        restPersonClothesMockMvc.perform(get("/api/person-clothes/{id}", personClothes.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(personClothes.getId().intValue()));
    }
    @Test
    @Transactional
    public void getNonExistingPersonClothes() throws Exception {
        // Get the personClothes
        restPersonClothesMockMvc.perform(get("/api/person-clothes/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updatePersonClothes() throws Exception {
        // Initialize the database
        personClothesRepository.saveAndFlush(personClothes);

        int databaseSizeBeforeUpdate = personClothesRepository.findAll().size();

        // Update the personClothes
        PersonClothes updatedPersonClothes = personClothesRepository.findById(personClothes.getId()).get();
        // Disconnect from session so that the updates on updatedPersonClothes are not directly saved in db
        em.detach(updatedPersonClothes);
        PersonClothesDTO personClothesDTO = personClothesMapper.toDto(updatedPersonClothes);

        restPersonClothesMockMvc.perform(put("/api/person-clothes")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(personClothesDTO)))
            .andExpect(status().isOk());

        // Validate the PersonClothes in the database
        List<PersonClothes> personClothesList = personClothesRepository.findAll();
        assertThat(personClothesList).hasSize(databaseSizeBeforeUpdate);
        PersonClothes testPersonClothes = personClothesList.get(personClothesList.size() - 1);
    }

    @Test
    @Transactional
    public void updateNonExistingPersonClothes() throws Exception {
        int databaseSizeBeforeUpdate = personClothesRepository.findAll().size();

        // Create the PersonClothes
        PersonClothesDTO personClothesDTO = personClothesMapper.toDto(personClothes);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException 
        restPersonClothesMockMvc.perform(put("/api/person-clothes")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(personClothesDTO)))
            .andExpect(status().isBadRequest());

        // Validate the PersonClothes in the database
        List<PersonClothes> personClothesList = personClothesRepository.findAll();
        assertThat(personClothesList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deletePersonClothes() throws Exception {
        // Initialize the database
        personClothesRepository.saveAndFlush(personClothes);

        int databaseSizeBeforeDelete = personClothesRepository.findAll().size();

        // Get the personClothes
        restPersonClothesMockMvc.perform(delete("/api/person-clothes/{id}", personClothes.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<PersonClothes> personClothesList = personClothesRepository.findAll();
        assertThat(personClothesList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(PersonClothes.class);
        PersonClothes personClothes1 = new PersonClothes();
        personClothes1.setId(1L);
        PersonClothes personClothes2 = new PersonClothes();
        personClothes2.setId(personClothes1.getId());
        assertThat(personClothes1).isEqualTo(personClothes2);
        personClothes2.setId(2L);
        assertThat(personClothes1).isNotEqualTo(personClothes2);
        personClothes1.setId(null);
        assertThat(personClothes1).isNotEqualTo(personClothes2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(PersonClothesDTO.class);
        PersonClothesDTO personClothesDTO1 = new PersonClothesDTO();
        personClothesDTO1.setId(1L);
        PersonClothesDTO personClothesDTO2 = new PersonClothesDTO();
        assertThat(personClothesDTO1).isNotEqualTo(personClothesDTO2);
        personClothesDTO2.setId(personClothesDTO1.getId());
        assertThat(personClothesDTO1).isEqualTo(personClothesDTO2);
        personClothesDTO2.setId(2L);
        assertThat(personClothesDTO1).isNotEqualTo(personClothesDTO2);
        personClothesDTO1.setId(null);
        assertThat(personClothesDTO1).isNotEqualTo(personClothesDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(personClothesMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(personClothesMapper.fromId(null)).isNull();
    }
}
