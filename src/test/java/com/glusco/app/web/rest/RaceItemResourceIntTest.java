package com.glusco.app.web.rest;

import com.glusco.app.GluscoApp;

import com.glusco.app.domain.RaceItem;
import com.glusco.app.repository.RaceItemRepository;
import com.glusco.app.service.RaceItemService;
import com.glusco.app.service.dto.RaceItemDTO;
import com.glusco.app.service.mapper.RaceItemMapper;
import com.glusco.app.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;


import static com.glusco.app.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the RaceItemResource REST controller.
 *
 * @see RaceItemResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = GluscoApp.class)
public class RaceItemResourceIntTest {

    private static final String DEFAULT_TYPE = "AAAAAAAAAA";
    private static final String UPDATED_TYPE = "BBBBBBBBBB";

    @Autowired
    private RaceItemRepository raceItemRepository;


    @Autowired
    private RaceItemMapper raceItemMapper;
    

    @Autowired
    private RaceItemService raceItemService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restRaceItemMockMvc;

    private RaceItem raceItem;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final RaceItemResource raceItemResource = new RaceItemResource(raceItemService);
        this.restRaceItemMockMvc = MockMvcBuilders.standaloneSetup(raceItemResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static RaceItem createEntity(EntityManager em) {
        RaceItem raceItem = new RaceItem()
            .type(DEFAULT_TYPE);
        return raceItem;
    }

    @Before
    public void initTest() {
        raceItem = createEntity(em);
    }

    @Test
    @Transactional
    public void createRaceItem() throws Exception {
        int databaseSizeBeforeCreate = raceItemRepository.findAll().size();

        // Create the RaceItem
        RaceItemDTO raceItemDTO = raceItemMapper.toDto(raceItem);
        restRaceItemMockMvc.perform(post("/api/race-items")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(raceItemDTO)))
            .andExpect(status().isCreated());

        // Validate the RaceItem in the database
        List<RaceItem> raceItemList = raceItemRepository.findAll();
        assertThat(raceItemList).hasSize(databaseSizeBeforeCreate + 1);
        RaceItem testRaceItem = raceItemList.get(raceItemList.size() - 1);
        assertThat(testRaceItem.getType()).isEqualTo(DEFAULT_TYPE);
    }

    @Test
    @Transactional
    public void createRaceItemWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = raceItemRepository.findAll().size();

        // Create the RaceItem with an existing ID
        raceItem.setId(1L);
        RaceItemDTO raceItemDTO = raceItemMapper.toDto(raceItem);

        // An entity with an existing ID cannot be created, so this API call must fail
        restRaceItemMockMvc.perform(post("/api/race-items")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(raceItemDTO)))
            .andExpect(status().isBadRequest());

        // Validate the RaceItem in the database
        List<RaceItem> raceItemList = raceItemRepository.findAll();
        assertThat(raceItemList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkTypeIsRequired() throws Exception {
        int databaseSizeBeforeTest = raceItemRepository.findAll().size();
        // set the field null
        raceItem.setType(null);

        // Create the RaceItem, which fails.
        RaceItemDTO raceItemDTO = raceItemMapper.toDto(raceItem);

        restRaceItemMockMvc.perform(post("/api/race-items")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(raceItemDTO)))
            .andExpect(status().isBadRequest());

        List<RaceItem> raceItemList = raceItemRepository.findAll();
        assertThat(raceItemList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllRaceItems() throws Exception {
        // Initialize the database
        raceItemRepository.saveAndFlush(raceItem);

        // Get all the raceItemList
        restRaceItemMockMvc.perform(get("/api/race-items?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(raceItem.getId().intValue())))
            .andExpect(jsonPath("$.[*].type").value(hasItem(DEFAULT_TYPE.toString())));
    }
    

    @Test
    @Transactional
    public void getRaceItem() throws Exception {
        // Initialize the database
        raceItemRepository.saveAndFlush(raceItem);

        // Get the raceItem
        restRaceItemMockMvc.perform(get("/api/race-items/{id}", raceItem.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(raceItem.getId().intValue()))
            .andExpect(jsonPath("$.type").value(DEFAULT_TYPE.toString()));
    }
    @Test
    @Transactional
    public void getNonExistingRaceItem() throws Exception {
        // Get the raceItem
        restRaceItemMockMvc.perform(get("/api/race-items/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateRaceItem() throws Exception {
        // Initialize the database
        raceItemRepository.saveAndFlush(raceItem);

        int databaseSizeBeforeUpdate = raceItemRepository.findAll().size();

        // Update the raceItem
        RaceItem updatedRaceItem = raceItemRepository.findById(raceItem.getId()).get();
        // Disconnect from session so that the updates on updatedRaceItem are not directly saved in db
        em.detach(updatedRaceItem);
        updatedRaceItem
            .type(UPDATED_TYPE);
        RaceItemDTO raceItemDTO = raceItemMapper.toDto(updatedRaceItem);

        restRaceItemMockMvc.perform(put("/api/race-items")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(raceItemDTO)))
            .andExpect(status().isOk());

        // Validate the RaceItem in the database
        List<RaceItem> raceItemList = raceItemRepository.findAll();
        assertThat(raceItemList).hasSize(databaseSizeBeforeUpdate);
        RaceItem testRaceItem = raceItemList.get(raceItemList.size() - 1);
        assertThat(testRaceItem.getType()).isEqualTo(UPDATED_TYPE);
    }

    @Test
    @Transactional
    public void updateNonExistingRaceItem() throws Exception {
        int databaseSizeBeforeUpdate = raceItemRepository.findAll().size();

        // Create the RaceItem
        RaceItemDTO raceItemDTO = raceItemMapper.toDto(raceItem);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException 
        restRaceItemMockMvc.perform(put("/api/race-items")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(raceItemDTO)))
            .andExpect(status().isBadRequest());

        // Validate the RaceItem in the database
        List<RaceItem> raceItemList = raceItemRepository.findAll();
        assertThat(raceItemList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteRaceItem() throws Exception {
        // Initialize the database
        raceItemRepository.saveAndFlush(raceItem);

        int databaseSizeBeforeDelete = raceItemRepository.findAll().size();

        // Get the raceItem
        restRaceItemMockMvc.perform(delete("/api/race-items/{id}", raceItem.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<RaceItem> raceItemList = raceItemRepository.findAll();
        assertThat(raceItemList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(RaceItem.class);
        RaceItem raceItem1 = new RaceItem();
        raceItem1.setId(1L);
        RaceItem raceItem2 = new RaceItem();
        raceItem2.setId(raceItem1.getId());
        assertThat(raceItem1).isEqualTo(raceItem2);
        raceItem2.setId(2L);
        assertThat(raceItem1).isNotEqualTo(raceItem2);
        raceItem1.setId(null);
        assertThat(raceItem1).isNotEqualTo(raceItem2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(RaceItemDTO.class);
        RaceItemDTO raceItemDTO1 = new RaceItemDTO();
        raceItemDTO1.setId(1L);
        RaceItemDTO raceItemDTO2 = new RaceItemDTO();
        assertThat(raceItemDTO1).isNotEqualTo(raceItemDTO2);
        raceItemDTO2.setId(raceItemDTO1.getId());
        assertThat(raceItemDTO1).isEqualTo(raceItemDTO2);
        raceItemDTO2.setId(2L);
        assertThat(raceItemDTO1).isNotEqualTo(raceItemDTO2);
        raceItemDTO1.setId(null);
        assertThat(raceItemDTO1).isNotEqualTo(raceItemDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(raceItemMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(raceItemMapper.fromId(null)).isNull();
    }
}
