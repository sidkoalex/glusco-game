package com.glusco.app.web.rest;

import com.glusco.app.GluscoApp;

import com.glusco.app.domain.PersonCar;
import com.glusco.app.domain.Person;
import com.glusco.app.domain.Car;
import com.glusco.app.repository.PersonCarRepository;
import com.glusco.app.service.PersonCarService;
import com.glusco.app.service.dto.PersonCarDTO;
import com.glusco.app.service.mapper.PersonCarMapper;
import com.glusco.app.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;


import static com.glusco.app.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the PersonCarResource REST controller.
 *
 * @see PersonCarResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = GluscoApp.class)
public class PersonCarResourceIntTest {

    @Autowired
    private PersonCarRepository personCarRepository;


    @Autowired
    private PersonCarMapper personCarMapper;
    

    @Autowired
    private PersonCarService personCarService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restPersonCarMockMvc;

    private PersonCar personCar;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final PersonCarResource personCarResource = new PersonCarResource(personCarService);
        this.restPersonCarMockMvc = MockMvcBuilders.standaloneSetup(personCarResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static PersonCar createEntity(EntityManager em) {
        PersonCar personCar = new PersonCar();
        // Add required entity
        Person person = PersonResourceIntTest.createEntity(em);
        em.persist(person);
        em.flush();
        personCar.setPerson(person);
        // Add required entity
        Car car = CarResourceIntTest.createEntity(em);
        em.persist(car);
        em.flush();
        personCar.setCar(car);
        return personCar;
    }

    @Before
    public void initTest() {
        personCar = createEntity(em);
    }

    @Test
    @Transactional
    public void createPersonCar() throws Exception {
        int databaseSizeBeforeCreate = personCarRepository.findAll().size();

        // Create the PersonCar
        PersonCarDTO personCarDTO = personCarMapper.toDto(personCar);
        restPersonCarMockMvc.perform(post("/api/person-cars")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(personCarDTO)))
            .andExpect(status().isCreated());

        // Validate the PersonCar in the database
        List<PersonCar> personCarList = personCarRepository.findAll();
        assertThat(personCarList).hasSize(databaseSizeBeforeCreate + 1);
        PersonCar testPersonCar = personCarList.get(personCarList.size() - 1);
    }

    @Test
    @Transactional
    public void createPersonCarWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = personCarRepository.findAll().size();

        // Create the PersonCar with an existing ID
        personCar.setId(1L);
        PersonCarDTO personCarDTO = personCarMapper.toDto(personCar);

        // An entity with an existing ID cannot be created, so this API call must fail
        restPersonCarMockMvc.perform(post("/api/person-cars")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(personCarDTO)))
            .andExpect(status().isBadRequest());

        // Validate the PersonCar in the database
        List<PersonCar> personCarList = personCarRepository.findAll();
        assertThat(personCarList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllPersonCars() throws Exception {
        // Initialize the database
        personCarRepository.saveAndFlush(personCar);

        // Get all the personCarList
        restPersonCarMockMvc.perform(get("/api/person-cars?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(personCar.getId().intValue())));
    }
    

    @Test
    @Transactional
    public void getPersonCar() throws Exception {
        // Initialize the database
        personCarRepository.saveAndFlush(personCar);

        // Get the personCar
        restPersonCarMockMvc.perform(get("/api/person-cars/{id}", personCar.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(personCar.getId().intValue()));
    }
    @Test
    @Transactional
    public void getNonExistingPersonCar() throws Exception {
        // Get the personCar
        restPersonCarMockMvc.perform(get("/api/person-cars/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updatePersonCar() throws Exception {
        // Initialize the database
        personCarRepository.saveAndFlush(personCar);

        int databaseSizeBeforeUpdate = personCarRepository.findAll().size();

        // Update the personCar
        PersonCar updatedPersonCar = personCarRepository.findById(personCar.getId()).get();
        // Disconnect from session so that the updates on updatedPersonCar are not directly saved in db
        em.detach(updatedPersonCar);
        PersonCarDTO personCarDTO = personCarMapper.toDto(updatedPersonCar);

        restPersonCarMockMvc.perform(put("/api/person-cars")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(personCarDTO)))
            .andExpect(status().isOk());

        // Validate the PersonCar in the database
        List<PersonCar> personCarList = personCarRepository.findAll();
        assertThat(personCarList).hasSize(databaseSizeBeforeUpdate);
        PersonCar testPersonCar = personCarList.get(personCarList.size() - 1);
    }

    @Test
    @Transactional
    public void updateNonExistingPersonCar() throws Exception {
        int databaseSizeBeforeUpdate = personCarRepository.findAll().size();

        // Create the PersonCar
        PersonCarDTO personCarDTO = personCarMapper.toDto(personCar);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException 
        restPersonCarMockMvc.perform(put("/api/person-cars")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(personCarDTO)))
            .andExpect(status().isBadRequest());

        // Validate the PersonCar in the database
        List<PersonCar> personCarList = personCarRepository.findAll();
        assertThat(personCarList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deletePersonCar() throws Exception {
        // Initialize the database
        personCarRepository.saveAndFlush(personCar);

        int databaseSizeBeforeDelete = personCarRepository.findAll().size();

        // Get the personCar
        restPersonCarMockMvc.perform(delete("/api/person-cars/{id}", personCar.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<PersonCar> personCarList = personCarRepository.findAll();
        assertThat(personCarList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(PersonCar.class);
        PersonCar personCar1 = new PersonCar();
        personCar1.setId(1L);
        PersonCar personCar2 = new PersonCar();
        personCar2.setId(personCar1.getId());
        assertThat(personCar1).isEqualTo(personCar2);
        personCar2.setId(2L);
        assertThat(personCar1).isNotEqualTo(personCar2);
        personCar1.setId(null);
        assertThat(personCar1).isNotEqualTo(personCar2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(PersonCarDTO.class);
        PersonCarDTO personCarDTO1 = new PersonCarDTO();
        personCarDTO1.setId(1L);
        PersonCarDTO personCarDTO2 = new PersonCarDTO();
        assertThat(personCarDTO1).isNotEqualTo(personCarDTO2);
        personCarDTO2.setId(personCarDTO1.getId());
        assertThat(personCarDTO1).isEqualTo(personCarDTO2);
        personCarDTO2.setId(2L);
        assertThat(personCarDTO1).isNotEqualTo(personCarDTO2);
        personCarDTO1.setId(null);
        assertThat(personCarDTO1).isNotEqualTo(personCarDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(personCarMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(personCarMapper.fromId(null)).isNull();
    }
}
