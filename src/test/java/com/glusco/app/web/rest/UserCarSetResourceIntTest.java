package com.glusco.app.web.rest;

import com.glusco.app.GluscoApp;

import com.glusco.app.domain.UserCarSet;
import com.glusco.app.domain.Car;
import com.glusco.app.domain.UserPersonSet;
import com.glusco.app.repository.UserCarSetRepository;
import com.glusco.app.service.UserCarSetService;
import com.glusco.app.service.dto.UserCarSetDTO;
import com.glusco.app.service.mapper.UserCarSetMapper;
import com.glusco.app.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;


import static com.glusco.app.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the UserCarSetResource REST controller.
 *
 * @see UserCarSetResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = GluscoApp.class)
public class UserCarSetResourceIntTest {

    private static final String DEFAULT_COLOR = "AAAAAAAAAA";
    private static final String UPDATED_COLOR = "BBBBBBBBBB";

    @Autowired
    private UserCarSetRepository userCarSetRepository;


    @Autowired
    private UserCarSetMapper userCarSetMapper;
    

    @Autowired
    private UserCarSetService userCarSetService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restUserCarSetMockMvc;

    private UserCarSet userCarSet;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final UserCarSetResource userCarSetResource = new UserCarSetResource(userCarSetService);
        this.restUserCarSetMockMvc = MockMvcBuilders.standaloneSetup(userCarSetResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static UserCarSet createEntity(EntityManager em) {
        UserCarSet userCarSet = new UserCarSet()
            .color(DEFAULT_COLOR);
        // Add required entity
        Car car = CarResourceIntTest.createEntity(em);
        em.persist(car);
        em.flush();
        userCarSet.setCar(car);
        // Add required entity
        UserPersonSet userPersonSet = UserPersonSetResourceIntTest.createEntity(em);
        em.persist(userPersonSet);
        em.flush();
        userCarSet.setUserPersonSet(userPersonSet);
        return userCarSet;
    }

    @Before
    public void initTest() {
        userCarSet = createEntity(em);
    }

    @Test
    @Transactional
    public void createUserCarSet() throws Exception {
        int databaseSizeBeforeCreate = userCarSetRepository.findAll().size();

        // Create the UserCarSet
        UserCarSetDTO userCarSetDTO = userCarSetMapper.toDto(userCarSet);
        restUserCarSetMockMvc.perform(post("/api/user-car-sets")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(userCarSetDTO)))
            .andExpect(status().isCreated());

        // Validate the UserCarSet in the database
        List<UserCarSet> userCarSetList = userCarSetRepository.findAll();
        assertThat(userCarSetList).hasSize(databaseSizeBeforeCreate + 1);
        UserCarSet testUserCarSet = userCarSetList.get(userCarSetList.size() - 1);
        assertThat(testUserCarSet.getColor()).isEqualTo(DEFAULT_COLOR);
    }

    @Test
    @Transactional
    public void createUserCarSetWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = userCarSetRepository.findAll().size();

        // Create the UserCarSet with an existing ID
        userCarSet.setId(1L);
        UserCarSetDTO userCarSetDTO = userCarSetMapper.toDto(userCarSet);

        // An entity with an existing ID cannot be created, so this API call must fail
        restUserCarSetMockMvc.perform(post("/api/user-car-sets")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(userCarSetDTO)))
            .andExpect(status().isBadRequest());

        // Validate the UserCarSet in the database
        List<UserCarSet> userCarSetList = userCarSetRepository.findAll();
        assertThat(userCarSetList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllUserCarSets() throws Exception {
        // Initialize the database
        userCarSetRepository.saveAndFlush(userCarSet);

        // Get all the userCarSetList
        restUserCarSetMockMvc.perform(get("/api/user-car-sets?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(userCarSet.getId().intValue())))
            .andExpect(jsonPath("$.[*].color").value(hasItem(DEFAULT_COLOR.toString())));
    }
    

    @Test
    @Transactional
    public void getUserCarSet() throws Exception {
        // Initialize the database
        userCarSetRepository.saveAndFlush(userCarSet);

        // Get the userCarSet
        restUserCarSetMockMvc.perform(get("/api/user-car-sets/{id}", userCarSet.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(userCarSet.getId().intValue()))
            .andExpect(jsonPath("$.color").value(DEFAULT_COLOR.toString()));
    }
    @Test
    @Transactional
    public void getNonExistingUserCarSet() throws Exception {
        // Get the userCarSet
        restUserCarSetMockMvc.perform(get("/api/user-car-sets/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateUserCarSet() throws Exception {
        // Initialize the database
        userCarSetRepository.saveAndFlush(userCarSet);

        int databaseSizeBeforeUpdate = userCarSetRepository.findAll().size();

        // Update the userCarSet
        UserCarSet updatedUserCarSet = userCarSetRepository.findById(userCarSet.getId()).get();
        // Disconnect from session so that the updates on updatedUserCarSet are not directly saved in db
        em.detach(updatedUserCarSet);
        updatedUserCarSet
            .color(UPDATED_COLOR);
        UserCarSetDTO userCarSetDTO = userCarSetMapper.toDto(updatedUserCarSet);

        restUserCarSetMockMvc.perform(put("/api/user-car-sets")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(userCarSetDTO)))
            .andExpect(status().isOk());

        // Validate the UserCarSet in the database
        List<UserCarSet> userCarSetList = userCarSetRepository.findAll();
        assertThat(userCarSetList).hasSize(databaseSizeBeforeUpdate);
        UserCarSet testUserCarSet = userCarSetList.get(userCarSetList.size() - 1);
        assertThat(testUserCarSet.getColor()).isEqualTo(UPDATED_COLOR);
    }

    @Test
    @Transactional
    public void updateNonExistingUserCarSet() throws Exception {
        int databaseSizeBeforeUpdate = userCarSetRepository.findAll().size();

        // Create the UserCarSet
        UserCarSetDTO userCarSetDTO = userCarSetMapper.toDto(userCarSet);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException 
        restUserCarSetMockMvc.perform(put("/api/user-car-sets")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(userCarSetDTO)))
            .andExpect(status().isBadRequest());

        // Validate the UserCarSet in the database
        List<UserCarSet> userCarSetList = userCarSetRepository.findAll();
        assertThat(userCarSetList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteUserCarSet() throws Exception {
        // Initialize the database
        userCarSetRepository.saveAndFlush(userCarSet);

        int databaseSizeBeforeDelete = userCarSetRepository.findAll().size();

        // Get the userCarSet
        restUserCarSetMockMvc.perform(delete("/api/user-car-sets/{id}", userCarSet.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<UserCarSet> userCarSetList = userCarSetRepository.findAll();
        assertThat(userCarSetList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(UserCarSet.class);
        UserCarSet userCarSet1 = new UserCarSet();
        userCarSet1.setId(1L);
        UserCarSet userCarSet2 = new UserCarSet();
        userCarSet2.setId(userCarSet1.getId());
        assertThat(userCarSet1).isEqualTo(userCarSet2);
        userCarSet2.setId(2L);
        assertThat(userCarSet1).isNotEqualTo(userCarSet2);
        userCarSet1.setId(null);
        assertThat(userCarSet1).isNotEqualTo(userCarSet2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(UserCarSetDTO.class);
        UserCarSetDTO userCarSetDTO1 = new UserCarSetDTO();
        userCarSetDTO1.setId(1L);
        UserCarSetDTO userCarSetDTO2 = new UserCarSetDTO();
        assertThat(userCarSetDTO1).isNotEqualTo(userCarSetDTO2);
        userCarSetDTO2.setId(userCarSetDTO1.getId());
        assertThat(userCarSetDTO1).isEqualTo(userCarSetDTO2);
        userCarSetDTO2.setId(2L);
        assertThat(userCarSetDTO1).isNotEqualTo(userCarSetDTO2);
        userCarSetDTO1.setId(null);
        assertThat(userCarSetDTO1).isNotEqualTo(userCarSetDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(userCarSetMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(userCarSetMapper.fromId(null)).isNull();
    }
}
