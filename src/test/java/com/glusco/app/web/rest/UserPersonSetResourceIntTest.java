package com.glusco.app.web.rest;

import com.glusco.app.GluscoApp;

import com.glusco.app.domain.UserPersonSet;
import com.glusco.app.domain.User;
import com.glusco.app.domain.Person;
import com.glusco.app.domain.Clothes;
import com.glusco.app.repository.UserPersonSetRepository;
import com.glusco.app.service.UserPersonSetService;
import com.glusco.app.service.dto.UserPersonSetDTO;
import com.glusco.app.service.mapper.UserPersonSetMapper;
import com.glusco.app.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;


import static com.glusco.app.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the UserPersonSetResource REST controller.
 *
 * @see UserPersonSetResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = GluscoApp.class)
public class UserPersonSetResourceIntTest {

    @Autowired
    private UserPersonSetRepository userPersonSetRepository;


    @Autowired
    private UserPersonSetMapper userPersonSetMapper;
    

    @Autowired
    private UserPersonSetService userPersonSetService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restUserPersonSetMockMvc;

    private UserPersonSet userPersonSet;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final UserPersonSetResource userPersonSetResource = new UserPersonSetResource(userPersonSetService);
        this.restUserPersonSetMockMvc = MockMvcBuilders.standaloneSetup(userPersonSetResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static UserPersonSet createEntity(EntityManager em) {
        UserPersonSet userPersonSet = new UserPersonSet();
        // Add required entity
        User user = UserResourceIntTest.createEntity(em);
        em.persist(user);
        em.flush();
        userPersonSet.setUser(user);
        // Add required entity
        Person person = PersonResourceIntTest.createEntity(em);
        em.persist(person);
        em.flush();
        userPersonSet.setPerson(person);
        // Add required entity
        Clothes clothes = ClothesResourceIntTest.createEntity(em);
        em.persist(clothes);
        em.flush();
        userPersonSet.setClothes(clothes);
        return userPersonSet;
    }

    @Before
    public void initTest() {
        userPersonSet = createEntity(em);
    }

    @Test
    @Transactional
    public void createUserPersonSet() throws Exception {
        int databaseSizeBeforeCreate = userPersonSetRepository.findAll().size();

        // Create the UserPersonSet
        UserPersonSetDTO userPersonSetDTO = userPersonSetMapper.toDto(userPersonSet);
        restUserPersonSetMockMvc.perform(post("/api/user-person-sets")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(userPersonSetDTO)))
            .andExpect(status().isCreated());

        // Validate the UserPersonSet in the database
        List<UserPersonSet> userPersonSetList = userPersonSetRepository.findAll();
        assertThat(userPersonSetList).hasSize(databaseSizeBeforeCreate + 1);
        UserPersonSet testUserPersonSet = userPersonSetList.get(userPersonSetList.size() - 1);
    }

    @Test
    @Transactional
    public void createUserPersonSetWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = userPersonSetRepository.findAll().size();

        // Create the UserPersonSet with an existing ID
        userPersonSet.setId(1L);
        UserPersonSetDTO userPersonSetDTO = userPersonSetMapper.toDto(userPersonSet);

        // An entity with an existing ID cannot be created, so this API call must fail
        restUserPersonSetMockMvc.perform(post("/api/user-person-sets")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(userPersonSetDTO)))
            .andExpect(status().isBadRequest());

        // Validate the UserPersonSet in the database
        List<UserPersonSet> userPersonSetList = userPersonSetRepository.findAll();
        assertThat(userPersonSetList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllUserPersonSets() throws Exception {
        // Initialize the database
        userPersonSetRepository.saveAndFlush(userPersonSet);

        // Get all the userPersonSetList
        restUserPersonSetMockMvc.perform(get("/api/user-person-sets?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(userPersonSet.getId().intValue())));
    }
    

    @Test
    @Transactional
    public void getUserPersonSet() throws Exception {
        // Initialize the database
        userPersonSetRepository.saveAndFlush(userPersonSet);

        // Get the userPersonSet
        restUserPersonSetMockMvc.perform(get("/api/user-person-sets/{id}", userPersonSet.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(userPersonSet.getId().intValue()));
    }
    @Test
    @Transactional
    public void getNonExistingUserPersonSet() throws Exception {
        // Get the userPersonSet
        restUserPersonSetMockMvc.perform(get("/api/user-person-sets/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateUserPersonSet() throws Exception {
        // Initialize the database
        userPersonSetRepository.saveAndFlush(userPersonSet);

        int databaseSizeBeforeUpdate = userPersonSetRepository.findAll().size();

        // Update the userPersonSet
        UserPersonSet updatedUserPersonSet = userPersonSetRepository.findById(userPersonSet.getId()).get();
        // Disconnect from session so that the updates on updatedUserPersonSet are not directly saved in db
        em.detach(updatedUserPersonSet);
        UserPersonSetDTO userPersonSetDTO = userPersonSetMapper.toDto(updatedUserPersonSet);

        restUserPersonSetMockMvc.perform(put("/api/user-person-sets")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(userPersonSetDTO)))
            .andExpect(status().isOk());

        // Validate the UserPersonSet in the database
        List<UserPersonSet> userPersonSetList = userPersonSetRepository.findAll();
        assertThat(userPersonSetList).hasSize(databaseSizeBeforeUpdate);
        UserPersonSet testUserPersonSet = userPersonSetList.get(userPersonSetList.size() - 1);
    }

    @Test
    @Transactional
    public void updateNonExistingUserPersonSet() throws Exception {
        int databaseSizeBeforeUpdate = userPersonSetRepository.findAll().size();

        // Create the UserPersonSet
        UserPersonSetDTO userPersonSetDTO = userPersonSetMapper.toDto(userPersonSet);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException 
        restUserPersonSetMockMvc.perform(put("/api/user-person-sets")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(userPersonSetDTO)))
            .andExpect(status().isBadRequest());

        // Validate the UserPersonSet in the database
        List<UserPersonSet> userPersonSetList = userPersonSetRepository.findAll();
        assertThat(userPersonSetList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteUserPersonSet() throws Exception {
        // Initialize the database
        userPersonSetRepository.saveAndFlush(userPersonSet);

        int databaseSizeBeforeDelete = userPersonSetRepository.findAll().size();

        // Get the userPersonSet
        restUserPersonSetMockMvc.perform(delete("/api/user-person-sets/{id}", userPersonSet.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<UserPersonSet> userPersonSetList = userPersonSetRepository.findAll();
        assertThat(userPersonSetList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(UserPersonSet.class);
        UserPersonSet userPersonSet1 = new UserPersonSet();
        userPersonSet1.setId(1L);
        UserPersonSet userPersonSet2 = new UserPersonSet();
        userPersonSet2.setId(userPersonSet1.getId());
        assertThat(userPersonSet1).isEqualTo(userPersonSet2);
        userPersonSet2.setId(2L);
        assertThat(userPersonSet1).isNotEqualTo(userPersonSet2);
        userPersonSet1.setId(null);
        assertThat(userPersonSet1).isNotEqualTo(userPersonSet2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(UserPersonSetDTO.class);
        UserPersonSetDTO userPersonSetDTO1 = new UserPersonSetDTO();
        userPersonSetDTO1.setId(1L);
        UserPersonSetDTO userPersonSetDTO2 = new UserPersonSetDTO();
        assertThat(userPersonSetDTO1).isNotEqualTo(userPersonSetDTO2);
        userPersonSetDTO2.setId(userPersonSetDTO1.getId());
        assertThat(userPersonSetDTO1).isEqualTo(userPersonSetDTO2);
        userPersonSetDTO2.setId(2L);
        assertThat(userPersonSetDTO1).isNotEqualTo(userPersonSetDTO2);
        userPersonSetDTO1.setId(null);
        assertThat(userPersonSetDTO1).isNotEqualTo(userPersonSetDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(userPersonSetMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(userPersonSetMapper.fromId(null)).isNull();
    }
}
