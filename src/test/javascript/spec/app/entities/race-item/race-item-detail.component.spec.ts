/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { GluscoTestModule } from '../../../test.module';
import { RaceItemDetailComponent } from 'app/entities/race-item/race-item-detail.component';
import { RaceItem } from 'app/shared/model/race-item.model';

describe('Component Tests', () => {
    describe('RaceItem Management Detail Component', () => {
        let comp: RaceItemDetailComponent;
        let fixture: ComponentFixture<RaceItemDetailComponent>;
        const route = ({ data: of({ raceItem: new RaceItem(123) }) } as any) as ActivatedRoute;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [GluscoTestModule],
                declarations: [RaceItemDetailComponent],
                providers: [{ provide: ActivatedRoute, useValue: route }]
            })
                .overrideTemplate(RaceItemDetailComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(RaceItemDetailComponent);
            comp = fixture.componentInstance;
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(comp.raceItem).toEqual(jasmine.objectContaining({ id: 123 }));
            });
        });
    });
});
