/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable, of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { GluscoTestModule } from '../../../test.module';
import { RaceItemDeleteDialogComponent } from 'app/entities/race-item/race-item-delete-dialog.component';
import { RaceItemService } from 'app/entities/race-item/race-item.service';

describe('Component Tests', () => {
    describe('RaceItem Management Delete Component', () => {
        let comp: RaceItemDeleteDialogComponent;
        let fixture: ComponentFixture<RaceItemDeleteDialogComponent>;
        let service: RaceItemService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [GluscoTestModule],
                declarations: [RaceItemDeleteDialogComponent]
            })
                .overrideTemplate(RaceItemDeleteDialogComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(RaceItemDeleteDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(RaceItemService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('confirmDelete', () => {
            it('Should call delete service on confirmDelete', inject(
                [],
                fakeAsync(() => {
                    // GIVEN
                    spyOn(service, 'delete').and.returnValue(of({}));

                    // WHEN
                    comp.confirmDelete(123);
                    tick();

                    // THEN
                    expect(service.delete).toHaveBeenCalledWith(123);
                    expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                    expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
                })
            ));
        });
    });
});
