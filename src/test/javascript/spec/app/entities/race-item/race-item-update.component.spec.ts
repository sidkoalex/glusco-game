/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';

import { GluscoTestModule } from '../../../test.module';
import { RaceItemUpdateComponent } from 'app/entities/race-item/race-item-update.component';
import { RaceItemService } from 'app/entities/race-item/race-item.service';
import { RaceItem } from 'app/shared/model/race-item.model';

describe('Component Tests', () => {
    describe('RaceItem Management Update Component', () => {
        let comp: RaceItemUpdateComponent;
        let fixture: ComponentFixture<RaceItemUpdateComponent>;
        let service: RaceItemService;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [GluscoTestModule],
                declarations: [RaceItemUpdateComponent]
            })
                .overrideTemplate(RaceItemUpdateComponent, '')
                .compileComponents();

            fixture = TestBed.createComponent(RaceItemUpdateComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(RaceItemService);
        });

        describe('save', () => {
            it(
                'Should call update service on save for existing entity',
                fakeAsync(() => {
                    // GIVEN
                    const entity = new RaceItem(123);
                    spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
                    comp.raceItem = entity;
                    // WHEN
                    comp.save();
                    tick(); // simulate async

                    // THEN
                    expect(service.update).toHaveBeenCalledWith(entity);
                    expect(comp.isSaving).toEqual(false);
                })
            );

            it(
                'Should call create service on save for new entity',
                fakeAsync(() => {
                    // GIVEN
                    const entity = new RaceItem();
                    spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
                    comp.raceItem = entity;
                    // WHEN
                    comp.save();
                    tick(); // simulate async

                    // THEN
                    expect(service.create).toHaveBeenCalledWith(entity);
                    expect(comp.isSaving).toEqual(false);
                })
            );
        });
    });
});
