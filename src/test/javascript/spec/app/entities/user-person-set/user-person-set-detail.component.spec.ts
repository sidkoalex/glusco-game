/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { GluscoTestModule } from '../../../test.module';
import { UserPersonSetDetailComponent } from 'app/entities/user-person-set/user-person-set-detail.component';
import { UserPersonSet } from 'app/shared/model/user-person-set.model';

describe('Component Tests', () => {
    describe('UserPersonSet Management Detail Component', () => {
        let comp: UserPersonSetDetailComponent;
        let fixture: ComponentFixture<UserPersonSetDetailComponent>;
        const route = ({ data: of({ userPersonSet: new UserPersonSet(123) }) } as any) as ActivatedRoute;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [GluscoTestModule],
                declarations: [UserPersonSetDetailComponent],
                providers: [{ provide: ActivatedRoute, useValue: route }]
            })
                .overrideTemplate(UserPersonSetDetailComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(UserPersonSetDetailComponent);
            comp = fixture.componentInstance;
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(comp.userPersonSet).toEqual(jasmine.objectContaining({ id: 123 }));
            });
        });
    });
});
