/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';

import { GluscoTestModule } from '../../../test.module';
import { UserPersonSetUpdateComponent } from 'app/entities/user-person-set/user-person-set-update.component';
import { UserPersonSetService } from 'app/entities/user-person-set/user-person-set.service';
import { UserPersonSet } from 'app/shared/model/user-person-set.model';

describe('Component Tests', () => {
    describe('UserPersonSet Management Update Component', () => {
        let comp: UserPersonSetUpdateComponent;
        let fixture: ComponentFixture<UserPersonSetUpdateComponent>;
        let service: UserPersonSetService;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [GluscoTestModule],
                declarations: [UserPersonSetUpdateComponent]
            })
                .overrideTemplate(UserPersonSetUpdateComponent, '')
                .compileComponents();

            fixture = TestBed.createComponent(UserPersonSetUpdateComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(UserPersonSetService);
        });

        describe('save', () => {
            it(
                'Should call update service on save for existing entity',
                fakeAsync(() => {
                    // GIVEN
                    const entity = new UserPersonSet(123);
                    spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
                    comp.userPersonSet = entity;
                    // WHEN
                    comp.save();
                    tick(); // simulate async

                    // THEN
                    expect(service.update).toHaveBeenCalledWith(entity);
                    expect(comp.isSaving).toEqual(false);
                })
            );

            it(
                'Should call create service on save for new entity',
                fakeAsync(() => {
                    // GIVEN
                    const entity = new UserPersonSet();
                    spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
                    comp.userPersonSet = entity;
                    // WHEN
                    comp.save();
                    tick(); // simulate async

                    // THEN
                    expect(service.create).toHaveBeenCalledWith(entity);
                    expect(comp.isSaving).toEqual(false);
                })
            );
        });
    });
});
