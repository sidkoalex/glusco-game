/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';

import { GluscoTestModule } from '../../../test.module';
import { PersonCarUpdateComponent } from 'app/entities/person-car/person-car-update.component';
import { PersonCarService } from 'app/entities/person-car/person-car.service';
import { PersonCar } from 'app/shared/model/person-car.model';

describe('Component Tests', () => {
    describe('PersonCar Management Update Component', () => {
        let comp: PersonCarUpdateComponent;
        let fixture: ComponentFixture<PersonCarUpdateComponent>;
        let service: PersonCarService;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [GluscoTestModule],
                declarations: [PersonCarUpdateComponent]
            })
                .overrideTemplate(PersonCarUpdateComponent, '')
                .compileComponents();

            fixture = TestBed.createComponent(PersonCarUpdateComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(PersonCarService);
        });

        describe('save', () => {
            it(
                'Should call update service on save for existing entity',
                fakeAsync(() => {
                    // GIVEN
                    const entity = new PersonCar(123);
                    spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
                    comp.personCar = entity;
                    // WHEN
                    comp.save();
                    tick(); // simulate async

                    // THEN
                    expect(service.update).toHaveBeenCalledWith(entity);
                    expect(comp.isSaving).toEqual(false);
                })
            );

            it(
                'Should call create service on save for new entity',
                fakeAsync(() => {
                    // GIVEN
                    const entity = new PersonCar();
                    spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
                    comp.personCar = entity;
                    // WHEN
                    comp.save();
                    tick(); // simulate async

                    // THEN
                    expect(service.create).toHaveBeenCalledWith(entity);
                    expect(comp.isSaving).toEqual(false);
                })
            );
        });
    });
});
