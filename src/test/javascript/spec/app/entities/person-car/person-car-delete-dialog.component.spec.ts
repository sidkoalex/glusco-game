/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable, of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { GluscoTestModule } from '../../../test.module';
import { PersonCarDeleteDialogComponent } from 'app/entities/person-car/person-car-delete-dialog.component';
import { PersonCarService } from 'app/entities/person-car/person-car.service';

describe('Component Tests', () => {
    describe('PersonCar Management Delete Component', () => {
        let comp: PersonCarDeleteDialogComponent;
        let fixture: ComponentFixture<PersonCarDeleteDialogComponent>;
        let service: PersonCarService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [GluscoTestModule],
                declarations: [PersonCarDeleteDialogComponent]
            })
                .overrideTemplate(PersonCarDeleteDialogComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(PersonCarDeleteDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(PersonCarService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('confirmDelete', () => {
            it('Should call delete service on confirmDelete', inject(
                [],
                fakeAsync(() => {
                    // GIVEN
                    spyOn(service, 'delete').and.returnValue(of({}));

                    // WHEN
                    comp.confirmDelete(123);
                    tick();

                    // THEN
                    expect(service.delete).toHaveBeenCalledWith(123);
                    expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                    expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
                })
            ));
        });
    });
});
