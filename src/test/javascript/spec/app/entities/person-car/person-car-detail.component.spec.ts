/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { GluscoTestModule } from '../../../test.module';
import { PersonCarDetailComponent } from 'app/entities/person-car/person-car-detail.component';
import { PersonCar } from 'app/shared/model/person-car.model';

describe('Component Tests', () => {
    describe('PersonCar Management Detail Component', () => {
        let comp: PersonCarDetailComponent;
        let fixture: ComponentFixture<PersonCarDetailComponent>;
        const route = ({ data: of({ personCar: new PersonCar(123) }) } as any) as ActivatedRoute;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [GluscoTestModule],
                declarations: [PersonCarDetailComponent],
                providers: [{ provide: ActivatedRoute, useValue: route }]
            })
                .overrideTemplate(PersonCarDetailComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(PersonCarDetailComponent);
            comp = fixture.componentInstance;
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(comp.personCar).toEqual(jasmine.objectContaining({ id: 123 }));
            });
        });
    });
});
