/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { GluscoTestModule } from '../../../test.module';
import { RaceRewardDetailComponent } from 'app/entities/race-reward/race-reward-detail.component';
import { RaceReward } from 'app/shared/model/race-reward.model';

describe('Component Tests', () => {
    describe('RaceReward Management Detail Component', () => {
        let comp: RaceRewardDetailComponent;
        let fixture: ComponentFixture<RaceRewardDetailComponent>;
        const route = ({ data: of({ raceReward: new RaceReward(123) }) } as any) as ActivatedRoute;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [GluscoTestModule],
                declarations: [RaceRewardDetailComponent],
                providers: [{ provide: ActivatedRoute, useValue: route }]
            })
                .overrideTemplate(RaceRewardDetailComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(RaceRewardDetailComponent);
            comp = fixture.componentInstance;
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(comp.raceReward).toEqual(jasmine.objectContaining({ id: 123 }));
            });
        });
    });
});
