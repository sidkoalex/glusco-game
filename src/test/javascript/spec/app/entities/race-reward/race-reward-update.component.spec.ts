/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';

import { GluscoTestModule } from '../../../test.module';
import { RaceRewardUpdateComponent } from 'app/entities/race-reward/race-reward-update.component';
import { RaceRewardService } from 'app/entities/race-reward/race-reward.service';
import { RaceReward } from 'app/shared/model/race-reward.model';

describe('Component Tests', () => {
    describe('RaceReward Management Update Component', () => {
        let comp: RaceRewardUpdateComponent;
        let fixture: ComponentFixture<RaceRewardUpdateComponent>;
        let service: RaceRewardService;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [GluscoTestModule],
                declarations: [RaceRewardUpdateComponent]
            })
                .overrideTemplate(RaceRewardUpdateComponent, '')
                .compileComponents();

            fixture = TestBed.createComponent(RaceRewardUpdateComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(RaceRewardService);
        });

        describe('save', () => {
            it(
                'Should call update service on save for existing entity',
                fakeAsync(() => {
                    // GIVEN
                    const entity = new RaceReward(123);
                    spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
                    comp.raceReward = entity;
                    // WHEN
                    comp.save();
                    tick(); // simulate async

                    // THEN
                    expect(service.update).toHaveBeenCalledWith(entity);
                    expect(comp.isSaving).toEqual(false);
                })
            );

            it(
                'Should call create service on save for new entity',
                fakeAsync(() => {
                    // GIVEN
                    const entity = new RaceReward();
                    spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
                    comp.raceReward = entity;
                    // WHEN
                    comp.save();
                    tick(); // simulate async

                    // THEN
                    expect(service.create).toHaveBeenCalledWith(entity);
                    expect(comp.isSaving).toEqual(false);
                })
            );
        });
    });
});
