/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable, of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { GluscoTestModule } from '../../../test.module';
import { RaceRewardDeleteDialogComponent } from 'app/entities/race-reward/race-reward-delete-dialog.component';
import { RaceRewardService } from 'app/entities/race-reward/race-reward.service';

describe('Component Tests', () => {
    describe('RaceReward Management Delete Component', () => {
        let comp: RaceRewardDeleteDialogComponent;
        let fixture: ComponentFixture<RaceRewardDeleteDialogComponent>;
        let service: RaceRewardService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [GluscoTestModule],
                declarations: [RaceRewardDeleteDialogComponent]
            })
                .overrideTemplate(RaceRewardDeleteDialogComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(RaceRewardDeleteDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(RaceRewardService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('confirmDelete', () => {
            it('Should call delete service on confirmDelete', inject(
                [],
                fakeAsync(() => {
                    // GIVEN
                    spyOn(service, 'delete').and.returnValue(of({}));

                    // WHEN
                    comp.confirmDelete(123);
                    tick();

                    // THEN
                    expect(service.delete).toHaveBeenCalledWith(123);
                    expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                    expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
                })
            ));
        });
    });
});
