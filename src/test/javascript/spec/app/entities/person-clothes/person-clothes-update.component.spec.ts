/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';

import { GluscoTestModule } from '../../../test.module';
import { PersonClothesUpdateComponent } from 'app/entities/person-clothes/person-clothes-update.component';
import { PersonClothesService } from 'app/entities/person-clothes/person-clothes.service';
import { PersonClothes } from 'app/shared/model/person-clothes.model';

describe('Component Tests', () => {
    describe('PersonClothes Management Update Component', () => {
        let comp: PersonClothesUpdateComponent;
        let fixture: ComponentFixture<PersonClothesUpdateComponent>;
        let service: PersonClothesService;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [GluscoTestModule],
                declarations: [PersonClothesUpdateComponent]
            })
                .overrideTemplate(PersonClothesUpdateComponent, '')
                .compileComponents();

            fixture = TestBed.createComponent(PersonClothesUpdateComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(PersonClothesService);
        });

        describe('save', () => {
            it(
                'Should call update service on save for existing entity',
                fakeAsync(() => {
                    // GIVEN
                    const entity = new PersonClothes(123);
                    spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
                    comp.personClothes = entity;
                    // WHEN
                    comp.save();
                    tick(); // simulate async

                    // THEN
                    expect(service.update).toHaveBeenCalledWith(entity);
                    expect(comp.isSaving).toEqual(false);
                })
            );

            it(
                'Should call create service on save for new entity',
                fakeAsync(() => {
                    // GIVEN
                    const entity = new PersonClothes();
                    spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
                    comp.personClothes = entity;
                    // WHEN
                    comp.save();
                    tick(); // simulate async

                    // THEN
                    expect(service.create).toHaveBeenCalledWith(entity);
                    expect(comp.isSaving).toEqual(false);
                })
            );
        });
    });
});
