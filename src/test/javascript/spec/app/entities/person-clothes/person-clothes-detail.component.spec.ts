/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { GluscoTestModule } from '../../../test.module';
import { PersonClothesDetailComponent } from 'app/entities/person-clothes/person-clothes-detail.component';
import { PersonClothes } from 'app/shared/model/person-clothes.model';

describe('Component Tests', () => {
    describe('PersonClothes Management Detail Component', () => {
        let comp: PersonClothesDetailComponent;
        let fixture: ComponentFixture<PersonClothesDetailComponent>;
        const route = ({ data: of({ personClothes: new PersonClothes(123) }) } as any) as ActivatedRoute;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [GluscoTestModule],
                declarations: [PersonClothesDetailComponent],
                providers: [{ provide: ActivatedRoute, useValue: route }]
            })
                .overrideTemplate(PersonClothesDetailComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(PersonClothesDetailComponent);
            comp = fixture.componentInstance;
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(comp.personClothes).toEqual(jasmine.objectContaining({ id: 123 }));
            });
        });
    });
});
