/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable, of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { GluscoTestModule } from '../../../test.module';
import { PersonClothesDeleteDialogComponent } from 'app/entities/person-clothes/person-clothes-delete-dialog.component';
import { PersonClothesService } from 'app/entities/person-clothes/person-clothes.service';

describe('Component Tests', () => {
    describe('PersonClothes Management Delete Component', () => {
        let comp: PersonClothesDeleteDialogComponent;
        let fixture: ComponentFixture<PersonClothesDeleteDialogComponent>;
        let service: PersonClothesService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [GluscoTestModule],
                declarations: [PersonClothesDeleteDialogComponent]
            })
                .overrideTemplate(PersonClothesDeleteDialogComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(PersonClothesDeleteDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(PersonClothesService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('confirmDelete', () => {
            it('Should call delete service on confirmDelete', inject(
                [],
                fakeAsync(() => {
                    // GIVEN
                    spyOn(service, 'delete').and.returnValue(of({}));

                    // WHEN
                    comp.confirmDelete(123);
                    tick();

                    // THEN
                    expect(service.delete).toHaveBeenCalledWith(123);
                    expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                    expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
                })
            ));
        });
    });
});
