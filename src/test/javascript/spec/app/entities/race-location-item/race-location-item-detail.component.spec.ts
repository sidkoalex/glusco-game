/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { GluscoTestModule } from '../../../test.module';
import { RaceLocationItemDetailComponent } from 'app/entities/race-location-item/race-location-item-detail.component';
import { RaceLocationItem } from 'app/shared/model/race-location-item.model';

describe('Component Tests', () => {
    describe('RaceLocationItem Management Detail Component', () => {
        let comp: RaceLocationItemDetailComponent;
        let fixture: ComponentFixture<RaceLocationItemDetailComponent>;
        const route = ({ data: of({ raceLocationItem: new RaceLocationItem(123) }) } as any) as ActivatedRoute;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [GluscoTestModule],
                declarations: [RaceLocationItemDetailComponent],
                providers: [{ provide: ActivatedRoute, useValue: route }]
            })
                .overrideTemplate(RaceLocationItemDetailComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(RaceLocationItemDetailComponent);
            comp = fixture.componentInstance;
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(comp.raceLocationItem).toEqual(jasmine.objectContaining({ id: 123 }));
            });
        });
    });
});
