/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';

import { GluscoTestModule } from '../../../test.module';
import { RaceLocationItemUpdateComponent } from 'app/entities/race-location-item/race-location-item-update.component';
import { RaceLocationItemService } from 'app/entities/race-location-item/race-location-item.service';
import { RaceLocationItem } from 'app/shared/model/race-location-item.model';

describe('Component Tests', () => {
    describe('RaceLocationItem Management Update Component', () => {
        let comp: RaceLocationItemUpdateComponent;
        let fixture: ComponentFixture<RaceLocationItemUpdateComponent>;
        let service: RaceLocationItemService;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [GluscoTestModule],
                declarations: [RaceLocationItemUpdateComponent]
            })
                .overrideTemplate(RaceLocationItemUpdateComponent, '')
                .compileComponents();

            fixture = TestBed.createComponent(RaceLocationItemUpdateComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(RaceLocationItemService);
        });

        describe('save', () => {
            it(
                'Should call update service on save for existing entity',
                fakeAsync(() => {
                    // GIVEN
                    const entity = new RaceLocationItem(123);
                    spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
                    comp.raceLocationItem = entity;
                    // WHEN
                    comp.save();
                    tick(); // simulate async

                    // THEN
                    expect(service.update).toHaveBeenCalledWith(entity);
                    expect(comp.isSaving).toEqual(false);
                })
            );

            it(
                'Should call create service on save for new entity',
                fakeAsync(() => {
                    // GIVEN
                    const entity = new RaceLocationItem();
                    spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
                    comp.raceLocationItem = entity;
                    // WHEN
                    comp.save();
                    tick(); // simulate async

                    // THEN
                    expect(service.create).toHaveBeenCalledWith(entity);
                    expect(comp.isSaving).toEqual(false);
                })
            );
        });
    });
});
