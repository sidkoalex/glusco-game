/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { GluscoTestModule } from '../../../test.module';
import { RaceLocationDetailComponent } from 'app/entities/race-location/race-location-detail.component';
import { RaceLocation } from 'app/shared/model/race-location.model';

describe('Component Tests', () => {
    describe('RaceLocation Management Detail Component', () => {
        let comp: RaceLocationDetailComponent;
        let fixture: ComponentFixture<RaceLocationDetailComponent>;
        const route = ({ data: of({ raceLocation: new RaceLocation(123) }) } as any) as ActivatedRoute;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [GluscoTestModule],
                declarations: [RaceLocationDetailComponent],
                providers: [{ provide: ActivatedRoute, useValue: route }]
            })
                .overrideTemplate(RaceLocationDetailComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(RaceLocationDetailComponent);
            comp = fixture.componentInstance;
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(comp.raceLocation).toEqual(jasmine.objectContaining({ id: 123 }));
            });
        });
    });
});
