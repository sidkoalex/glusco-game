/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';

import { GluscoTestModule } from '../../../test.module';
import { RaceLocationUpdateComponent } from 'app/entities/race-location/race-location-update.component';
import { RaceLocationService } from 'app/entities/race-location/race-location.service';
import { RaceLocation } from 'app/shared/model/race-location.model';

describe('Component Tests', () => {
    describe('RaceLocation Management Update Component', () => {
        let comp: RaceLocationUpdateComponent;
        let fixture: ComponentFixture<RaceLocationUpdateComponent>;
        let service: RaceLocationService;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [GluscoTestModule],
                declarations: [RaceLocationUpdateComponent]
            })
                .overrideTemplate(RaceLocationUpdateComponent, '')
                .compileComponents();

            fixture = TestBed.createComponent(RaceLocationUpdateComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(RaceLocationService);
        });

        describe('save', () => {
            it(
                'Should call update service on save for existing entity',
                fakeAsync(() => {
                    // GIVEN
                    const entity = new RaceLocation(123);
                    spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
                    comp.raceLocation = entity;
                    // WHEN
                    comp.save();
                    tick(); // simulate async

                    // THEN
                    expect(service.update).toHaveBeenCalledWith(entity);
                    expect(comp.isSaving).toEqual(false);
                })
            );

            it(
                'Should call create service on save for new entity',
                fakeAsync(() => {
                    // GIVEN
                    const entity = new RaceLocation();
                    spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
                    comp.raceLocation = entity;
                    // WHEN
                    comp.save();
                    tick(); // simulate async

                    // THEN
                    expect(service.create).toHaveBeenCalledWith(entity);
                    expect(comp.isSaving).toEqual(false);
                })
            );
        });
    });
});
