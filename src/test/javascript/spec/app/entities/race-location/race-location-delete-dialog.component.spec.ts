/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable, of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { GluscoTestModule } from '../../../test.module';
import { RaceLocationDeleteDialogComponent } from 'app/entities/race-location/race-location-delete-dialog.component';
import { RaceLocationService } from 'app/entities/race-location/race-location.service';

describe('Component Tests', () => {
    describe('RaceLocation Management Delete Component', () => {
        let comp: RaceLocationDeleteDialogComponent;
        let fixture: ComponentFixture<RaceLocationDeleteDialogComponent>;
        let service: RaceLocationService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [GluscoTestModule],
                declarations: [RaceLocationDeleteDialogComponent]
            })
                .overrideTemplate(RaceLocationDeleteDialogComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(RaceLocationDeleteDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(RaceLocationService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('confirmDelete', () => {
            it('Should call delete service on confirmDelete', inject(
                [],
                fakeAsync(() => {
                    // GIVEN
                    spyOn(service, 'delete').and.returnValue(of({}));

                    // WHEN
                    comp.confirmDelete(123);
                    tick();

                    // THEN
                    expect(service.delete).toHaveBeenCalledWith(123);
                    expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                    expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
                })
            ));
        });
    });
});
