/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';

import { GluscoTestModule } from '../../../test.module';
import { ClothesUpdateComponent } from 'app/entities/clothes/clothes-update.component';
import { ClothesService } from 'app/entities/clothes/clothes.service';
import { Clothes } from 'app/shared/model/clothes.model';

describe('Component Tests', () => {
    describe('Clothes Management Update Component', () => {
        let comp: ClothesUpdateComponent;
        let fixture: ComponentFixture<ClothesUpdateComponent>;
        let service: ClothesService;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [GluscoTestModule],
                declarations: [ClothesUpdateComponent]
            })
                .overrideTemplate(ClothesUpdateComponent, '')
                .compileComponents();

            fixture = TestBed.createComponent(ClothesUpdateComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(ClothesService);
        });

        describe('save', () => {
            it(
                'Should call update service on save for existing entity',
                fakeAsync(() => {
                    // GIVEN
                    const entity = new Clothes(123);
                    spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
                    comp.clothes = entity;
                    // WHEN
                    comp.save();
                    tick(); // simulate async

                    // THEN
                    expect(service.update).toHaveBeenCalledWith(entity);
                    expect(comp.isSaving).toEqual(false);
                })
            );

            it(
                'Should call create service on save for new entity',
                fakeAsync(() => {
                    // GIVEN
                    const entity = new Clothes();
                    spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
                    comp.clothes = entity;
                    // WHEN
                    comp.save();
                    tick(); // simulate async

                    // THEN
                    expect(service.create).toHaveBeenCalledWith(entity);
                    expect(comp.isSaving).toEqual(false);
                })
            );
        });
    });
});
