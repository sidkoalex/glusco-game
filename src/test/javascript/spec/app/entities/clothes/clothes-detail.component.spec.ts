/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { GluscoTestModule } from '../../../test.module';
import { ClothesDetailComponent } from 'app/entities/clothes/clothes-detail.component';
import { Clothes } from 'app/shared/model/clothes.model';

describe('Component Tests', () => {
    describe('Clothes Management Detail Component', () => {
        let comp: ClothesDetailComponent;
        let fixture: ComponentFixture<ClothesDetailComponent>;
        const route = ({ data: of({ clothes: new Clothes(123) }) } as any) as ActivatedRoute;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [GluscoTestModule],
                declarations: [ClothesDetailComponent],
                providers: [{ provide: ActivatedRoute, useValue: route }]
            })
                .overrideTemplate(ClothesDetailComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(ClothesDetailComponent);
            comp = fixture.componentInstance;
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(comp.clothes).toEqual(jasmine.objectContaining({ id: 123 }));
            });
        });
    });
});
