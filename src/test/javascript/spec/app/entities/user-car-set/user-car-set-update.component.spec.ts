/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';

import { GluscoTestModule } from '../../../test.module';
import { UserCarSetUpdateComponent } from 'app/entities/user-car-set/user-car-set-update.component';
import { UserCarSetService } from 'app/entities/user-car-set/user-car-set.service';
import { UserCarSet } from 'app/shared/model/user-car-set.model';

describe('Component Tests', () => {
    describe('UserCarSet Management Update Component', () => {
        let comp: UserCarSetUpdateComponent;
        let fixture: ComponentFixture<UserCarSetUpdateComponent>;
        let service: UserCarSetService;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [GluscoTestModule],
                declarations: [UserCarSetUpdateComponent]
            })
                .overrideTemplate(UserCarSetUpdateComponent, '')
                .compileComponents();

            fixture = TestBed.createComponent(UserCarSetUpdateComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(UserCarSetService);
        });

        describe('save', () => {
            it(
                'Should call update service on save for existing entity',
                fakeAsync(() => {
                    // GIVEN
                    const entity = new UserCarSet(123);
                    spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
                    comp.userCarSet = entity;
                    // WHEN
                    comp.save();
                    tick(); // simulate async

                    // THEN
                    expect(service.update).toHaveBeenCalledWith(entity);
                    expect(comp.isSaving).toEqual(false);
                })
            );

            it(
                'Should call create service on save for new entity',
                fakeAsync(() => {
                    // GIVEN
                    const entity = new UserCarSet();
                    spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
                    comp.userCarSet = entity;
                    // WHEN
                    comp.save();
                    tick(); // simulate async

                    // THEN
                    expect(service.create).toHaveBeenCalledWith(entity);
                    expect(comp.isSaving).toEqual(false);
                })
            );
        });
    });
});
