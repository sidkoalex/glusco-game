/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable, of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { GluscoTestModule } from '../../../test.module';
import { UserCarSetDeleteDialogComponent } from 'app/entities/user-car-set/user-car-set-delete-dialog.component';
import { UserCarSetService } from 'app/entities/user-car-set/user-car-set.service';

describe('Component Tests', () => {
    describe('UserCarSet Management Delete Component', () => {
        let comp: UserCarSetDeleteDialogComponent;
        let fixture: ComponentFixture<UserCarSetDeleteDialogComponent>;
        let service: UserCarSetService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [GluscoTestModule],
                declarations: [UserCarSetDeleteDialogComponent]
            })
                .overrideTemplate(UserCarSetDeleteDialogComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(UserCarSetDeleteDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(UserCarSetService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('confirmDelete', () => {
            it('Should call delete service on confirmDelete', inject(
                [],
                fakeAsync(() => {
                    // GIVEN
                    spyOn(service, 'delete').and.returnValue(of({}));

                    // WHEN
                    comp.confirmDelete(123);
                    tick();

                    // THEN
                    expect(service.delete).toHaveBeenCalledWith(123);
                    expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                    expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
                })
            ));
        });
    });
});
