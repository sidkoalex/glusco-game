/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { GluscoTestModule } from '../../../test.module';
import { UserCarSetDetailComponent } from 'app/entities/user-car-set/user-car-set-detail.component';
import { UserCarSet } from 'app/shared/model/user-car-set.model';

describe('Component Tests', () => {
    describe('UserCarSet Management Detail Component', () => {
        let comp: UserCarSetDetailComponent;
        let fixture: ComponentFixture<UserCarSetDetailComponent>;
        const route = ({ data: of({ userCarSet: new UserCarSet(123) }) } as any) as ActivatedRoute;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [GluscoTestModule],
                declarations: [UserCarSetDetailComponent],
                providers: [{ provide: ActivatedRoute, useValue: route }]
            })
                .overrideTemplate(UserCarSetDetailComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(UserCarSetDetailComponent);
            comp = fixture.componentInstance;
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(comp.userCarSet).toEqual(jasmine.objectContaining({ id: 123 }));
            });
        });
    });
});
