import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { IClothes } from 'app/shared/model/clothes.model';
import { ClothesService } from './clothes.service';

@Component({
    selector: 'jhi-clothes-update',
    templateUrl: './clothes-update.component.html'
})
export class ClothesUpdateComponent implements OnInit {
    private _clothes: IClothes;
    isSaving: boolean;

    constructor(private clothesService: ClothesService, private activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ clothes }) => {
            this.clothes = clothes;
        });
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        if (this.clothes.id !== undefined) {
            this.subscribeToSaveResponse(this.clothesService.update(this.clothes));
        } else {
            this.subscribeToSaveResponse(this.clothesService.create(this.clothes));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<IClothes>>) {
        result.subscribe((res: HttpResponse<IClothes>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    private onSaveError() {
        this.isSaving = false;
    }
    get clothes() {
        return this._clothes;
    }

    set clothes(clothes: IClothes) {
        this._clothes = clothes;
    }
}
