import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { GluscoSharedModule } from 'app/shared';
import {
    ClothesComponent,
    ClothesDetailComponent,
    ClothesUpdateComponent,
    ClothesDeletePopupComponent,
    ClothesDeleteDialogComponent,
    clothesRoute,
    clothesPopupRoute
} from './';

const ENTITY_STATES = [...clothesRoute, ...clothesPopupRoute];

@NgModule({
    imports: [GluscoSharedModule, RouterModule.forChild(ENTITY_STATES)],
    declarations: [
        ClothesComponent,
        ClothesDetailComponent,
        ClothesUpdateComponent,
        ClothesDeleteDialogComponent,
        ClothesDeletePopupComponent
    ],
    entryComponents: [ClothesComponent, ClothesUpdateComponent, ClothesDeleteDialogComponent, ClothesDeletePopupComponent],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class GluscoClothesModule {}
