import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil, JhiResolvePagingParams } from 'ng-jhipster';
import { UserRouteAccessService } from 'app/core';
import { of } from 'rxjs';
import { map } from 'rxjs/operators';
import { Clothes } from 'app/shared/model/clothes.model';
import { ClothesService } from './clothes.service';
import { ClothesComponent } from './clothes.component';
import { ClothesDetailComponent } from './clothes-detail.component';
import { ClothesUpdateComponent } from './clothes-update.component';
import { ClothesDeletePopupComponent } from './clothes-delete-dialog.component';
import { IClothes } from 'app/shared/model/clothes.model';

@Injectable({ providedIn: 'root' })
export class ClothesResolve implements Resolve<IClothes> {
    constructor(private service: ClothesService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const id = route.params['id'] ? route.params['id'] : null;
        if (id) {
            return this.service.find(id).pipe(map((clothes: HttpResponse<Clothes>) => clothes.body));
        }
        return of(new Clothes());
    }
}

export const clothesRoute: Routes = [
    {
        path: 'clothes',
        component: ClothesComponent,
        resolve: {
            pagingParams: JhiResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            defaultSort: 'id,asc',
            pageTitle: 'gluscoApp.clothes.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'clothes/:id/view',
        component: ClothesDetailComponent,
        resolve: {
            clothes: ClothesResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'gluscoApp.clothes.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'clothes/new',
        component: ClothesUpdateComponent,
        resolve: {
            clothes: ClothesResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'gluscoApp.clothes.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'clothes/:id/edit',
        component: ClothesUpdateComponent,
        resolve: {
            clothes: ClothesResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'gluscoApp.clothes.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const clothesPopupRoute: Routes = [
    {
        path: 'clothes/:id/delete',
        component: ClothesDeletePopupComponent,
        resolve: {
            clothes: ClothesResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'gluscoApp.clothes.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
