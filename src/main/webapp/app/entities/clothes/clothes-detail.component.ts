import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IClothes } from 'app/shared/model/clothes.model';

@Component({
    selector: 'jhi-clothes-detail',
    templateUrl: './clothes-detail.component.html'
})
export class ClothesDetailComponent implements OnInit {
    clothes: IClothes;

    constructor(private activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ clothes }) => {
            this.clothes = clothes;
        });
    }

    previousState() {
        window.history.back();
    }
}
