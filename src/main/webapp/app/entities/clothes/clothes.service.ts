import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { IClothes } from 'app/shared/model/clothes.model';

type EntityResponseType = HttpResponse<IClothes>;
type EntityArrayResponseType = HttpResponse<IClothes[]>;

@Injectable({ providedIn: 'root' })
export class ClothesService {
    private resourceUrl = SERVER_API_URL + 'api/clothes';

    constructor(private http: HttpClient) {}

    create(clothes: IClothes): Observable<EntityResponseType> {
        return this.http.post<IClothes>(this.resourceUrl, clothes, { observe: 'response' });
    }

    update(clothes: IClothes): Observable<EntityResponseType> {
        return this.http.put<IClothes>(this.resourceUrl, clothes, { observe: 'response' });
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<IClothes>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    query(req?: any): Observable<EntityArrayResponseType> {
        const options = createRequestOption(req);
        return this.http.get<IClothes[]>(this.resourceUrl, { params: options, observe: 'response' });
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }
}
