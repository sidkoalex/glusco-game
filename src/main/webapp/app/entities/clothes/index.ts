export * from './clothes.service';
export * from './clothes-update.component';
export * from './clothes-delete-dialog.component';
export * from './clothes-detail.component';
export * from './clothes.component';
export * from './clothes.route';
