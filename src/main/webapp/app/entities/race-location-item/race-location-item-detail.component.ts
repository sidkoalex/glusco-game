import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IRaceLocationItem } from 'app/shared/model/race-location-item.model';

@Component({
    selector: 'jhi-race-location-item-detail',
    templateUrl: './race-location-item-detail.component.html'
})
export class RaceLocationItemDetailComponent implements OnInit {
    raceLocationItem: IRaceLocationItem;

    constructor(private activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ raceLocationItem }) => {
            this.raceLocationItem = raceLocationItem;
        });
    }

    previousState() {
        window.history.back();
    }
}
