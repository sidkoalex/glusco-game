import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IRaceLocationItem } from 'app/shared/model/race-location-item.model';
import { RaceLocationItemService } from './race-location-item.service';

@Component({
    selector: 'jhi-race-location-item-delete-dialog',
    templateUrl: './race-location-item-delete-dialog.component.html'
})
export class RaceLocationItemDeleteDialogComponent {
    raceLocationItem: IRaceLocationItem;

    constructor(
        private raceLocationItemService: RaceLocationItemService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {}

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.raceLocationItemService.delete(id).subscribe(response => {
            this.eventManager.broadcast({
                name: 'raceLocationItemListModification',
                content: 'Deleted an raceLocationItem'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-race-location-item-delete-popup',
    template: ''
})
export class RaceLocationItemDeletePopupComponent implements OnInit, OnDestroy {
    private ngbModalRef: NgbModalRef;

    constructor(private activatedRoute: ActivatedRoute, private router: Router, private modalService: NgbModal) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ raceLocationItem }) => {
            setTimeout(() => {
                this.ngbModalRef = this.modalService.open(RaceLocationItemDeleteDialogComponent as Component, {
                    size: 'lg',
                    backdrop: 'static'
                });
                this.ngbModalRef.componentInstance.raceLocationItem = raceLocationItem;
                this.ngbModalRef.result.then(
                    result => {
                        this.router.navigate([{ outlets: { popup: null } }], { replaceUrl: true, queryParamsHandling: 'merge' });
                        this.ngbModalRef = null;
                    },
                    reason => {
                        this.router.navigate([{ outlets: { popup: null } }], { replaceUrl: true, queryParamsHandling: 'merge' });
                        this.ngbModalRef = null;
                    }
                );
            }, 0);
        });
    }

    ngOnDestroy() {
        this.ngbModalRef = null;
    }
}
