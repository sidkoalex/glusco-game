import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil, JhiResolvePagingParams } from 'ng-jhipster';
import { UserRouteAccessService } from 'app/core';
import { of } from 'rxjs';
import { map } from 'rxjs/operators';
import { RaceLocationItem } from 'app/shared/model/race-location-item.model';
import { RaceLocationItemService } from './race-location-item.service';
import { RaceLocationItemComponent } from './race-location-item.component';
import { RaceLocationItemDetailComponent } from './race-location-item-detail.component';
import { RaceLocationItemUpdateComponent } from './race-location-item-update.component';
import { RaceLocationItemDeletePopupComponent } from './race-location-item-delete-dialog.component';
import { IRaceLocationItem } from 'app/shared/model/race-location-item.model';

@Injectable({ providedIn: 'root' })
export class RaceLocationItemResolve implements Resolve<IRaceLocationItem> {
    constructor(private service: RaceLocationItemService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const id = route.params['id'] ? route.params['id'] : null;
        if (id) {
            return this.service.find(id).pipe(map((raceLocationItem: HttpResponse<RaceLocationItem>) => raceLocationItem.body));
        }
        return of(new RaceLocationItem());
    }
}

export const raceLocationItemRoute: Routes = [
    {
        path: 'race-location-item',
        component: RaceLocationItemComponent,
        resolve: {
            pagingParams: JhiResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            defaultSort: 'id,asc',
            pageTitle: 'gluscoApp.raceLocationItem.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'race-location-item/:id/view',
        component: RaceLocationItemDetailComponent,
        resolve: {
            raceLocationItem: RaceLocationItemResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'gluscoApp.raceLocationItem.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'race-location-item/new',
        component: RaceLocationItemUpdateComponent,
        resolve: {
            raceLocationItem: RaceLocationItemResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'gluscoApp.raceLocationItem.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'race-location-item/:id/edit',
        component: RaceLocationItemUpdateComponent,
        resolve: {
            raceLocationItem: RaceLocationItemResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'gluscoApp.raceLocationItem.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const raceLocationItemPopupRoute: Routes = [
    {
        path: 'race-location-item/:id/delete',
        component: RaceLocationItemDeletePopupComponent,
        resolve: {
            raceLocationItem: RaceLocationItemResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'gluscoApp.raceLocationItem.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
