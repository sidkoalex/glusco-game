import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { JhiAlertService } from 'ng-jhipster';

import { IRaceLocationItem } from 'app/shared/model/race-location-item.model';
import { RaceLocationItemService } from './race-location-item.service';
import { IRaceLocation } from 'app/shared/model/race-location.model';
import { RaceLocationService } from 'app/entities/race-location';
import { IRaceItem } from 'app/shared/model/race-item.model';
import { RaceItemService } from 'app/entities/race-item';

@Component({
    selector: 'jhi-race-location-item-update',
    templateUrl: './race-location-item-update.component.html'
})
export class RaceLocationItemUpdateComponent implements OnInit {
    private _raceLocationItem: IRaceLocationItem;
    isSaving: boolean;

    racelocations: IRaceLocation[];

    raceitems: IRaceItem[];

    constructor(
        private jhiAlertService: JhiAlertService,
        private raceLocationItemService: RaceLocationItemService,
        private raceLocationService: RaceLocationService,
        private raceItemService: RaceItemService,
        private activatedRoute: ActivatedRoute
    ) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ raceLocationItem }) => {
            this.raceLocationItem = raceLocationItem;
        });
        this.raceLocationService.query().subscribe(
            (res: HttpResponse<IRaceLocation[]>) => {
                this.racelocations = res.body;
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
        this.raceItemService.query().subscribe(
            (res: HttpResponse<IRaceItem[]>) => {
                this.raceitems = res.body;
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        if (this.raceLocationItem.id !== undefined) {
            this.subscribeToSaveResponse(this.raceLocationItemService.update(this.raceLocationItem));
        } else {
            this.subscribeToSaveResponse(this.raceLocationItemService.create(this.raceLocationItem));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<IRaceLocationItem>>) {
        result.subscribe((res: HttpResponse<IRaceLocationItem>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }

    trackRaceLocationById(index: number, item: IRaceLocation) {
        return item.id;
    }

    trackRaceItemById(index: number, item: IRaceItem) {
        return item.id;
    }
    get raceLocationItem() {
        return this._raceLocationItem;
    }

    set raceLocationItem(raceLocationItem: IRaceLocationItem) {
        this._raceLocationItem = raceLocationItem;
    }
}
