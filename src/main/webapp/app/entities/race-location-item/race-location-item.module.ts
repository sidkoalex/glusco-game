import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { GluscoSharedModule } from 'app/shared';
import {
    RaceLocationItemComponent,
    RaceLocationItemDetailComponent,
    RaceLocationItemUpdateComponent,
    RaceLocationItemDeletePopupComponent,
    RaceLocationItemDeleteDialogComponent,
    raceLocationItemRoute,
    raceLocationItemPopupRoute
} from './';

const ENTITY_STATES = [...raceLocationItemRoute, ...raceLocationItemPopupRoute];

@NgModule({
    imports: [GluscoSharedModule, RouterModule.forChild(ENTITY_STATES)],
    declarations: [
        RaceLocationItemComponent,
        RaceLocationItemDetailComponent,
        RaceLocationItemUpdateComponent,
        RaceLocationItemDeleteDialogComponent,
        RaceLocationItemDeletePopupComponent
    ],
    entryComponents: [
        RaceLocationItemComponent,
        RaceLocationItemUpdateComponent,
        RaceLocationItemDeleteDialogComponent,
        RaceLocationItemDeletePopupComponent
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class GluscoRaceLocationItemModule {}
