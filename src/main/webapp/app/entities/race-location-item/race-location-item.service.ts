import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { IRaceLocationItem } from 'app/shared/model/race-location-item.model';

type EntityResponseType = HttpResponse<IRaceLocationItem>;
type EntityArrayResponseType = HttpResponse<IRaceLocationItem[]>;

@Injectable({ providedIn: 'root' })
export class RaceLocationItemService {
    private resourceUrl = SERVER_API_URL + 'api/race-location-items';

    constructor(private http: HttpClient) {}

    create(raceLocationItem: IRaceLocationItem): Observable<EntityResponseType> {
        return this.http.post<IRaceLocationItem>(this.resourceUrl, raceLocationItem, { observe: 'response' });
    }

    update(raceLocationItem: IRaceLocationItem): Observable<EntityResponseType> {
        return this.http.put<IRaceLocationItem>(this.resourceUrl, raceLocationItem, { observe: 'response' });
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<IRaceLocationItem>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    query(req?: any): Observable<EntityArrayResponseType> {
        const options = createRequestOption(req);
        return this.http.get<IRaceLocationItem[]>(this.resourceUrl, { params: options, observe: 'response' });
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }
}
