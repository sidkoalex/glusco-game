import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { JhiAlertService } from 'ng-jhipster';

import { IUserPersonSet } from 'app/shared/model/user-person-set.model';
import { UserPersonSetService } from './user-person-set.service';
import { IUser, UserService } from 'app/core';
import { IPerson } from 'app/shared/model/person.model';
import { PersonService } from 'app/entities/person';
import { IClothes } from 'app/shared/model/clothes.model';
import { ClothesService } from 'app/entities/clothes';

@Component({
    selector: 'jhi-user-person-set-update',
    templateUrl: './user-person-set-update.component.html'
})
export class UserPersonSetUpdateComponent implements OnInit {
    private _userPersonSet: IUserPersonSet;
    isSaving: boolean;

    users: IUser[];

    people: IPerson[];

    clothes: IClothes[];

    constructor(
        private jhiAlertService: JhiAlertService,
        private userPersonSetService: UserPersonSetService,
        private userService: UserService,
        private personService: PersonService,
        private clothesService: ClothesService,
        private activatedRoute: ActivatedRoute
    ) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ userPersonSet }) => {
            this.userPersonSet = userPersonSet;
        });
        this.userService.query().subscribe(
            (res: HttpResponse<IUser[]>) => {
                this.users = res.body;
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
        this.personService.query().subscribe(
            (res: HttpResponse<IPerson[]>) => {
                this.people = res.body;
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
        this.clothesService.query().subscribe(
            (res: HttpResponse<IClothes[]>) => {
                this.clothes = res.body;
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        if (this.userPersonSet.id !== undefined) {
            this.subscribeToSaveResponse(this.userPersonSetService.update(this.userPersonSet));
        } else {
            this.subscribeToSaveResponse(this.userPersonSetService.create(this.userPersonSet));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<IUserPersonSet>>) {
        result.subscribe((res: HttpResponse<IUserPersonSet>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }

    trackUserById(index: number, item: IUser) {
        return item.id;
    }

    trackPersonById(index: number, item: IPerson) {
        return item.id;
    }

    trackClothesById(index: number, item: IClothes) {
        return item.id;
    }
    get userPersonSet() {
        return this._userPersonSet;
    }

    set userPersonSet(userPersonSet: IUserPersonSet) {
        this._userPersonSet = userPersonSet;
    }
}
