import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { GluscoSharedModule } from 'app/shared';
import { GluscoAdminModule } from 'app/admin/admin.module';
import {
    UserPersonSetComponent,
    UserPersonSetDetailComponent,
    UserPersonSetUpdateComponent,
    UserPersonSetDeletePopupComponent,
    UserPersonSetDeleteDialogComponent,
    userPersonSetRoute,
    userPersonSetPopupRoute
} from './';

const ENTITY_STATES = [...userPersonSetRoute, ...userPersonSetPopupRoute];

@NgModule({
    imports: [GluscoSharedModule, GluscoAdminModule, RouterModule.forChild(ENTITY_STATES)],
    declarations: [
        UserPersonSetComponent,
        UserPersonSetDetailComponent,
        UserPersonSetUpdateComponent,
        UserPersonSetDeleteDialogComponent,
        UserPersonSetDeletePopupComponent
    ],
    entryComponents: [
        UserPersonSetComponent,
        UserPersonSetUpdateComponent,
        UserPersonSetDeleteDialogComponent,
        UserPersonSetDeletePopupComponent
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class GluscoUserPersonSetModule {}
