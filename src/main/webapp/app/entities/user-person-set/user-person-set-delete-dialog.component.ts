import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IUserPersonSet } from 'app/shared/model/user-person-set.model';
import { UserPersonSetService } from './user-person-set.service';

@Component({
    selector: 'jhi-user-person-set-delete-dialog',
    templateUrl: './user-person-set-delete-dialog.component.html'
})
export class UserPersonSetDeleteDialogComponent {
    userPersonSet: IUserPersonSet;

    constructor(
        private userPersonSetService: UserPersonSetService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {}

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.userPersonSetService.delete(id).subscribe(response => {
            this.eventManager.broadcast({
                name: 'userPersonSetListModification',
                content: 'Deleted an userPersonSet'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-user-person-set-delete-popup',
    template: ''
})
export class UserPersonSetDeletePopupComponent implements OnInit, OnDestroy {
    private ngbModalRef: NgbModalRef;

    constructor(private activatedRoute: ActivatedRoute, private router: Router, private modalService: NgbModal) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ userPersonSet }) => {
            setTimeout(() => {
                this.ngbModalRef = this.modalService.open(UserPersonSetDeleteDialogComponent as Component, {
                    size: 'lg',
                    backdrop: 'static'
                });
                this.ngbModalRef.componentInstance.userPersonSet = userPersonSet;
                this.ngbModalRef.result.then(
                    result => {
                        this.router.navigate([{ outlets: { popup: null } }], { replaceUrl: true, queryParamsHandling: 'merge' });
                        this.ngbModalRef = null;
                    },
                    reason => {
                        this.router.navigate([{ outlets: { popup: null } }], { replaceUrl: true, queryParamsHandling: 'merge' });
                        this.ngbModalRef = null;
                    }
                );
            }, 0);
        });
    }

    ngOnDestroy() {
        this.ngbModalRef = null;
    }
}
