import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { IUserPersonSet } from 'app/shared/model/user-person-set.model';

type EntityResponseType = HttpResponse<IUserPersonSet>;
type EntityArrayResponseType = HttpResponse<IUserPersonSet[]>;

@Injectable({ providedIn: 'root' })
export class UserPersonSetService {
    private resourceUrl = SERVER_API_URL + 'api/user-person-sets';

    constructor(private http: HttpClient) {}

    create(userPersonSet: IUserPersonSet): Observable<EntityResponseType> {
        return this.http.post<IUserPersonSet>(this.resourceUrl, userPersonSet, { observe: 'response' });
    }

    update(userPersonSet: IUserPersonSet): Observable<EntityResponseType> {
        return this.http.put<IUserPersonSet>(this.resourceUrl, userPersonSet, { observe: 'response' });
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<IUserPersonSet>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    query(req?: any): Observable<EntityArrayResponseType> {
        const options = createRequestOption(req);
        return this.http.get<IUserPersonSet[]>(this.resourceUrl, { params: options, observe: 'response' });
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }
}
