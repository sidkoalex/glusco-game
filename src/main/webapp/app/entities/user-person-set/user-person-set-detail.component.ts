import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IUserPersonSet } from 'app/shared/model/user-person-set.model';

@Component({
    selector: 'jhi-user-person-set-detail',
    templateUrl: './user-person-set-detail.component.html'
})
export class UserPersonSetDetailComponent implements OnInit {
    userPersonSet: IUserPersonSet;

    constructor(private activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ userPersonSet }) => {
            this.userPersonSet = userPersonSet;
        });
    }

    previousState() {
        window.history.back();
    }
}
