import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil, JhiResolvePagingParams } from 'ng-jhipster';
import { UserRouteAccessService } from 'app/core';
import { of } from 'rxjs';
import { map } from 'rxjs/operators';
import { UserPersonSet } from 'app/shared/model/user-person-set.model';
import { UserPersonSetService } from './user-person-set.service';
import { UserPersonSetComponent } from './user-person-set.component';
import { UserPersonSetDetailComponent } from './user-person-set-detail.component';
import { UserPersonSetUpdateComponent } from './user-person-set-update.component';
import { UserPersonSetDeletePopupComponent } from './user-person-set-delete-dialog.component';
import { IUserPersonSet } from 'app/shared/model/user-person-set.model';

@Injectable({ providedIn: 'root' })
export class UserPersonSetResolve implements Resolve<IUserPersonSet> {
    constructor(private service: UserPersonSetService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const id = route.params['id'] ? route.params['id'] : null;
        if (id) {
            return this.service.find(id).pipe(map((userPersonSet: HttpResponse<UserPersonSet>) => userPersonSet.body));
        }
        return of(new UserPersonSet());
    }
}

export const userPersonSetRoute: Routes = [
    {
        path: 'user-person-set',
        component: UserPersonSetComponent,
        resolve: {
            pagingParams: JhiResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            defaultSort: 'id,asc',
            pageTitle: 'gluscoApp.userPersonSet.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'user-person-set/:id/view',
        component: UserPersonSetDetailComponent,
        resolve: {
            userPersonSet: UserPersonSetResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'gluscoApp.userPersonSet.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'user-person-set/new',
        component: UserPersonSetUpdateComponent,
        resolve: {
            userPersonSet: UserPersonSetResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'gluscoApp.userPersonSet.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'user-person-set/:id/edit',
        component: UserPersonSetUpdateComponent,
        resolve: {
            userPersonSet: UserPersonSetResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'gluscoApp.userPersonSet.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const userPersonSetPopupRoute: Routes = [
    {
        path: 'user-person-set/:id/delete',
        component: UserPersonSetDeletePopupComponent,
        resolve: {
            userPersonSet: UserPersonSetResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'gluscoApp.userPersonSet.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
