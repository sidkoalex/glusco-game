import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IPersonCar } from 'app/shared/model/person-car.model';

@Component({
    selector: 'jhi-person-car-detail',
    templateUrl: './person-car-detail.component.html'
})
export class PersonCarDetailComponent implements OnInit {
    personCar: IPersonCar;

    constructor(private activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ personCar }) => {
            this.personCar = personCar;
        });
    }

    previousState() {
        window.history.back();
    }
}
