import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { GluscoSharedModule } from 'app/shared';
import {
    PersonCarComponent,
    PersonCarDetailComponent,
    PersonCarUpdateComponent,
    PersonCarDeletePopupComponent,
    PersonCarDeleteDialogComponent,
    personCarRoute,
    personCarPopupRoute
} from './';

const ENTITY_STATES = [...personCarRoute, ...personCarPopupRoute];

@NgModule({
    imports: [GluscoSharedModule, RouterModule.forChild(ENTITY_STATES)],
    declarations: [
        PersonCarComponent,
        PersonCarDetailComponent,
        PersonCarUpdateComponent,
        PersonCarDeleteDialogComponent,
        PersonCarDeletePopupComponent
    ],
    entryComponents: [PersonCarComponent, PersonCarUpdateComponent, PersonCarDeleteDialogComponent, PersonCarDeletePopupComponent],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class GluscoPersonCarModule {}
