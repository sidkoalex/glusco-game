import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { IPersonCar } from 'app/shared/model/person-car.model';

type EntityResponseType = HttpResponse<IPersonCar>;
type EntityArrayResponseType = HttpResponse<IPersonCar[]>;

@Injectable({ providedIn: 'root' })
export class PersonCarService {
    private resourceUrl = SERVER_API_URL + 'api/person-cars';

    constructor(private http: HttpClient) {}

    create(personCar: IPersonCar): Observable<EntityResponseType> {
        return this.http.post<IPersonCar>(this.resourceUrl, personCar, { observe: 'response' });
    }

    update(personCar: IPersonCar): Observable<EntityResponseType> {
        return this.http.put<IPersonCar>(this.resourceUrl, personCar, { observe: 'response' });
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<IPersonCar>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    query(req?: any): Observable<EntityArrayResponseType> {
        const options = createRequestOption(req);
        return this.http.get<IPersonCar[]>(this.resourceUrl, { params: options, observe: 'response' });
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }
}
