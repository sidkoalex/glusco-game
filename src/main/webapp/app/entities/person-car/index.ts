export * from './person-car.service';
export * from './person-car-update.component';
export * from './person-car-delete-dialog.component';
export * from './person-car-detail.component';
export * from './person-car.component';
export * from './person-car.route';
