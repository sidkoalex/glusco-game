import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { JhiAlertService } from 'ng-jhipster';

import { IPersonCar } from 'app/shared/model/person-car.model';
import { PersonCarService } from './person-car.service';
import { IPerson } from 'app/shared/model/person.model';
import { PersonService } from 'app/entities/person';
import { ICar } from 'app/shared/model/car.model';
import { CarService } from 'app/entities/car';

@Component({
    selector: 'jhi-person-car-update',
    templateUrl: './person-car-update.component.html'
})
export class PersonCarUpdateComponent implements OnInit {
    private _personCar: IPersonCar;
    isSaving: boolean;

    people: IPerson[];

    cars: ICar[];

    constructor(
        private jhiAlertService: JhiAlertService,
        private personCarService: PersonCarService,
        private personService: PersonService,
        private carService: CarService,
        private activatedRoute: ActivatedRoute
    ) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ personCar }) => {
            this.personCar = personCar;
        });
        this.personService.query().subscribe(
            (res: HttpResponse<IPerson[]>) => {
                this.people = res.body;
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
        this.carService.query().subscribe(
            (res: HttpResponse<ICar[]>) => {
                this.cars = res.body;
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        if (this.personCar.id !== undefined) {
            this.subscribeToSaveResponse(this.personCarService.update(this.personCar));
        } else {
            this.subscribeToSaveResponse(this.personCarService.create(this.personCar));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<IPersonCar>>) {
        result.subscribe((res: HttpResponse<IPersonCar>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }

    trackPersonById(index: number, item: IPerson) {
        return item.id;
    }

    trackCarById(index: number, item: ICar) {
        return item.id;
    }
    get personCar() {
        return this._personCar;
    }

    set personCar(personCar: IPersonCar) {
        this._personCar = personCar;
    }
}
