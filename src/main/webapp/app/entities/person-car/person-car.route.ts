import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil, JhiResolvePagingParams } from 'ng-jhipster';
import { UserRouteAccessService } from 'app/core';
import { of } from 'rxjs';
import { map } from 'rxjs/operators';
import { PersonCar } from 'app/shared/model/person-car.model';
import { PersonCarService } from './person-car.service';
import { PersonCarComponent } from './person-car.component';
import { PersonCarDetailComponent } from './person-car-detail.component';
import { PersonCarUpdateComponent } from './person-car-update.component';
import { PersonCarDeletePopupComponent } from './person-car-delete-dialog.component';
import { IPersonCar } from 'app/shared/model/person-car.model';

@Injectable({ providedIn: 'root' })
export class PersonCarResolve implements Resolve<IPersonCar> {
    constructor(private service: PersonCarService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const id = route.params['id'] ? route.params['id'] : null;
        if (id) {
            return this.service.find(id).pipe(map((personCar: HttpResponse<PersonCar>) => personCar.body));
        }
        return of(new PersonCar());
    }
}

export const personCarRoute: Routes = [
    {
        path: 'person-car',
        component: PersonCarComponent,
        resolve: {
            pagingParams: JhiResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            defaultSort: 'id,asc',
            pageTitle: 'gluscoApp.personCar.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'person-car/:id/view',
        component: PersonCarDetailComponent,
        resolve: {
            personCar: PersonCarResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'gluscoApp.personCar.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'person-car/new',
        component: PersonCarUpdateComponent,
        resolve: {
            personCar: PersonCarResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'gluscoApp.personCar.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'person-car/:id/edit',
        component: PersonCarUpdateComponent,
        resolve: {
            personCar: PersonCarResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'gluscoApp.personCar.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const personCarPopupRoute: Routes = [
    {
        path: 'person-car/:id/delete',
        component: PersonCarDeletePopupComponent,
        resolve: {
            personCar: PersonCarResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'gluscoApp.personCar.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
