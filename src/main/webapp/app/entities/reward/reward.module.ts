import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { GluscoSharedModule } from 'app/shared';
import {
    RewardComponent,
    RewardDetailComponent,
    RewardUpdateComponent,
    RewardDeletePopupComponent,
    RewardDeleteDialogComponent,
    rewardRoute,
    rewardPopupRoute
} from './';

const ENTITY_STATES = [...rewardRoute, ...rewardPopupRoute];

@NgModule({
    imports: [GluscoSharedModule, RouterModule.forChild(ENTITY_STATES)],
    declarations: [RewardComponent, RewardDetailComponent, RewardUpdateComponent, RewardDeleteDialogComponent, RewardDeletePopupComponent],
    entryComponents: [RewardComponent, RewardUpdateComponent, RewardDeleteDialogComponent, RewardDeletePopupComponent],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class GluscoRewardModule {}
