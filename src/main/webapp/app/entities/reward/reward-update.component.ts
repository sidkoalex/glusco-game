import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { IReward } from 'app/shared/model/reward.model';
import { RewardService } from './reward.service';

@Component({
    selector: 'jhi-reward-update',
    templateUrl: './reward-update.component.html'
})
export class RewardUpdateComponent implements OnInit {
    private _reward: IReward;
    isSaving: boolean;

    constructor(private rewardService: RewardService, private activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ reward }) => {
            this.reward = reward;
        });
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        if (this.reward.id !== undefined) {
            this.subscribeToSaveResponse(this.rewardService.update(this.reward));
        } else {
            this.subscribeToSaveResponse(this.rewardService.create(this.reward));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<IReward>>) {
        result.subscribe((res: HttpResponse<IReward>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    private onSaveError() {
        this.isSaving = false;
    }
    get reward() {
        return this._reward;
    }

    set reward(reward: IReward) {
        this._reward = reward;
    }
}
