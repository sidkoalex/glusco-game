import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil, JhiResolvePagingParams } from 'ng-jhipster';
import { UserRouteAccessService } from 'app/core';
import { of } from 'rxjs';
import { map } from 'rxjs/operators';
import { Race } from 'app/shared/model/race.model';
import { RaceService } from './race.service';
import { RaceComponent } from './race.component';
import { RaceDetailComponent } from './race-detail.component';
import { RaceUpdateComponent } from './race-update.component';
import { RaceDeletePopupComponent } from './race-delete-dialog.component';
import { IRace } from 'app/shared/model/race.model';

@Injectable({ providedIn: 'root' })
export class RaceResolve implements Resolve<IRace> {
    constructor(private service: RaceService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const id = route.params['id'] ? route.params['id'] : null;
        if (id) {
            return this.service.find(id).pipe(map((race: HttpResponse<Race>) => race.body));
        }
        return of(new Race());
    }
}

export const raceRoute: Routes = [
    {
        path: 'race',
        component: RaceComponent,
        resolve: {
            pagingParams: JhiResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            defaultSort: 'id,asc',
            pageTitle: 'gluscoApp.race.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'race/:id/view',
        component: RaceDetailComponent,
        resolve: {
            race: RaceResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'gluscoApp.race.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'race/new',
        component: RaceUpdateComponent,
        resolve: {
            race: RaceResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'gluscoApp.race.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'race/:id/edit',
        component: RaceUpdateComponent,
        resolve: {
            race: RaceResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'gluscoApp.race.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const racePopupRoute: Routes = [
    {
        path: 'race/:id/delete',
        component: RaceDeletePopupComponent,
        resolve: {
            race: RaceResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'gluscoApp.race.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
