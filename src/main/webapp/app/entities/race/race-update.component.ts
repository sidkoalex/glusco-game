import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';
import { JhiAlertService } from 'ng-jhipster';

import { IRace } from 'app/shared/model/race.model';
import { RaceService } from './race.service';
import { IUser, UserService } from 'app/core';
import { IRaceLocation } from 'app/shared/model/race-location.model';
import { RaceLocationService } from 'app/entities/race-location';
import { IRaceItem } from 'app/shared/model/race-item.model';
import { RaceItemService } from 'app/entities/race-item';

@Component({
    selector: 'jhi-race-update',
    templateUrl: './race-update.component.html'
})
export class RaceUpdateComponent implements OnInit {
    private _race: IRace;
    isSaving: boolean;

    users: IUser[];

    racelocations: IRaceLocation[];

    raceitems: IRaceItem[];
    createdAt: string;

    constructor(
        private jhiAlertService: JhiAlertService,
        private raceService: RaceService,
        private userService: UserService,
        private raceLocationService: RaceLocationService,
        private raceItemService: RaceItemService,
        private activatedRoute: ActivatedRoute
    ) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ race }) => {
            this.race = race;
        });
        this.userService.query().subscribe(
            (res: HttpResponse<IUser[]>) => {
                this.users = res.body;
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
        this.raceLocationService.query().subscribe(
            (res: HttpResponse<IRaceLocation[]>) => {
                this.racelocations = res.body;
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
        this.raceItemService.query().subscribe(
            (res: HttpResponse<IRaceItem[]>) => {
                this.raceitems = res.body;
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        this.race.createdAt = moment(this.createdAt, DATE_TIME_FORMAT);
        if (this.race.id !== undefined) {
            this.subscribeToSaveResponse(this.raceService.update(this.race));
        } else {
            this.subscribeToSaveResponse(this.raceService.create(this.race));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<IRace>>) {
        result.subscribe((res: HttpResponse<IRace>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }

    trackUserById(index: number, item: IUser) {
        return item.id;
    }

    trackRaceLocationById(index: number, item: IRaceLocation) {
        return item.id;
    }

    trackRaceItemById(index: number, item: IRaceItem) {
        return item.id;
    }
    get race() {
        return this._race;
    }

    set race(race: IRace) {
        this._race = race;
        this.createdAt = moment(race.createdAt).format(DATE_TIME_FORMAT);
    }
}
