import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

import { GluscoClothesModule } from './clothes/clothes.module';
import { GluscoPersonModule } from './person/person.module';
import { GluscoRewardModule } from './reward/reward.module';
import { GluscoRaceLocationModule } from './race-location/race-location.module';
import { GluscoRaceItemModule } from './race-item/race-item.module';
import { GluscoRaceLocationItemModule } from './race-location-item/race-location-item.module';
import { GluscoCarModule } from './car/car.module';
import { GluscoPersonCarModule } from './person-car/person-car.module';
import { GluscoPersonClothesModule } from './person-clothes/person-clothes.module';
import { GluscoUserPersonSetModule } from './user-person-set/user-person-set.module';
import { GluscoUserCarSetModule } from './user-car-set/user-car-set.module';
import { GluscoRaceModule } from './race/race.module';
import { GluscoRaceRewardModule } from './race-reward/race-reward.module';
/* jhipster-needle-add-entity-module-import - JHipster will add entity modules imports here */

@NgModule({
    // prettier-ignore
    imports: [
        GluscoClothesModule,
        GluscoPersonModule,
        GluscoRewardModule,
        GluscoRaceLocationModule,
        GluscoRaceItemModule,
        GluscoRaceLocationItemModule,
        GluscoCarModule,
        GluscoPersonCarModule,
        GluscoPersonClothesModule,
        GluscoUserPersonSetModule,
        GluscoUserCarSetModule,
        GluscoRaceModule,
        GluscoRaceRewardModule,
        /* jhipster-needle-add-entity-module - JHipster will add entity modules here */
    ],
    declarations: [],
    entryComponents: [],
    providers: [],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class GluscoEntityModule {}
