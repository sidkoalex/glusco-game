import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { JhiAlertService } from 'ng-jhipster';

import { IUserCarSet } from 'app/shared/model/user-car-set.model';
import { UserCarSetService } from './user-car-set.service';
import { ICar } from 'app/shared/model/car.model';
import { CarService } from 'app/entities/car';
import { IUserPersonSet } from 'app/shared/model/user-person-set.model';
import { UserPersonSetService } from 'app/entities/user-person-set';

@Component({
    selector: 'jhi-user-car-set-update',
    templateUrl: './user-car-set-update.component.html'
})
export class UserCarSetUpdateComponent implements OnInit {
    private _userCarSet: IUserCarSet;
    isSaving: boolean;

    cars: ICar[];

    userpersonsets: IUserPersonSet[];

    constructor(
        private jhiAlertService: JhiAlertService,
        private userCarSetService: UserCarSetService,
        private carService: CarService,
        private userPersonSetService: UserPersonSetService,
        private activatedRoute: ActivatedRoute
    ) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ userCarSet }) => {
            this.userCarSet = userCarSet;
        });
        this.carService.query().subscribe(
            (res: HttpResponse<ICar[]>) => {
                this.cars = res.body;
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
        this.userPersonSetService.query().subscribe(
            (res: HttpResponse<IUserPersonSet[]>) => {
                this.userpersonsets = res.body;
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        if (this.userCarSet.id !== undefined) {
            this.subscribeToSaveResponse(this.userCarSetService.update(this.userCarSet));
        } else {
            this.subscribeToSaveResponse(this.userCarSetService.create(this.userCarSet));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<IUserCarSet>>) {
        result.subscribe((res: HttpResponse<IUserCarSet>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }

    trackCarById(index: number, item: ICar) {
        return item.id;
    }

    trackUserPersonSetById(index: number, item: IUserPersonSet) {
        return item.id;
    }
    get userCarSet() {
        return this._userCarSet;
    }

    set userCarSet(userCarSet: IUserCarSet) {
        this._userCarSet = userCarSet;
    }
}
