import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { GluscoSharedModule } from 'app/shared';
import {
    UserCarSetComponent,
    UserCarSetDetailComponent,
    UserCarSetUpdateComponent,
    UserCarSetDeletePopupComponent,
    UserCarSetDeleteDialogComponent,
    userCarSetRoute,
    userCarSetPopupRoute
} from './';

const ENTITY_STATES = [...userCarSetRoute, ...userCarSetPopupRoute];

@NgModule({
    imports: [GluscoSharedModule, RouterModule.forChild(ENTITY_STATES)],
    declarations: [
        UserCarSetComponent,
        UserCarSetDetailComponent,
        UserCarSetUpdateComponent,
        UserCarSetDeleteDialogComponent,
        UserCarSetDeletePopupComponent
    ],
    entryComponents: [UserCarSetComponent, UserCarSetUpdateComponent, UserCarSetDeleteDialogComponent, UserCarSetDeletePopupComponent],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class GluscoUserCarSetModule {}
