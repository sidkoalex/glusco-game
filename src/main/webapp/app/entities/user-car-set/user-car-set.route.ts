import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil, JhiResolvePagingParams } from 'ng-jhipster';
import { UserRouteAccessService } from 'app/core';
import { of } from 'rxjs';
import { map } from 'rxjs/operators';
import { UserCarSet } from 'app/shared/model/user-car-set.model';
import { UserCarSetService } from './user-car-set.service';
import { UserCarSetComponent } from './user-car-set.component';
import { UserCarSetDetailComponent } from './user-car-set-detail.component';
import { UserCarSetUpdateComponent } from './user-car-set-update.component';
import { UserCarSetDeletePopupComponent } from './user-car-set-delete-dialog.component';
import { IUserCarSet } from 'app/shared/model/user-car-set.model';

@Injectable({ providedIn: 'root' })
export class UserCarSetResolve implements Resolve<IUserCarSet> {
    constructor(private service: UserCarSetService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const id = route.params['id'] ? route.params['id'] : null;
        if (id) {
            return this.service.find(id).pipe(map((userCarSet: HttpResponse<UserCarSet>) => userCarSet.body));
        }
        return of(new UserCarSet());
    }
}

export const userCarSetRoute: Routes = [
    {
        path: 'user-car-set',
        component: UserCarSetComponent,
        resolve: {
            pagingParams: JhiResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            defaultSort: 'id,asc',
            pageTitle: 'gluscoApp.userCarSet.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'user-car-set/:id/view',
        component: UserCarSetDetailComponent,
        resolve: {
            userCarSet: UserCarSetResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'gluscoApp.userCarSet.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'user-car-set/new',
        component: UserCarSetUpdateComponent,
        resolve: {
            userCarSet: UserCarSetResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'gluscoApp.userCarSet.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'user-car-set/:id/edit',
        component: UserCarSetUpdateComponent,
        resolve: {
            userCarSet: UserCarSetResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'gluscoApp.userCarSet.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const userCarSetPopupRoute: Routes = [
    {
        path: 'user-car-set/:id/delete',
        component: UserCarSetDeletePopupComponent,
        resolve: {
            userCarSet: UserCarSetResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'gluscoApp.userCarSet.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
