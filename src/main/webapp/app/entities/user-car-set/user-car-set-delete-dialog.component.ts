import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IUserCarSet } from 'app/shared/model/user-car-set.model';
import { UserCarSetService } from './user-car-set.service';

@Component({
    selector: 'jhi-user-car-set-delete-dialog',
    templateUrl: './user-car-set-delete-dialog.component.html'
})
export class UserCarSetDeleteDialogComponent {
    userCarSet: IUserCarSet;

    constructor(private userCarSetService: UserCarSetService, public activeModal: NgbActiveModal, private eventManager: JhiEventManager) {}

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.userCarSetService.delete(id).subscribe(response => {
            this.eventManager.broadcast({
                name: 'userCarSetListModification',
                content: 'Deleted an userCarSet'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-user-car-set-delete-popup',
    template: ''
})
export class UserCarSetDeletePopupComponent implements OnInit, OnDestroy {
    private ngbModalRef: NgbModalRef;

    constructor(private activatedRoute: ActivatedRoute, private router: Router, private modalService: NgbModal) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ userCarSet }) => {
            setTimeout(() => {
                this.ngbModalRef = this.modalService.open(UserCarSetDeleteDialogComponent as Component, { size: 'lg', backdrop: 'static' });
                this.ngbModalRef.componentInstance.userCarSet = userCarSet;
                this.ngbModalRef.result.then(
                    result => {
                        this.router.navigate([{ outlets: { popup: null } }], { replaceUrl: true, queryParamsHandling: 'merge' });
                        this.ngbModalRef = null;
                    },
                    reason => {
                        this.router.navigate([{ outlets: { popup: null } }], { replaceUrl: true, queryParamsHandling: 'merge' });
                        this.ngbModalRef = null;
                    }
                );
            }, 0);
        });
    }

    ngOnDestroy() {
        this.ngbModalRef = null;
    }
}
