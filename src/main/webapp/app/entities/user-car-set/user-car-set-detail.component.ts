import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IUserCarSet } from 'app/shared/model/user-car-set.model';

@Component({
    selector: 'jhi-user-car-set-detail',
    templateUrl: './user-car-set-detail.component.html'
})
export class UserCarSetDetailComponent implements OnInit {
    userCarSet: IUserCarSet;

    constructor(private activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ userCarSet }) => {
            this.userCarSet = userCarSet;
        });
    }

    previousState() {
        window.history.back();
    }
}
