import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil, JhiResolvePagingParams } from 'ng-jhipster';
import { UserRouteAccessService } from 'app/core';
import { of } from 'rxjs';
import { map } from 'rxjs/operators';
import { RaceLocation } from 'app/shared/model/race-location.model';
import { RaceLocationService } from './race-location.service';
import { RaceLocationComponent } from './race-location.component';
import { RaceLocationDetailComponent } from './race-location-detail.component';
import { RaceLocationUpdateComponent } from './race-location-update.component';
import { RaceLocationDeletePopupComponent } from './race-location-delete-dialog.component';
import { IRaceLocation } from 'app/shared/model/race-location.model';

@Injectable({ providedIn: 'root' })
export class RaceLocationResolve implements Resolve<IRaceLocation> {
    constructor(private service: RaceLocationService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const id = route.params['id'] ? route.params['id'] : null;
        if (id) {
            return this.service.find(id).pipe(map((raceLocation: HttpResponse<RaceLocation>) => raceLocation.body));
        }
        return of(new RaceLocation());
    }
}

export const raceLocationRoute: Routes = [
    {
        path: 'race-location',
        component: RaceLocationComponent,
        resolve: {
            pagingParams: JhiResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            defaultSort: 'id,asc',
            pageTitle: 'gluscoApp.raceLocation.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'race-location/:id/view',
        component: RaceLocationDetailComponent,
        resolve: {
            raceLocation: RaceLocationResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'gluscoApp.raceLocation.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'race-location/new',
        component: RaceLocationUpdateComponent,
        resolve: {
            raceLocation: RaceLocationResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'gluscoApp.raceLocation.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'race-location/:id/edit',
        component: RaceLocationUpdateComponent,
        resolve: {
            raceLocation: RaceLocationResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'gluscoApp.raceLocation.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const raceLocationPopupRoute: Routes = [
    {
        path: 'race-location/:id/delete',
        component: RaceLocationDeletePopupComponent,
        resolve: {
            raceLocation: RaceLocationResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'gluscoApp.raceLocation.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
