import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { IRaceLocation } from 'app/shared/model/race-location.model';
import { RaceLocationService } from './race-location.service';

@Component({
    selector: 'jhi-race-location-update',
    templateUrl: './race-location-update.component.html'
})
export class RaceLocationUpdateComponent implements OnInit {
    private _raceLocation: IRaceLocation;
    isSaving: boolean;

    constructor(private raceLocationService: RaceLocationService, private activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ raceLocation }) => {
            this.raceLocation = raceLocation;
        });
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        if (this.raceLocation.id !== undefined) {
            this.subscribeToSaveResponse(this.raceLocationService.update(this.raceLocation));
        } else {
            this.subscribeToSaveResponse(this.raceLocationService.create(this.raceLocation));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<IRaceLocation>>) {
        result.subscribe((res: HttpResponse<IRaceLocation>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    private onSaveError() {
        this.isSaving = false;
    }
    get raceLocation() {
        return this._raceLocation;
    }

    set raceLocation(raceLocation: IRaceLocation) {
        this._raceLocation = raceLocation;
    }
}
