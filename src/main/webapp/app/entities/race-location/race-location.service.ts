import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { IRaceLocation } from 'app/shared/model/race-location.model';

type EntityResponseType = HttpResponse<IRaceLocation>;
type EntityArrayResponseType = HttpResponse<IRaceLocation[]>;

@Injectable({ providedIn: 'root' })
export class RaceLocationService {
    private resourceUrl = SERVER_API_URL + 'api/race-locations';

    constructor(private http: HttpClient) {}

    create(raceLocation: IRaceLocation): Observable<EntityResponseType> {
        return this.http.post<IRaceLocation>(this.resourceUrl, raceLocation, { observe: 'response' });
    }

    update(raceLocation: IRaceLocation): Observable<EntityResponseType> {
        return this.http.put<IRaceLocation>(this.resourceUrl, raceLocation, { observe: 'response' });
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<IRaceLocation>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    query(req?: any): Observable<EntityArrayResponseType> {
        const options = createRequestOption(req);
        return this.http.get<IRaceLocation[]>(this.resourceUrl, { params: options, observe: 'response' });
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }
}
