import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { GluscoSharedModule } from 'app/shared';
import {
    RaceLocationComponent,
    RaceLocationDetailComponent,
    RaceLocationUpdateComponent,
    RaceLocationDeletePopupComponent,
    RaceLocationDeleteDialogComponent,
    raceLocationRoute,
    raceLocationPopupRoute
} from './';

const ENTITY_STATES = [...raceLocationRoute, ...raceLocationPopupRoute];

@NgModule({
    imports: [GluscoSharedModule, RouterModule.forChild(ENTITY_STATES)],
    declarations: [
        RaceLocationComponent,
        RaceLocationDetailComponent,
        RaceLocationUpdateComponent,
        RaceLocationDeleteDialogComponent,
        RaceLocationDeletePopupComponent
    ],
    entryComponents: [
        RaceLocationComponent,
        RaceLocationUpdateComponent,
        RaceLocationDeleteDialogComponent,
        RaceLocationDeletePopupComponent
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class GluscoRaceLocationModule {}
