import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IRaceLocation } from 'app/shared/model/race-location.model';

@Component({
    selector: 'jhi-race-location-detail',
    templateUrl: './race-location-detail.component.html'
})
export class RaceLocationDetailComponent implements OnInit {
    raceLocation: IRaceLocation;

    constructor(private activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ raceLocation }) => {
            this.raceLocation = raceLocation;
        });
    }

    previousState() {
        window.history.back();
    }
}
