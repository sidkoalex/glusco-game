import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IRaceItem } from 'app/shared/model/race-item.model';

@Component({
    selector: 'jhi-race-item-detail',
    templateUrl: './race-item-detail.component.html'
})
export class RaceItemDetailComponent implements OnInit {
    raceItem: IRaceItem;

    constructor(private activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ raceItem }) => {
            this.raceItem = raceItem;
        });
    }

    previousState() {
        window.history.back();
    }
}
