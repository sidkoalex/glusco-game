import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { IRaceItem } from 'app/shared/model/race-item.model';
import { RaceItemService } from './race-item.service';

@Component({
    selector: 'jhi-race-item-update',
    templateUrl: './race-item-update.component.html'
})
export class RaceItemUpdateComponent implements OnInit {
    private _raceItem: IRaceItem;
    isSaving: boolean;

    constructor(private raceItemService: RaceItemService, private activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ raceItem }) => {
            this.raceItem = raceItem;
        });
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        if (this.raceItem.id !== undefined) {
            this.subscribeToSaveResponse(this.raceItemService.update(this.raceItem));
        } else {
            this.subscribeToSaveResponse(this.raceItemService.create(this.raceItem));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<IRaceItem>>) {
        result.subscribe((res: HttpResponse<IRaceItem>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    private onSaveError() {
        this.isSaving = false;
    }
    get raceItem() {
        return this._raceItem;
    }

    set raceItem(raceItem: IRaceItem) {
        this._raceItem = raceItem;
    }
}
