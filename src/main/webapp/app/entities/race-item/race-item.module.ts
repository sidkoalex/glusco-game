import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { GluscoSharedModule } from 'app/shared';
import {
    RaceItemComponent,
    RaceItemDetailComponent,
    RaceItemUpdateComponent,
    RaceItemDeletePopupComponent,
    RaceItemDeleteDialogComponent,
    raceItemRoute,
    raceItemPopupRoute
} from './';

const ENTITY_STATES = [...raceItemRoute, ...raceItemPopupRoute];

@NgModule({
    imports: [GluscoSharedModule, RouterModule.forChild(ENTITY_STATES)],
    declarations: [
        RaceItemComponent,
        RaceItemDetailComponent,
        RaceItemUpdateComponent,
        RaceItemDeleteDialogComponent,
        RaceItemDeletePopupComponent
    ],
    entryComponents: [RaceItemComponent, RaceItemUpdateComponent, RaceItemDeleteDialogComponent, RaceItemDeletePopupComponent],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class GluscoRaceItemModule {}
