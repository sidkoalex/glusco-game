import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil, JhiResolvePagingParams } from 'ng-jhipster';
import { UserRouteAccessService } from 'app/core';
import { of } from 'rxjs';
import { map } from 'rxjs/operators';
import { RaceItem } from 'app/shared/model/race-item.model';
import { RaceItemService } from './race-item.service';
import { RaceItemComponent } from './race-item.component';
import { RaceItemDetailComponent } from './race-item-detail.component';
import { RaceItemUpdateComponent } from './race-item-update.component';
import { RaceItemDeletePopupComponent } from './race-item-delete-dialog.component';
import { IRaceItem } from 'app/shared/model/race-item.model';

@Injectable({ providedIn: 'root' })
export class RaceItemResolve implements Resolve<IRaceItem> {
    constructor(private service: RaceItemService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const id = route.params['id'] ? route.params['id'] : null;
        if (id) {
            return this.service.find(id).pipe(map((raceItem: HttpResponse<RaceItem>) => raceItem.body));
        }
        return of(new RaceItem());
    }
}

export const raceItemRoute: Routes = [
    {
        path: 'race-item',
        component: RaceItemComponent,
        resolve: {
            pagingParams: JhiResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            defaultSort: 'id,asc',
            pageTitle: 'gluscoApp.raceItem.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'race-item/:id/view',
        component: RaceItemDetailComponent,
        resolve: {
            raceItem: RaceItemResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'gluscoApp.raceItem.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'race-item/new',
        component: RaceItemUpdateComponent,
        resolve: {
            raceItem: RaceItemResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'gluscoApp.raceItem.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'race-item/:id/edit',
        component: RaceItemUpdateComponent,
        resolve: {
            raceItem: RaceItemResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'gluscoApp.raceItem.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const raceItemPopupRoute: Routes = [
    {
        path: 'race-item/:id/delete',
        component: RaceItemDeletePopupComponent,
        resolve: {
            raceItem: RaceItemResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'gluscoApp.raceItem.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
