import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { IRaceItem } from 'app/shared/model/race-item.model';

type EntityResponseType = HttpResponse<IRaceItem>;
type EntityArrayResponseType = HttpResponse<IRaceItem[]>;

@Injectable({ providedIn: 'root' })
export class RaceItemService {
    private resourceUrl = SERVER_API_URL + 'api/race-items';

    constructor(private http: HttpClient) {}

    create(raceItem: IRaceItem): Observable<EntityResponseType> {
        return this.http.post<IRaceItem>(this.resourceUrl, raceItem, { observe: 'response' });
    }

    update(raceItem: IRaceItem): Observable<EntityResponseType> {
        return this.http.put<IRaceItem>(this.resourceUrl, raceItem, { observe: 'response' });
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<IRaceItem>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    query(req?: any): Observable<EntityArrayResponseType> {
        const options = createRequestOption(req);
        return this.http.get<IRaceItem[]>(this.resourceUrl, { params: options, observe: 'response' });
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }
}
