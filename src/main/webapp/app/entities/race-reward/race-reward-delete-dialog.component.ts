import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IRaceReward } from 'app/shared/model/race-reward.model';
import { RaceRewardService } from './race-reward.service';

@Component({
    selector: 'jhi-race-reward-delete-dialog',
    templateUrl: './race-reward-delete-dialog.component.html'
})
export class RaceRewardDeleteDialogComponent {
    raceReward: IRaceReward;

    constructor(private raceRewardService: RaceRewardService, public activeModal: NgbActiveModal, private eventManager: JhiEventManager) {}

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.raceRewardService.delete(id).subscribe(response => {
            this.eventManager.broadcast({
                name: 'raceRewardListModification',
                content: 'Deleted an raceReward'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-race-reward-delete-popup',
    template: ''
})
export class RaceRewardDeletePopupComponent implements OnInit, OnDestroy {
    private ngbModalRef: NgbModalRef;

    constructor(private activatedRoute: ActivatedRoute, private router: Router, private modalService: NgbModal) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ raceReward }) => {
            setTimeout(() => {
                this.ngbModalRef = this.modalService.open(RaceRewardDeleteDialogComponent as Component, { size: 'lg', backdrop: 'static' });
                this.ngbModalRef.componentInstance.raceReward = raceReward;
                this.ngbModalRef.result.then(
                    result => {
                        this.router.navigate([{ outlets: { popup: null } }], { replaceUrl: true, queryParamsHandling: 'merge' });
                        this.ngbModalRef = null;
                    },
                    reason => {
                        this.router.navigate([{ outlets: { popup: null } }], { replaceUrl: true, queryParamsHandling: 'merge' });
                        this.ngbModalRef = null;
                    }
                );
            }, 0);
        });
    }

    ngOnDestroy() {
        this.ngbModalRef = null;
    }
}
