import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil, JhiResolvePagingParams } from 'ng-jhipster';
import { UserRouteAccessService } from 'app/core';
import { of } from 'rxjs';
import { map } from 'rxjs/operators';
import { RaceReward } from 'app/shared/model/race-reward.model';
import { RaceRewardService } from './race-reward.service';
import { RaceRewardComponent } from './race-reward.component';
import { RaceRewardDetailComponent } from './race-reward-detail.component';
import { RaceRewardUpdateComponent } from './race-reward-update.component';
import { RaceRewardDeletePopupComponent } from './race-reward-delete-dialog.component';
import { IRaceReward } from 'app/shared/model/race-reward.model';

@Injectable({ providedIn: 'root' })
export class RaceRewardResolve implements Resolve<IRaceReward> {
    constructor(private service: RaceRewardService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const id = route.params['id'] ? route.params['id'] : null;
        if (id) {
            return this.service.find(id).pipe(map((raceReward: HttpResponse<RaceReward>) => raceReward.body));
        }
        return of(new RaceReward());
    }
}

export const raceRewardRoute: Routes = [
    {
        path: 'race-reward',
        component: RaceRewardComponent,
        resolve: {
            pagingParams: JhiResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            defaultSort: 'id,asc',
            pageTitle: 'gluscoApp.raceReward.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'race-reward/:id/view',
        component: RaceRewardDetailComponent,
        resolve: {
            raceReward: RaceRewardResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'gluscoApp.raceReward.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'race-reward/new',
        component: RaceRewardUpdateComponent,
        resolve: {
            raceReward: RaceRewardResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'gluscoApp.raceReward.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'race-reward/:id/edit',
        component: RaceRewardUpdateComponent,
        resolve: {
            raceReward: RaceRewardResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'gluscoApp.raceReward.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const raceRewardPopupRoute: Routes = [
    {
        path: 'race-reward/:id/delete',
        component: RaceRewardDeletePopupComponent,
        resolve: {
            raceReward: RaceRewardResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'gluscoApp.raceReward.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
