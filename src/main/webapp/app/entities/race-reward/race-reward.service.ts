import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { IRaceReward } from 'app/shared/model/race-reward.model';

type EntityResponseType = HttpResponse<IRaceReward>;
type EntityArrayResponseType = HttpResponse<IRaceReward[]>;

@Injectable({ providedIn: 'root' })
export class RaceRewardService {
    private resourceUrl = SERVER_API_URL + 'api/race-rewards';

    constructor(private http: HttpClient) {}

    create(raceReward: IRaceReward): Observable<EntityResponseType> {
        return this.http.post<IRaceReward>(this.resourceUrl, raceReward, { observe: 'response' });
    }

    update(raceReward: IRaceReward): Observable<EntityResponseType> {
        return this.http.put<IRaceReward>(this.resourceUrl, raceReward, { observe: 'response' });
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<IRaceReward>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    query(req?: any): Observable<EntityArrayResponseType> {
        const options = createRequestOption(req);
        return this.http.get<IRaceReward[]>(this.resourceUrl, { params: options, observe: 'response' });
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }
}
