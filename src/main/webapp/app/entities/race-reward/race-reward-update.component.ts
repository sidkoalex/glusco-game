import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { JhiAlertService } from 'ng-jhipster';

import { IRaceReward } from 'app/shared/model/race-reward.model';
import { RaceRewardService } from './race-reward.service';
import { IRace } from 'app/shared/model/race.model';
import { RaceService } from 'app/entities/race';
import { IReward } from 'app/shared/model/reward.model';
import { RewardService } from 'app/entities/reward';

@Component({
    selector: 'jhi-race-reward-update',
    templateUrl: './race-reward-update.component.html'
})
export class RaceRewardUpdateComponent implements OnInit {
    private _raceReward: IRaceReward;
    isSaving: boolean;

    races: IRace[];

    rewards: IReward[];

    constructor(
        private jhiAlertService: JhiAlertService,
        private raceRewardService: RaceRewardService,
        private raceService: RaceService,
        private rewardService: RewardService,
        private activatedRoute: ActivatedRoute
    ) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ raceReward }) => {
            this.raceReward = raceReward;
        });
        this.raceService.query().subscribe(
            (res: HttpResponse<IRace[]>) => {
                this.races = res.body;
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
        this.rewardService.query().subscribe(
            (res: HttpResponse<IReward[]>) => {
                this.rewards = res.body;
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        if (this.raceReward.id !== undefined) {
            this.subscribeToSaveResponse(this.raceRewardService.update(this.raceReward));
        } else {
            this.subscribeToSaveResponse(this.raceRewardService.create(this.raceReward));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<IRaceReward>>) {
        result.subscribe((res: HttpResponse<IRaceReward>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }

    trackRaceById(index: number, item: IRace) {
        return item.id;
    }

    trackRewardById(index: number, item: IReward) {
        return item.id;
    }
    get raceReward() {
        return this._raceReward;
    }

    set raceReward(raceReward: IRaceReward) {
        this._raceReward = raceReward;
    }
}
