import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IRaceReward } from 'app/shared/model/race-reward.model';

@Component({
    selector: 'jhi-race-reward-detail',
    templateUrl: './race-reward-detail.component.html'
})
export class RaceRewardDetailComponent implements OnInit {
    raceReward: IRaceReward;

    constructor(private activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ raceReward }) => {
            this.raceReward = raceReward;
        });
    }

    previousState() {
        window.history.back();
    }
}
