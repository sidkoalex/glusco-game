import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { GluscoSharedModule } from 'app/shared';
import {
    RaceRewardComponent,
    RaceRewardDetailComponent,
    RaceRewardUpdateComponent,
    RaceRewardDeletePopupComponent,
    RaceRewardDeleteDialogComponent,
    raceRewardRoute,
    raceRewardPopupRoute
} from './';

const ENTITY_STATES = [...raceRewardRoute, ...raceRewardPopupRoute];

@NgModule({
    imports: [GluscoSharedModule, RouterModule.forChild(ENTITY_STATES)],
    declarations: [
        RaceRewardComponent,
        RaceRewardDetailComponent,
        RaceRewardUpdateComponent,
        RaceRewardDeleteDialogComponent,
        RaceRewardDeletePopupComponent
    ],
    entryComponents: [RaceRewardComponent, RaceRewardUpdateComponent, RaceRewardDeleteDialogComponent, RaceRewardDeletePopupComponent],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class GluscoRaceRewardModule {}
