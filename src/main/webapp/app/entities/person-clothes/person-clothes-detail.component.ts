import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IPersonClothes } from 'app/shared/model/person-clothes.model';

@Component({
    selector: 'jhi-person-clothes-detail',
    templateUrl: './person-clothes-detail.component.html'
})
export class PersonClothesDetailComponent implements OnInit {
    personClothes: IPersonClothes;

    constructor(private activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ personClothes }) => {
            this.personClothes = personClothes;
        });
    }

    previousState() {
        window.history.back();
    }
}
