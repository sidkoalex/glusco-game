import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { IPersonClothes } from 'app/shared/model/person-clothes.model';

type EntityResponseType = HttpResponse<IPersonClothes>;
type EntityArrayResponseType = HttpResponse<IPersonClothes[]>;

@Injectable({ providedIn: 'root' })
export class PersonClothesService {
    private resourceUrl = SERVER_API_URL + 'api/person-clothes';

    constructor(private http: HttpClient) {}

    create(personClothes: IPersonClothes): Observable<EntityResponseType> {
        return this.http.post<IPersonClothes>(this.resourceUrl, personClothes, { observe: 'response' });
    }

    update(personClothes: IPersonClothes): Observable<EntityResponseType> {
        return this.http.put<IPersonClothes>(this.resourceUrl, personClothes, { observe: 'response' });
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<IPersonClothes>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    query(req?: any): Observable<EntityArrayResponseType> {
        const options = createRequestOption(req);
        return this.http.get<IPersonClothes[]>(this.resourceUrl, { params: options, observe: 'response' });
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }
}
