import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IPersonClothes } from 'app/shared/model/person-clothes.model';
import { PersonClothesService } from './person-clothes.service';

@Component({
    selector: 'jhi-person-clothes-delete-dialog',
    templateUrl: './person-clothes-delete-dialog.component.html'
})
export class PersonClothesDeleteDialogComponent {
    personClothes: IPersonClothes;

    constructor(
        private personClothesService: PersonClothesService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {}

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.personClothesService.delete(id).subscribe(response => {
            this.eventManager.broadcast({
                name: 'personClothesListModification',
                content: 'Deleted an personClothes'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-person-clothes-delete-popup',
    template: ''
})
export class PersonClothesDeletePopupComponent implements OnInit, OnDestroy {
    private ngbModalRef: NgbModalRef;

    constructor(private activatedRoute: ActivatedRoute, private router: Router, private modalService: NgbModal) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ personClothes }) => {
            setTimeout(() => {
                this.ngbModalRef = this.modalService.open(PersonClothesDeleteDialogComponent as Component, {
                    size: 'lg',
                    backdrop: 'static'
                });
                this.ngbModalRef.componentInstance.personClothes = personClothes;
                this.ngbModalRef.result.then(
                    result => {
                        this.router.navigate([{ outlets: { popup: null } }], { replaceUrl: true, queryParamsHandling: 'merge' });
                        this.ngbModalRef = null;
                    },
                    reason => {
                        this.router.navigate([{ outlets: { popup: null } }], { replaceUrl: true, queryParamsHandling: 'merge' });
                        this.ngbModalRef = null;
                    }
                );
            }, 0);
        });
    }

    ngOnDestroy() {
        this.ngbModalRef = null;
    }
}
