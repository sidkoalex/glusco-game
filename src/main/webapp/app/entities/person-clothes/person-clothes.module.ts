import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { GluscoSharedModule } from 'app/shared';
import {
    PersonClothesComponent,
    PersonClothesDetailComponent,
    PersonClothesUpdateComponent,
    PersonClothesDeletePopupComponent,
    PersonClothesDeleteDialogComponent,
    personClothesRoute,
    personClothesPopupRoute
} from './';

const ENTITY_STATES = [...personClothesRoute, ...personClothesPopupRoute];

@NgModule({
    imports: [GluscoSharedModule, RouterModule.forChild(ENTITY_STATES)],
    declarations: [
        PersonClothesComponent,
        PersonClothesDetailComponent,
        PersonClothesUpdateComponent,
        PersonClothesDeleteDialogComponent,
        PersonClothesDeletePopupComponent
    ],
    entryComponents: [
        PersonClothesComponent,
        PersonClothesUpdateComponent,
        PersonClothesDeleteDialogComponent,
        PersonClothesDeletePopupComponent
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class GluscoPersonClothesModule {}
