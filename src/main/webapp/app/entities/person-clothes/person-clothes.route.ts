import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil, JhiResolvePagingParams } from 'ng-jhipster';
import { UserRouteAccessService } from 'app/core';
import { of } from 'rxjs';
import { map } from 'rxjs/operators';
import { PersonClothes } from 'app/shared/model/person-clothes.model';
import { PersonClothesService } from './person-clothes.service';
import { PersonClothesComponent } from './person-clothes.component';
import { PersonClothesDetailComponent } from './person-clothes-detail.component';
import { PersonClothesUpdateComponent } from './person-clothes-update.component';
import { PersonClothesDeletePopupComponent } from './person-clothes-delete-dialog.component';
import { IPersonClothes } from 'app/shared/model/person-clothes.model';

@Injectable({ providedIn: 'root' })
export class PersonClothesResolve implements Resolve<IPersonClothes> {
    constructor(private service: PersonClothesService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const id = route.params['id'] ? route.params['id'] : null;
        if (id) {
            return this.service.find(id).pipe(map((personClothes: HttpResponse<PersonClothes>) => personClothes.body));
        }
        return of(new PersonClothes());
    }
}

export const personClothesRoute: Routes = [
    {
        path: 'person-clothes',
        component: PersonClothesComponent,
        resolve: {
            pagingParams: JhiResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            defaultSort: 'id,asc',
            pageTitle: 'gluscoApp.personClothes.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'person-clothes/:id/view',
        component: PersonClothesDetailComponent,
        resolve: {
            personClothes: PersonClothesResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'gluscoApp.personClothes.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'person-clothes/new',
        component: PersonClothesUpdateComponent,
        resolve: {
            personClothes: PersonClothesResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'gluscoApp.personClothes.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'person-clothes/:id/edit',
        component: PersonClothesUpdateComponent,
        resolve: {
            personClothes: PersonClothesResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'gluscoApp.personClothes.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const personClothesPopupRoute: Routes = [
    {
        path: 'person-clothes/:id/delete',
        component: PersonClothesDeletePopupComponent,
        resolve: {
            personClothes: PersonClothesResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'gluscoApp.personClothes.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
