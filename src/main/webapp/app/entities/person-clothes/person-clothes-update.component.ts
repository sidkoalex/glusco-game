import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { JhiAlertService } from 'ng-jhipster';

import { IPersonClothes } from 'app/shared/model/person-clothes.model';
import { PersonClothesService } from './person-clothes.service';
import { IPerson } from 'app/shared/model/person.model';
import { PersonService } from 'app/entities/person';
import { IClothes } from 'app/shared/model/clothes.model';
import { ClothesService } from 'app/entities/clothes';

@Component({
    selector: 'jhi-person-clothes-update',
    templateUrl: './person-clothes-update.component.html'
})
export class PersonClothesUpdateComponent implements OnInit {
    private _personClothes: IPersonClothes;
    isSaving: boolean;

    people: IPerson[];

    clothes: IClothes[];

    constructor(
        private jhiAlertService: JhiAlertService,
        private personClothesService: PersonClothesService,
        private personService: PersonService,
        private clothesService: ClothesService,
        private activatedRoute: ActivatedRoute
    ) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ personClothes }) => {
            this.personClothes = personClothes;
        });
        this.personService.query().subscribe(
            (res: HttpResponse<IPerson[]>) => {
                this.people = res.body;
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
        this.clothesService.query().subscribe(
            (res: HttpResponse<IClothes[]>) => {
                this.clothes = res.body;
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        if (this.personClothes.id !== undefined) {
            this.subscribeToSaveResponse(this.personClothesService.update(this.personClothes));
        } else {
            this.subscribeToSaveResponse(this.personClothesService.create(this.personClothes));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<IPersonClothes>>) {
        result.subscribe((res: HttpResponse<IPersonClothes>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }

    trackPersonById(index: number, item: IPerson) {
        return item.id;
    }

    trackClothesById(index: number, item: IClothes) {
        return item.id;
    }
    get personClothes() {
        return this._personClothes;
    }

    set personClothes(personClothes: IPersonClothes) {
        this._personClothes = personClothes;
    }
}
