export * from './person-clothes.service';
export * from './person-clothes-update.component';
export * from './person-clothes-delete-dialog.component';
export * from './person-clothes-detail.component';
export * from './person-clothes.component';
export * from './person-clothes.route';
