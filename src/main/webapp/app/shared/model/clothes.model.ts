export interface IClothes {
    id?: number;
    type?: string;
}

export class Clothes implements IClothes {
    constructor(public id?: number, public type?: string) {}
}
