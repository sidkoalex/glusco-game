export interface IPersonClothes {
    id?: number;
    personType?: string;
    personId?: number;
    clothesType?: string;
    clothesId?: number;
}

export class PersonClothes implements IPersonClothes {
    constructor(
        public id?: number,
        public personType?: string,
        public personId?: number,
        public clothesType?: string,
        public clothesId?: number
    ) {}
}
