export interface IReward {
    id?: number;
    type?: string;
}

export class Reward implements IReward {
    constructor(public id?: number, public type?: string) {}
}
