export interface IUserPersonSet {
    id?: number;
    userLogin?: string;
    userId?: number;
    personType?: string;
    personId?: number;
    clothesType?: string;
    clothesId?: number;
}

export class UserPersonSet implements IUserPersonSet {
    constructor(
        public id?: number,
        public userLogin?: string,
        public userId?: number,
        public personType?: string,
        public personId?: number,
        public clothesType?: string,
        public clothesId?: number
    ) {}
}
