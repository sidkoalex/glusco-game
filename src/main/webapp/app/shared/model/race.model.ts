import { Moment } from 'moment';

export interface IRace {
    id?: number;
    itemCount?: number;
    createdAt?: Moment;
    userLogin?: string;
    userId?: number;
    raceLocationType?: string;
    raceLocationId?: number;
    raceItemType?: string;
    raceItemId?: number;
}

export class Race implements IRace {
    constructor(
        public id?: number,
        public itemCount?: number,
        public createdAt?: Moment,
        public userLogin?: string,
        public userId?: number,
        public raceLocationType?: string,
        public raceLocationId?: number,
        public raceItemType?: string,
        public raceItemId?: number
    ) {}
}
