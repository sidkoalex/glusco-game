export interface IRaceLocationItem {
    id?: number;
    raceLocationType?: string;
    raceLocationId?: number;
    raceItemType?: string;
    raceItemId?: number;
}

export class RaceLocationItem implements IRaceLocationItem {
    constructor(
        public id?: number,
        public raceLocationType?: string,
        public raceLocationId?: number,
        public raceItemType?: string,
        public raceItemId?: number
    ) {}
}
