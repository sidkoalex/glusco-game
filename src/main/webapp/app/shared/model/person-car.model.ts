export interface IPersonCar {
    id?: number;
    personType?: string;
    personId?: number;
    carType?: string;
    carId?: number;
}

export class PersonCar implements IPersonCar {
    constructor(public id?: number, public personType?: string, public personId?: number, public carType?: string, public carId?: number) {}
}
