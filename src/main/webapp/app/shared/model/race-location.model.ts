export interface IRaceLocation {
    id?: number;
    type?: string;
}

export class RaceLocation implements IRaceLocation {
    constructor(public id?: number, public type?: string) {}
}
