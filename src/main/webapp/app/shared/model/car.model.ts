export interface ICar {
    id?: number;
    type?: string;
    boxCapacity?: number;
}

export class Car implements ICar {
    constructor(public id?: number, public type?: string, public boxCapacity?: number) {}
}
