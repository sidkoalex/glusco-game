export interface IRaceReward {
    id?: number;
    rewardQr?: string;
    raceId?: number;
    rewardType?: string;
    rewardId?: number;
}

export class RaceReward implements IRaceReward {
    constructor(
        public id?: number,
        public rewardQr?: string,
        public raceId?: number,
        public rewardType?: string,
        public rewardId?: number
    ) {}
}
