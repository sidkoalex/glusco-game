export interface IUserCarSet {
    id?: number;
    color?: string;
    carType?: string;
    carId?: number;
    userPersonSetId?: number;
}

export class UserCarSet implements IUserCarSet {
    constructor(
        public id?: number,
        public color?: string,
        public carType?: string,
        public carId?: number,
        public userPersonSetId?: number
    ) {}
}
