export interface IPerson {
    id?: number;
    type?: string;
}

export class Person implements IPerson {
    constructor(public id?: number, public type?: string) {}
}
