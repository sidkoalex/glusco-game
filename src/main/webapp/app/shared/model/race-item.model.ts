export interface IRaceItem {
    id?: number;
    type?: string;
}

export class RaceItem implements IRaceItem {
    constructor(public id?: number, public type?: string) {}
}
