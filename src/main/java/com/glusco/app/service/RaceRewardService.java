package com.glusco.app.service;

import com.glusco.app.service.dto.RaceRewardDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing RaceReward.
 */
public interface RaceRewardService {

    /**
     * Save a raceReward.
     *
     * @param raceRewardDTO the entity to save
     * @return the persisted entity
     */
    RaceRewardDTO save(RaceRewardDTO raceRewardDTO);

    /**
     * Get all the raceRewards.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    Page<RaceRewardDTO> findAll(Pageable pageable);


    /**
     * Get the "id" raceReward.
     *
     * @param id the id of the entity
     * @return the entity
     */
    Optional<RaceRewardDTO> findOne(Long id);

    /**
     * Delete the "id" raceReward.
     *
     * @param id the id of the entity
     */
    void delete(Long id);
}
