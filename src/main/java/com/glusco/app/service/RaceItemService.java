package com.glusco.app.service;

import com.glusco.app.service.dto.RaceItemDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing RaceItem.
 */
public interface RaceItemService {

    /**
     * Save a raceItem.
     *
     * @param raceItemDTO the entity to save
     * @return the persisted entity
     */
    RaceItemDTO save(RaceItemDTO raceItemDTO);

    /**
     * Get all the raceItems.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    Page<RaceItemDTO> findAll(Pageable pageable);


    /**
     * Get the "id" raceItem.
     *
     * @param id the id of the entity
     * @return the entity
     */
    Optional<RaceItemDTO> findOne(Long id);

    /**
     * Delete the "id" raceItem.
     *
     * @param id the id of the entity
     */
    void delete(Long id);
}
