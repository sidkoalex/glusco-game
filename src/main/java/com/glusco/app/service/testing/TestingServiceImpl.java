package com.glusco.app.service.testing;

import com.glusco.app.entity.ShopStats;
import com.glusco.app.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class TestingServiceImpl {
    private final UserService userService;

    public ShopStats doShopping(Long userId) {
        return userService.incShopStats(userId, ShopStats.createRandom());
    }
}
