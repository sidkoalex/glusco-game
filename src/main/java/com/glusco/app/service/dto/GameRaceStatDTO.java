package com.glusco.app.service.dto;

import lombok.Data;

import javax.validation.constraints.Min;
import java.time.Instant;

@Data
public class GameRaceStatDTO {
    private Long id;
    private String userName;
    @Min(0)
    private Long metersCount;
    private Instant createdAt;
}
