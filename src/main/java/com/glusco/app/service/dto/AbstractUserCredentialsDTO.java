package com.glusco.app.service.dto;

import com.glusco.app.security.SecurityUtils;
import com.glusco.app.web.rest.errors.BadRequestAlertException;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import java.util.Objects;

@Getter
@Setter
@EqualsAndHashCode
public class AbstractUserCredentialsDTO {
    private Long userId;

    private String userLogin;

    public void checkUserIdAndLoginOrSetDefault() {
        String currentLogin = SecurityUtils.getCurrentUserLogin().get();
        Long currentId = SecurityUtils.getCurrentUserId();

        if (userId == null) setUserId(currentId);
        if (userLogin == null) setUserLogin(currentLogin);

        boolean hasRoleAdmin = SecurityUtils.isCurrentUserInRole("ROLE_ADMIN");
        boolean isThisUserLoginAndId = Objects.equals(userId, currentId) && Objects.equals(userLogin, currentLogin);
        if (!hasRoleAdmin && !isThisUserLoginAndId) {
            throw new BadRequestAlertException("You can't set custom user id or login", this.getClass().getSimpleName(), "noway");
        }
    }
}
