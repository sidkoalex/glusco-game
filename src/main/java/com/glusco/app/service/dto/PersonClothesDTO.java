package com.glusco.app.service.dto;

import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the PersonClothes entity.
 */
public class PersonClothesDTO implements Serializable {

    private Long id;

    private Long personId;

    private String personType;

    private Long clothesId;

    private String clothesType;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getPersonId() {
        return personId;
    }

    public void setPersonId(Long personId) {
        this.personId = personId;
    }

    public String getPersonType() {
        return personType;
    }

    public void setPersonType(String personType) {
        this.personType = personType;
    }

    public Long getClothesId() {
        return clothesId;
    }

    public void setClothesId(Long clothesId) {
        this.clothesId = clothesId;
    }

    public String getClothesType() {
        return clothesType;
    }

    public void setClothesType(String clothesType) {
        this.clothesType = clothesType;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        PersonClothesDTO personClothesDTO = (PersonClothesDTO) o;
        if (personClothesDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), personClothesDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "PersonClothesDTO{" +
            "id=" + getId() +
            ", person=" + getPersonId() +
            ", person='" + getPersonType() + "'" +
            ", clothes=" + getClothesId() +
            ", clothes='" + getClothesType() + "'" +
            "}";
    }
}
