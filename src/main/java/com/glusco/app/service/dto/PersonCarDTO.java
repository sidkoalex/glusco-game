package com.glusco.app.service.dto;

import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the PersonCar entity.
 */
public class PersonCarDTO implements Serializable {

    private Long id;

    private Long personId;

    private String personType;

    private Long carId;

    private String carType;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getPersonId() {
        return personId;
    }

    public void setPersonId(Long personId) {
        this.personId = personId;
    }

    public String getPersonType() {
        return personType;
    }

    public void setPersonType(String personType) {
        this.personType = personType;
    }

    public Long getCarId() {
        return carId;
    }

    public void setCarId(Long carId) {
        this.carId = carId;
    }

    public String getCarType() {
        return carType;
    }

    public void setCarType(String carType) {
        this.carType = carType;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        PersonCarDTO personCarDTO = (PersonCarDTO) o;
        if (personCarDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), personCarDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "PersonCarDTO{" +
            "id=" + getId() +
            ", person=" + getPersonId() +
            ", person='" + getPersonType() + "'" +
            ", car=" + getCarId() +
            ", car='" + getCarType() + "'" +
            "}";
    }
}
