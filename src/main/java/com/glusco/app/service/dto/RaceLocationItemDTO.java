package com.glusco.app.service.dto;

import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the RaceLocationItem entity.
 */
public class RaceLocationItemDTO implements Serializable {

    private Long id;

    private Long raceLocationId;

    private String raceLocationType;

    private Long raceItemId;

    private String raceItemType;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getRaceLocationId() {
        return raceLocationId;
    }

    public void setRaceLocationId(Long raceLocationId) {
        this.raceLocationId = raceLocationId;
    }

    public String getRaceLocationType() {
        return raceLocationType;
    }

    public void setRaceLocationType(String raceLocationType) {
        this.raceLocationType = raceLocationType;
    }

    public Long getRaceItemId() {
        return raceItemId;
    }

    public void setRaceItemId(Long raceItemId) {
        this.raceItemId = raceItemId;
    }

    public String getRaceItemType() {
        return raceItemType;
    }

    public void setRaceItemType(String raceItemType) {
        this.raceItemType = raceItemType;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        RaceLocationItemDTO raceLocationItemDTO = (RaceLocationItemDTO) o;
        if (raceLocationItemDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), raceLocationItemDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "RaceLocationItemDTO{" +
            "id=" + getId() +
            ", raceLocation=" + getRaceLocationId() +
            ", raceLocation='" + getRaceLocationType() + "'" +
            ", raceItem=" + getRaceItemId() +
            ", raceItem='" + getRaceItemType() + "'" +
            "}";
    }
}
