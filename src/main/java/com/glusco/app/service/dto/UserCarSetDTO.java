package com.glusco.app.service.dto;

import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the UserCarSet entity.
 */
public class UserCarSetDTO implements Serializable {

    private Long id;

    private String color;

    private Long carId;

    private String carType;

    private Long userPersonSetId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public Long getCarId() {
        return carId;
    }

    public void setCarId(Long carId) {
        this.carId = carId;
    }

    public String getCarType() {
        return carType;
    }

    public void setCarType(String carType) {
        this.carType = carType;
    }

    public Long getUserPersonSetId() {
        return userPersonSetId;
    }

    public void setUserPersonSetId(Long userPersonSetId) {
        this.userPersonSetId = userPersonSetId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        UserCarSetDTO userCarSetDTO = (UserCarSetDTO) o;
        if (userCarSetDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), userCarSetDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "UserCarSetDTO{" +
            "id=" + getId() +
            ", color='" + getColor() + "'" +
            ", car=" + getCarId() +
            ", car='" + getCarType() + "'" +
            ", userPersonSet=" + getUserPersonSetId() +
            "}";
    }
}
