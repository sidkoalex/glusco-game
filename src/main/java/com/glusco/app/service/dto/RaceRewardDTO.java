package com.glusco.app.service.dto;

import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the RaceReward entity.
 */
public class RaceRewardDTO implements Serializable {

    private Long id;

    private String rewardQr;

    private Long raceId;

    private Long rewardId;

    private String rewardType;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getRewardQr() {
        return rewardQr;
    }

    public void setRewardQr(String rewardQr) {
        this.rewardQr = rewardQr;
    }

    public Long getRaceId() {
        return raceId;
    }

    public void setRaceId(Long raceId) {
        this.raceId = raceId;
    }

    public Long getRewardId() {
        return rewardId;
    }

    public void setRewardId(Long rewardId) {
        this.rewardId = rewardId;
    }

    public String getRewardType() {
        return rewardType;
    }

    public void setRewardType(String rewardType) {
        this.rewardType = rewardType;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        RaceRewardDTO raceRewardDTO = (RaceRewardDTO) o;
        if (raceRewardDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), raceRewardDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "RaceRewardDTO{" +
            "id=" + getId() +
            ", rewardQr='" + getRewardQr() + "'" +
            ", race=" + getRaceId() +
            ", reward=" + getRewardId() +
            ", reward='" + getRewardType() + "'" +
            "}";
    }
}
