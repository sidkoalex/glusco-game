package com.glusco.app.service.dto;

import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the RaceLocation entity.
 */
public class RaceLocationDTO implements Serializable {

    private Long id;

    @NotNull
    @Pattern(regexp = "^[a-zA-Z0-9_]*$")
    private String type;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        RaceLocationDTO raceLocationDTO = (RaceLocationDTO) o;
        if (raceLocationDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), raceLocationDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "RaceLocationDTO{" +
            "id=" + getId() +
            ", type='" + getType() + "'" +
            "}";
    }
}
