package com.glusco.app.service.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.time.Instant;

/**
 * A DTO for the Race entity.
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class RaceDTO extends AbstractUserCredentialsDTO {

    private Long id;

    @NotNull
    @Min(value = 0)
    private Integer itemCount;

    @NotNull
    private Instant createdAt;

    private Long raceLocationId;

    private String raceLocationType;

    private Long raceItemId;

    private String raceItemType;
}
