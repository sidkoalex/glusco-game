package com.glusco.app.service.dto;

import lombok.Data;

import java.util.List;

@Data
public class GamePersonDTO {

    private static final long serialVersionUID = 1L;

    private Long id;

    private String personType;

    private List<String> personUnlockedSkins;

    private String personActiveSkin;

    private String personCar;

    private List<Float[]> personUnlockedCarColors;

    private Float[] personActiveCarColor;
}
