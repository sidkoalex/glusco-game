package com.glusco.app.service.dto;

import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the UserPersonSet entity.
 */
public class UserPersonSetDTO extends AbstractUserCredentialsDTO implements Serializable {

    private Long id;

    private Long personId;

    private String personType;

    private Long clothesId;

    private String clothesType;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getPersonId() {
        return personId;
    }

    public void setPersonId(Long personId) {
        this.personId = personId;
    }

    public String getPersonType() {
        return personType;
    }

    public void setPersonType(String personType) {
        this.personType = personType;
    }

    public Long getClothesId() {
        return clothesId;
    }

    public void setClothesId(Long clothesId) {
        this.clothesId = clothesId;
    }

    public String getClothesType() {
        return clothesType;
    }

    public void setClothesType(String clothesType) {
        this.clothesType = clothesType;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        UserPersonSetDTO userPersonSetDTO = (UserPersonSetDTO) o;
        if (userPersonSetDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), userPersonSetDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "UserPersonSetDTO{" +
            "id=" + getId() +
            ", user=" + getUserId() +
            ", user='" + getUserLogin() + "'" +
            ", person=" + getPersonId() +
            ", person='" + getPersonType() + "'" +
            ", clothes=" + getClothesId() +
            ", clothes='" + getClothesType() + "'" +
            "}";
    }
}
