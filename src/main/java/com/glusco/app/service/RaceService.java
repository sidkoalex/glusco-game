package com.glusco.app.service;

import com.glusco.app.service.dto.RaceDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing Race.
 */
public interface RaceService {

    /**
     * Save a race.
     *
     * @param raceDTO the entity to save
     * @return the persisted entity
     */
    RaceDTO save(RaceDTO raceDTO);

    /**
     * Get all the races for user.
     *
     * @param pageable the pagination information
     * @param userId the id of user
     *
     * @return the list of entities
     */
    Page<RaceDTO> findAll(Long userId, Pageable pageable);


    /**
     * Get the "id" race for user.
     *
     * @param id the id of the entity
     * @param userId the id of the user
     * @return the entity
     */
    Optional<RaceDTO> findOne(Long id, Long userId);

    /**
     * Delete the "id" race for user.
     *
     * @param id the id of the entity
     * @param userId the id of the entity
     */
    void delete(Long id, Long userId);
}
