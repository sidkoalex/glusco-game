package com.glusco.app.service.mapper;

import com.glusco.app.domain.GameRaceStat;
import com.glusco.app.service.dto.GameRaceStatDTO;
import com.glusco.app.web.rest.vm.GameRaceStatVM;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface GameRaceStatMapper extends EntityMapper<GameRaceStatDTO, GameRaceStat> {
    @Mapping(source = "user.login", target = "userName")
    GameRaceStatDTO toDto(GameRaceStat entity);

    GameRaceStat toEntity(GameRaceStatVM gameRaceStatVM);
}
