package com.glusco.app.service.mapper;

import com.glusco.app.domain.*;
import com.glusco.app.service.dto.PersonClothesDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity PersonClothes and its DTO PersonClothesDTO.
 */
@Mapper(componentModel = "spring", uses = {PersonMapper.class, ClothesMapper.class})
public interface PersonClothesMapper extends EntityMapper<PersonClothesDTO, PersonClothes> {

    @Mapping(source = "person.id", target = "personId")
    @Mapping(source = "person.type", target = "personType")
    @Mapping(source = "clothes.id", target = "clothesId")
    @Mapping(source = "clothes.type", target = "clothesType")
    PersonClothesDTO toDto(PersonClothes personClothes);

    @Mapping(source = "personId", target = "person")
    @Mapping(source = "clothesId", target = "clothes")
    PersonClothes toEntity(PersonClothesDTO personClothesDTO);

    default PersonClothes fromId(Long id) {
        if (id == null) {
            return null;
        }
        PersonClothes personClothes = new PersonClothes();
        personClothes.setId(id);
        return personClothes;
    }
}
