package com.glusco.app.service.mapper;

import com.glusco.app.domain.*;
import com.glusco.app.service.dto.UserCarSetDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity UserCarSet and its DTO UserCarSetDTO.
 */
@Mapper(componentModel = "spring", uses = {CarMapper.class, UserPersonSetMapper.class})
public interface UserCarSetMapper extends EntityMapper<UserCarSetDTO, UserCarSet> {

    @Mapping(source = "car.id", target = "carId")
    @Mapping(source = "car.type", target = "carType")
    @Mapping(source = "userPersonSet.id", target = "userPersonSetId")
    UserCarSetDTO toDto(UserCarSet userCarSet);

    @Mapping(source = "carId", target = "car")
    @Mapping(source = "userPersonSetId", target = "userPersonSet")
    UserCarSet toEntity(UserCarSetDTO userCarSetDTO);

    default UserCarSet fromId(Long id) {
        if (id == null) {
            return null;
        }
        UserCarSet userCarSet = new UserCarSet();
        userCarSet.setId(id);
        return userCarSet;
    }
}
