package com.glusco.app.service.mapper;

import com.glusco.app.domain.*;
import com.glusco.app.service.dto.RaceDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity Race and its DTO RaceDTO.
 */
@Mapper(componentModel = "spring", uses = {UserMapper.class, RaceLocationMapper.class, RaceItemMapper.class})
public interface RaceMapper extends EntityMapper<RaceDTO, Race> {

    @Mapping(source = "user.id", target = "userId")
    @Mapping(source = "user.login", target = "userLogin")
    @Mapping(source = "raceLocation.id", target = "raceLocationId")
    @Mapping(source = "raceLocation.type", target = "raceLocationType")
    @Mapping(source = "raceItem.id", target = "raceItemId")
    @Mapping(source = "raceItem.type", target = "raceItemType")
    RaceDTO toDto(Race race);

    @Mapping(source = "userId", target = "user")
    @Mapping(source = "raceLocationId", target = "raceLocation")
    @Mapping(source = "raceItemId", target = "raceItem")
    Race toEntity(RaceDTO raceDTO);

    default Race fromId(Long id) {
        if (id == null) {
            return null;
        }
        Race race = new Race();
        race.setId(id);
        return race;
    }
}
