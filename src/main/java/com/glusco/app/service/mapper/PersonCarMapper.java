package com.glusco.app.service.mapper;

import com.glusco.app.domain.*;
import com.glusco.app.service.dto.PersonCarDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity PersonCar and its DTO PersonCarDTO.
 */
@Mapper(componentModel = "spring", uses = {PersonMapper.class, CarMapper.class})
public interface PersonCarMapper extends EntityMapper<PersonCarDTO, PersonCar> {

    @Mapping(source = "person.id", target = "personId")
    @Mapping(source = "person.type", target = "personType")
    @Mapping(source = "car.id", target = "carId")
    @Mapping(source = "car.type", target = "carType")
    PersonCarDTO toDto(PersonCar personCar);

    @Mapping(source = "personId", target = "person")
    @Mapping(source = "carId", target = "car")
    PersonCar toEntity(PersonCarDTO personCarDTO);

    default PersonCar fromId(Long id) {
        if (id == null) {
            return null;
        }
        PersonCar personCar = new PersonCar();
        personCar.setId(id);
        return personCar;
    }
}
