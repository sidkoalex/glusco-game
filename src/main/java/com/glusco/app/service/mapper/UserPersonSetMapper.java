package com.glusco.app.service.mapper;

import com.glusco.app.domain.*;
import com.glusco.app.service.dto.UserPersonSetDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity UserPersonSet and its DTO UserPersonSetDTO.
 */
@Mapper(componentModel = "spring", uses = {UserMapper.class, PersonMapper.class, ClothesMapper.class})
public interface UserPersonSetMapper extends EntityMapper<UserPersonSetDTO, UserPersonSet> {

    @Mapping(source = "user.id", target = "userId")
    @Mapping(source = "user.login", target = "userLogin")
    @Mapping(source = "person.id", target = "personId")
    @Mapping(source = "person.type", target = "personType")
    @Mapping(source = "clothes.id", target = "clothesId")
    @Mapping(source = "clothes.type", target = "clothesType")
    UserPersonSetDTO toDto(UserPersonSet userPersonSet);

    @Mapping(source = "userId", target = "user")
    @Mapping(source = "personId", target = "person")
    @Mapping(source = "clothesId", target = "clothes")
    UserPersonSet toEntity(UserPersonSetDTO userPersonSetDTO);

    default UserPersonSet fromId(Long id) {
        if (id == null) {
            return null;
        }
        UserPersonSet userPersonSet = new UserPersonSet();
        userPersonSet.setId(id);
        return userPersonSet;
    }
}
