package com.glusco.app.service.mapper;

import com.glusco.app.domain.*;
import com.glusco.app.service.dto.RaceLocationDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity RaceLocation and its DTO RaceLocationDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface RaceLocationMapper extends EntityMapper<RaceLocationDTO, RaceLocation> {



    default RaceLocation fromId(Long id) {
        if (id == null) {
            return null;
        }
        RaceLocation raceLocation = new RaceLocation();
        raceLocation.setId(id);
        return raceLocation;
    }
}
