package com.glusco.app.service.mapper;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.glusco.app.domain.GamePerson;
import com.glusco.app.service.dto.GamePersonDTO;
import com.glusco.app.web.rest.vm.GamePersonVM;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.mapstruct.Mapper;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.IOException;
import java.util.List;

/**
 * Mapper for the entity GamePerson and its DTO GamePersonDTO.
 */
@Mapper(componentModel = "spring", uses = {})
@RequiredArgsConstructor
@Log4j2
public abstract class GamePersonMapper implements EntityMapper<GamePersonDTO, GamePerson> {
    @Autowired
    private ObjectMapper objectMapper;

    public abstract GamePersonDTO toDto(GamePersonVM vm);

    @Override
    public GamePerson toEntity(GamePersonDTO dto) {
        GamePerson entity = new GamePerson();
        try {
            String personActiveCarColor = objectMapper.writeValueAsString(dto.getPersonActiveCarColor());
            String personUnlockedCarColors = objectMapper.writeValueAsString(dto.getPersonUnlockedCarColors());
            String personUnlockedSkins = objectMapper.writeValueAsString(dto.getPersonUnlockedSkins());

            entity.setPersonActiveCarColor(personActiveCarColor);
            entity.setPersonActiveSkin(dto.getPersonActiveSkin());
            entity.setPersonCar(dto.getPersonCar());
            entity.setPersonType(dto.getPersonType());
            entity.setPersonUnlockedCarColors(personUnlockedCarColors);
            entity.setPersonUnlockedSkins(personUnlockedSkins);
        } catch (JsonProcessingException ex) {
            log.error("Error when processing json");
        }
        return entity;
    }

    @Override
    public GamePersonDTO toDto(GamePerson entity) {
        GamePersonDTO dto = new GamePersonDTO();
        try {
            Float[] personActiveCarColor = objectMapper.readValue(entity.getPersonActiveCarColor(), Float[].class);
            List<Float[]> personUnlockedCarColors = objectMapper.readValue(entity.getPersonUnlockedCarColors(),
                objectMapper.getTypeFactory().constructCollectionType(List.class, Float[].class));
            List<String> personUnlockedSkins = objectMapper.readValue(entity.getPersonUnlockedSkins(),
                objectMapper.getTypeFactory().constructCollectionType(List.class, String.class));

            dto.setId(entity.getId());
            dto.setPersonCar(entity.getPersonCar());
            dto.setPersonActiveCarColor(personActiveCarColor);
            dto.setPersonActiveSkin(entity.getPersonActiveSkin());
            dto.setPersonType(entity.getPersonType());
            dto.setPersonUnlockedCarColors(personUnlockedCarColors);
            dto.setPersonUnlockedSkins(personUnlockedSkins);
        } catch (IOException ex) {
            log.error("Error when processing json");
        }
        return dto;
    }
}
