package com.glusco.app.service.mapper;

import com.glusco.app.domain.*;
import com.glusco.app.service.dto.RaceRewardDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity RaceReward and its DTO RaceRewardDTO.
 */
@Mapper(componentModel = "spring", uses = {RaceMapper.class, RewardMapper.class})
public interface RaceRewardMapper extends EntityMapper<RaceRewardDTO, RaceReward> {

    @Mapping(source = "race.id", target = "raceId")
    @Mapping(source = "reward.id", target = "rewardId")
    @Mapping(source = "reward.type", target = "rewardType")
    RaceRewardDTO toDto(RaceReward raceReward);

    @Mapping(source = "raceId", target = "race")
    @Mapping(source = "rewardId", target = "reward")
    RaceReward toEntity(RaceRewardDTO raceRewardDTO);

    default RaceReward fromId(Long id) {
        if (id == null) {
            return null;
        }
        RaceReward raceReward = new RaceReward();
        raceReward.setId(id);
        return raceReward;
    }
}
