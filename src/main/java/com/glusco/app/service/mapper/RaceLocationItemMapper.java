package com.glusco.app.service.mapper;

import com.glusco.app.domain.*;
import com.glusco.app.service.dto.RaceLocationItemDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity RaceLocationItem and its DTO RaceLocationItemDTO.
 */
@Mapper(componentModel = "spring", uses = {RaceLocationMapper.class, RaceItemMapper.class})
public interface RaceLocationItemMapper extends EntityMapper<RaceLocationItemDTO, RaceLocationItem> {

    @Mapping(source = "raceLocation.id", target = "raceLocationId")
    @Mapping(source = "raceLocation.type", target = "raceLocationType")
    @Mapping(source = "raceItem.id", target = "raceItemId")
    @Mapping(source = "raceItem.type", target = "raceItemType")
    RaceLocationItemDTO toDto(RaceLocationItem raceLocationItem);

    @Mapping(source = "raceLocationId", target = "raceLocation")
    @Mapping(source = "raceItemId", target = "raceItem")
    RaceLocationItem toEntity(RaceLocationItemDTO raceLocationItemDTO);

    default RaceLocationItem fromId(Long id) {
        if (id == null) {
            return null;
        }
        RaceLocationItem raceLocationItem = new RaceLocationItem();
        raceLocationItem.setId(id);
        return raceLocationItem;
    }
}
