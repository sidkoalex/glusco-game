package com.glusco.app.service.mapper;

import com.glusco.app.domain.*;
import com.glusco.app.service.dto.ClothesDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity Clothes and its DTO ClothesDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface ClothesMapper extends EntityMapper<ClothesDTO, Clothes> {



    default Clothes fromId(Long id) {
        if (id == null) {
            return null;
        }
        Clothes clothes = new Clothes();
        clothes.setId(id);
        return clothes;
    }
}
