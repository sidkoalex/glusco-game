package com.glusco.app.service.mapper;

import com.glusco.app.domain.*;
import com.glusco.app.service.dto.RaceItemDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity RaceItem and its DTO RaceItemDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface RaceItemMapper extends EntityMapper<RaceItemDTO, RaceItem> {



    default RaceItem fromId(Long id) {
        if (id == null) {
            return null;
        }
        RaceItem raceItem = new RaceItem();
        raceItem.setId(id);
        return raceItem;
    }
}
