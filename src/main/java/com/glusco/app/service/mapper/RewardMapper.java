package com.glusco.app.service.mapper;

import com.glusco.app.domain.*;
import com.glusco.app.service.dto.RewardDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity Reward and its DTO RewardDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface RewardMapper extends EntityMapper<RewardDTO, Reward> {



    default Reward fromId(Long id) {
        if (id == null) {
            return null;
        }
        Reward reward = new Reward();
        reward.setId(id);
        return reward;
    }
}
