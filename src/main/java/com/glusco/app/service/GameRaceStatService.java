package com.glusco.app.service;

import com.glusco.app.service.dto.GameRaceStatDTO;
import com.glusco.app.web.rest.vm.GameRaceStatVM;
import com.glusco.app.web.rest.vm.criteria.GameRaceStatCriteria;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface GameRaceStatService {
    GameRaceStatDTO save(GameRaceStatVM gameRaceStatVM, Long userId);

    Page<GameRaceStatDTO> findByCriteria(GameRaceStatCriteria criteria, Pageable pageable, Long userId);

    Page<GameRaceStatDTO> findTopForMonth(Integer monthNo, Pageable pageable);

    Page<GameRaceStatDTO> findTopForToday(Pageable pageable);
}
