package com.glusco.app.service;

import com.glusco.app.service.dto.UserCarSetDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing UserCarSet.
 */
public interface UserCarSetService {

    /**
     * Save a userCarSet.
     *
     * @param userCarSetDTO the entity to save
     * @return the persisted entity
     */
    UserCarSetDTO save(UserCarSetDTO userCarSetDTO);

    /**
     * Get all the userCarSets for current user.
     *
     * @param pageable the pagination information
     * @param userId the user id
     * @return the list of entities
     */
    Page<UserCarSetDTO> findAll(Long userId, Pageable pageable);

    /**
     * Get the "id" userCarSet for current user.
     *
     * @param id the id of the entity
     * @param userId the id of the user
     * @return the entity
     */
    Optional<UserCarSetDTO> findOne(Long id, Long userId);


    /**
     * Delete the "id" userCarSet for current user.
     *
     * @param id the id of the entity
     * @param userId the id of the user
     */
    void delete(Long id, Long userId);
}
