package com.glusco.app.service;

import com.glusco.app.service.dto.GamePersonDTO;
import com.glusco.app.web.rest.vm.GamePersonVM;

import java.util.List;
import java.util.Optional;

public interface GamePersonService {
    GamePersonDTO savePerson(GamePersonVM gamePersonVM, Long currentUserId);

    Optional<GamePersonDTO> getPerson(Long userId, String personType);

    List<GamePersonDTO> getAllPersons(Long userId);

    void deletePerson(Long personId, Long userId);
}
