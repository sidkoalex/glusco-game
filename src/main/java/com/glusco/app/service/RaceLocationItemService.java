package com.glusco.app.service;

import com.glusco.app.service.dto.RaceLocationItemDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing RaceLocationItem.
 */
public interface RaceLocationItemService {

    /**
     * Save a raceLocationItem.
     *
     * @param raceLocationItemDTO the entity to save
     * @return the persisted entity
     */
    RaceLocationItemDTO save(RaceLocationItemDTO raceLocationItemDTO);

    /**
     * Get all the raceLocationItems.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    Page<RaceLocationItemDTO> findAll(Pageable pageable);


    /**
     * Get the "id" raceLocationItem.
     *
     * @param id the id of the entity
     * @return the entity
     */
    Optional<RaceLocationItemDTO> findOne(Long id);

    /**
     * Delete the "id" raceLocationItem.
     *
     * @param id the id of the entity
     */
    void delete(Long id);
}
