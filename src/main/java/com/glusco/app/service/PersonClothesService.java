package com.glusco.app.service;

import com.glusco.app.service.dto.PersonClothesDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing PersonClothes.
 */
public interface PersonClothesService {

    /**
     * Save a personClothes.
     *
     * @param personClothesDTO the entity to save
     * @return the persisted entity
     */
    PersonClothesDTO save(PersonClothesDTO personClothesDTO);

    /**
     * Get all the personClothes.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    Page<PersonClothesDTO> findAll(Pageable pageable);


    /**
     * Get the "id" personClothes.
     *
     * @param id the id of the entity
     * @return the entity
     */
    Optional<PersonClothesDTO> findOne(Long id);

    /**
     * Delete the "id" personClothes.
     *
     * @param id the id of the entity
     */
    void delete(Long id);
}
