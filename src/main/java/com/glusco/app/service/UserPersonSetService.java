package com.glusco.app.service;

import com.glusco.app.service.dto.UserPersonSetDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Interface for managing UserPersonSet.
 */
public interface UserPersonSetService {

    /**
     * Save a userPersonSet.
     *
     * @param userPersonSetDTO the entity to save
     * @return the persisted entity
     */
    UserPersonSetDTO save(UserPersonSetDTO userPersonSetDTO);

    /**
     * Get all the userPersonSets.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    Page<UserPersonSetDTO> findAllByUserId(Long userId, Pageable pageable);

    /**
     * Get the "id" userPersonSet.
     *
     * @param id the id of the entity
     * @param userId
     * @return the entity
     */
    Optional<UserPersonSetDTO> findOneByUserId(Long id, Long userId);

    /**
     * Delete the "id" userPersonSet.
     *
     * @param id the id of the entity
     * @param userId
     */
    void deleteOneByUserId(Long id, Long userId);

    void findAndCheckOwnerCredentials(Long userPersonSetId);
}
