package com.glusco.app.service;

import com.glusco.app.service.dto.RaceLocationDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing RaceLocation.
 */
public interface RaceLocationService {

    /**
     * Save a raceLocation.
     *
     * @param raceLocationDTO the entity to save
     * @return the persisted entity
     */
    RaceLocationDTO save(RaceLocationDTO raceLocationDTO);

    /**
     * Get all the raceLocations.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    Page<RaceLocationDTO> findAll(Pageable pageable);


    /**
     * Get the "id" raceLocation.
     *
     * @param id the id of the entity
     * @return the entity
     */
    Optional<RaceLocationDTO> findOne(Long id);

    /**
     * Delete the "id" raceLocation.
     *
     * @param id the id of the entity
     */
    void delete(Long id);
}
