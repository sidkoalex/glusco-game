package com.glusco.app.service.impl;

import com.glusco.app.service.RaceItemService;
import com.glusco.app.domain.RaceItem;
import com.glusco.app.repository.RaceItemRepository;
import com.glusco.app.service.dto.RaceItemDTO;
import com.glusco.app.service.mapper.RaceItemMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import java.util.Optional;
/**
 * Service Implementation for managing RaceItem.
 */
@Service
@Transactional
public class RaceItemServiceImpl implements RaceItemService {

    private final Logger log = LoggerFactory.getLogger(RaceItemServiceImpl.class);

    private final RaceItemRepository raceItemRepository;

    private final RaceItemMapper raceItemMapper;

    public RaceItemServiceImpl(RaceItemRepository raceItemRepository, RaceItemMapper raceItemMapper) {
        this.raceItemRepository = raceItemRepository;
        this.raceItemMapper = raceItemMapper;
    }

    /**
     * Save a raceItem.
     *
     * @param raceItemDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public RaceItemDTO save(RaceItemDTO raceItemDTO) {
        log.debug("Request to save RaceItem : {}", raceItemDTO);
        RaceItem raceItem = raceItemMapper.toEntity(raceItemDTO);
        raceItem = raceItemRepository.save(raceItem);
        return raceItemMapper.toDto(raceItem);
    }

    /**
     * Get all the raceItems.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<RaceItemDTO> findAll(Pageable pageable) {
        log.debug("Request to get all RaceItems");
        return raceItemRepository.findAll(pageable)
            .map(raceItemMapper::toDto);
    }


    /**
     * Get one raceItem by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<RaceItemDTO> findOne(Long id) {
        log.debug("Request to get RaceItem : {}", id);
        return raceItemRepository.findById(id)
            .map(raceItemMapper::toDto);
    }

    /**
     * Delete the raceItem by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete RaceItem : {}", id);
        raceItemRepository.deleteById(id);
    }
}
