package com.glusco.app.service.impl;

import com.glusco.app.service.RaceLocationService;
import com.glusco.app.domain.RaceLocation;
import com.glusco.app.repository.RaceLocationRepository;
import com.glusco.app.service.dto.RaceLocationDTO;
import com.glusco.app.service.mapper.RaceLocationMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import java.util.Optional;
/**
 * Service Implementation for managing RaceLocation.
 */
@Service
@Transactional
public class RaceLocationServiceImpl implements RaceLocationService {

    private final Logger log = LoggerFactory.getLogger(RaceLocationServiceImpl.class);

    private final RaceLocationRepository raceLocationRepository;

    private final RaceLocationMapper raceLocationMapper;

    public RaceLocationServiceImpl(RaceLocationRepository raceLocationRepository, RaceLocationMapper raceLocationMapper) {
        this.raceLocationRepository = raceLocationRepository;
        this.raceLocationMapper = raceLocationMapper;
    }

    /**
     * Save a raceLocation.
     *
     * @param raceLocationDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public RaceLocationDTO save(RaceLocationDTO raceLocationDTO) {
        log.debug("Request to save RaceLocation : {}", raceLocationDTO);
        RaceLocation raceLocation = raceLocationMapper.toEntity(raceLocationDTO);
        raceLocation = raceLocationRepository.save(raceLocation);
        return raceLocationMapper.toDto(raceLocation);
    }

    /**
     * Get all the raceLocations.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<RaceLocationDTO> findAll(Pageable pageable) {
        log.debug("Request to get all RaceLocations");
        return raceLocationRepository.findAll(pageable)
            .map(raceLocationMapper::toDto);
    }


    /**
     * Get one raceLocation by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<RaceLocationDTO> findOne(Long id) {
        log.debug("Request to get RaceLocation : {}", id);
        return raceLocationRepository.findById(id)
            .map(raceLocationMapper::toDto);
    }

    /**
     * Delete the raceLocation by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete RaceLocation : {}", id);
        raceLocationRepository.deleteById(id);
    }
}
