package com.glusco.app.service.impl;

import com.glusco.app.service.RaceRewardService;
import com.glusco.app.domain.RaceReward;
import com.glusco.app.repository.RaceRewardRepository;
import com.glusco.app.service.dto.RaceRewardDTO;
import com.glusco.app.service.mapper.RaceRewardMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import java.util.Optional;
/**
 * Service Implementation for managing RaceReward.
 */
@Service
@Transactional
public class RaceRewardServiceImpl implements RaceRewardService {

    private final Logger log = LoggerFactory.getLogger(RaceRewardServiceImpl.class);

    private final RaceRewardRepository raceRewardRepository;

    private final RaceRewardMapper raceRewardMapper;

    public RaceRewardServiceImpl(RaceRewardRepository raceRewardRepository, RaceRewardMapper raceRewardMapper) {
        this.raceRewardRepository = raceRewardRepository;
        this.raceRewardMapper = raceRewardMapper;
    }

    /**
     * Save a raceReward.
     *
     * @param raceRewardDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public RaceRewardDTO save(RaceRewardDTO raceRewardDTO) {
        log.debug("Request to save RaceReward : {}", raceRewardDTO);
        RaceReward raceReward = raceRewardMapper.toEntity(raceRewardDTO);
        raceReward = raceRewardRepository.save(raceReward);
        return raceRewardMapper.toDto(raceReward);
    }

    /**
     * Get all the raceRewards.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<RaceRewardDTO> findAll(Pageable pageable) {
        log.debug("Request to get all RaceRewards");
        return raceRewardRepository.findAll(pageable)
            .map(raceRewardMapper::toDto);
    }


    /**
     * Get one raceReward by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<RaceRewardDTO> findOne(Long id) {
        log.debug("Request to get RaceReward : {}", id);
        return raceRewardRepository.findById(id)
            .map(raceRewardMapper::toDto);
    }

    /**
     * Delete the raceReward by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete RaceReward : {}", id);
        raceRewardRepository.deleteById(id);
    }
}
