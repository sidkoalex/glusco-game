package com.glusco.app.service.impl;

import com.glusco.app.domain.GameRaceStat;
import com.glusco.app.domain.GameRaceStat_;
import com.glusco.app.domain.User;
import com.glusco.app.domain.User_;
import com.glusco.app.repository.GameRaceStatRepository;
import com.glusco.app.service.GameRaceStatService;
import com.glusco.app.service.dto.GameRaceStatDTO;
import com.glusco.app.service.mapper.GameRaceStatMapper;
import com.glusco.app.web.rest.vm.GameRaceStatVM;
import com.glusco.app.web.rest.vm.criteria.GameRaceStatCriteria;
import io.github.jhipster.service.QueryService;
import io.github.jhipster.service.filter.LongFilter;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import lombok.val;
import lombok.var;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.ZonedDateTime;

import static java.time.ZoneOffset.UTC;
import static java.time.temporal.ChronoUnit.DAYS;

@Service
@RequiredArgsConstructor
@Log4j2
public class GameRaceStatServiceImpl extends QueryService<GameRaceStat> implements GameRaceStatService {

    private final GameRaceStatRepository gameRaceStatRepository;

    private final GameRaceStatMapper gameRaceStatMapper;

    @Override
    @Transactional(readOnly = false)
    public GameRaceStatDTO save(GameRaceStatVM gameRaceStatVM, Long userId) {
        log.debug("Try to save race statistic for userId {}, {}", userId, gameRaceStatVM);
        val entity = gameRaceStatMapper.toEntity(gameRaceStatVM);
        entity.setUser(new User(userId));
        val savedEntity = gameRaceStatRepository.save(entity);
        log.debug("Race statistic was saved {}", savedEntity);
        return gameRaceStatMapper.toDto(savedEntity);
    }

    @Override
    public Page<GameRaceStatDTO> findByCriteria(GameRaceStatCriteria criteria, Pageable pageable, Long userId) {
        log.debug("Try to find races by criteria for userId {}, {}, {}", userId, criteria, pageable);
        criteria.setUserId(new LongFilter() {{
            setEquals(userId);
        }});
        val specification = createSpecification(criteria);
        val page = gameRaceStatRepository.findAll(specification, pageable);
        log.debug("Found {} races for userId {}", page.getSize(), userId);
        return page.map(gameRaceStatMapper::toDto);
    }

    @Override
    public Page<GameRaceStatDTO> findTopForMonth(Integer monthNo, Pageable pageable) {
        log.debug("Try to get top for month {}", pageable);
        ZonedDateTime now = ZonedDateTime.now(UTC);
        if (monthNo == null) monthNo = now.getMonthValue();
        var month = now.withMonth(monthNo).truncatedTo(DAYS).withDayOfMonth(1);
        if (monthNo > now.getMonthValue()) month = month.minusYears(1);
        val monthStart = month.toInstant();
        val monthEnd = month.plusMonths(1).toInstant();
        val page = gameRaceStatRepository.findByIntervalOrderByMetersCount(monthStart, monthEnd, pageable);
        log.debug("Found {} races for month top {}", page.getSize());
        return page.map(gameRaceStatMapper::toDto);
    }

    @Override
    public Page<GameRaceStatDTO> findTopForToday(Pageable pageable) {
        log.debug("Try to get top for today {}", pageable);
        val dayStart = ZonedDateTime.now(UTC).truncatedTo(DAYS).toInstant();
        val dayEnd = ZonedDateTime.now(UTC).truncatedTo(DAYS).plusDays(1).toInstant();
        val page = gameRaceStatRepository.findByIntervalOrderByMetersCount(dayStart, dayEnd, pageable);
        log.debug("Found {} races for today top {}", page.getSize());
        return page.map(gameRaceStatMapper::toDto);
    }


    private Specification<GameRaceStat> createSpecification(GameRaceStatCriteria criteria) {
        Specification<GameRaceStat> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getUserId() != null)
                specification = specification.and(buildReferringEntitySpecification(criteria.getUserId(), GameRaceStat_.user, User_.id));
            if (criteria.getCreatedAt() != null)
                specification = specification.and(buildRangeSpecification(criteria.getCreatedAt(), GameRaceStat_.createdAt));
        }
        return specification;
    }
}
