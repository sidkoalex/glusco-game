package com.glusco.app.service.impl;

import com.glusco.app.domain.UserPersonSet;
import com.glusco.app.repository.UserPersonSetRepository;
import com.glusco.app.service.UserPersonSetService;
import com.glusco.app.service.dto.AbstractUserCredentialsDTO;
import com.glusco.app.service.dto.UserPersonSetDTO;
import com.glusco.app.service.mapper.UserPersonSetMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

import static com.glusco.app.security.SecurityUtils.getCurrentUserId;

/**
 * Service Implementation for managing UserPersonSet.
 */
@Service
@Transactional
@Log4j2
@RequiredArgsConstructor
public class UserPersonSetServiceImpl implements UserPersonSetService {

    private final UserPersonSetRepository userPersonSetRepository;

    private final UserPersonSetMapper userPersonSetMapper;

    /**
     * Save a userPersonSet.
     *
     * @param userPersonSetDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public UserPersonSetDTO save(UserPersonSetDTO userPersonSetDTO) {
        log.debug("Request to save UserPersonSet : {}", userPersonSetDTO);
        userPersonSetDTO.checkUserIdAndLoginOrSetDefault();
        UserPersonSet userPersonSet = userPersonSetMapper.toEntity(userPersonSetDTO);
        userPersonSet = userPersonSetRepository.save(userPersonSet);
        return userPersonSetMapper.toDto(userPersonSet);
    }

    /**
     * Get all the userPersonSets.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<UserPersonSetDTO> findAllByUserId(Long userId, Pageable pageable) {
        log.debug("Request to get all UserPersonSets");
        return userPersonSetRepository.findAllByUserId(userId, pageable)
            .map(userPersonSetMapper::toDto);
    }


    /**
     * Get one userPersonSet by id.
     *
     * @param id     the id of the entity
     * @param userId
     * @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<UserPersonSetDTO> findOneByUserId(Long id, Long userId) {
        log.debug("Request to get UserPersonSet : {}", id);
        if (id == null || userId == null) return Optional.empty();
        return userPersonSetRepository.findByIdAndUserId(id, userId)
            .map(userPersonSetMapper::toDto);
    }

    /**
     * Delete the userPersonSet by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void deleteOneByUserId(Long id, Long userId) {
        log.debug("Request to delete UserPersonSet : {}", id);
        userPersonSetRepository.deleteByIdAndUserId(id, userId);
    }

    @Override
    public void findAndCheckOwnerCredentials(Long userPersonSetId) {
        findOneByUserId(userPersonSetId, getCurrentUserId())
            .ifPresent(AbstractUserCredentialsDTO::checkUserIdAndLoginOrSetDefault);
    }
}
