package com.glusco.app.service.impl;

import com.glusco.app.domain.Race;
import com.glusco.app.repository.RaceRepository;
import com.glusco.app.service.RaceService;
import com.glusco.app.service.dto.RaceDTO;
import com.glusco.app.service.mapper.RaceMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;
/**
 * Service Implementation for managing Race.
 */
@Service
@Transactional
@Log4j2
@RequiredArgsConstructor
public class RaceServiceImpl implements RaceService {

    private final RaceRepository raceRepository;

    private final RaceMapper raceMapper;

    /**
     * Save a race.
     *
     * @param raceDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public RaceDTO save(RaceDTO raceDTO) {
        log.debug("Request to save Race : {}", raceDTO);

        raceDTO.checkUserIdAndLoginOrSetDefault();

        Race race = raceMapper.toEntity(raceDTO);
        race = raceRepository.save(race);

        return raceMapper.toDto(race);
    }

    /**
     * Get all the races for user.
     *
     * @param userId id of the user
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<RaceDTO> findAll(Long userId, Pageable pageable) {

        log.debug("Request to get all Races");

        return raceRepository.findAllByUserId(userId, pageable)
            .map(raceMapper::toDto);
    }


    /**
     * Get one race by id for user.
     *
     * @param id the id of the entity
     * @param userId the id of the user
     * @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<RaceDTO> findOne(Long id, Long userId) {
        log.debug("Request to get Race : {}", id);
        return raceRepository.findFirstByIdAndUserId(id, userId)
            .map(raceMapper::toDto);
    }

    /**
     * Delete the race by id for user.
     *
     * @param id the id of the entity
     * @param userId the id of the user
     */
    @Override
    public void delete(Long id, Long userId) {
        log.debug("Request to delete Race : {}", id);
        raceRepository.deleteByIdAndUserId(id, userId);
    }
}
