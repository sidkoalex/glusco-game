package com.glusco.app.service.impl;

import com.glusco.app.service.PersonCarService;
import com.glusco.app.domain.PersonCar;
import com.glusco.app.repository.PersonCarRepository;
import com.glusco.app.service.dto.PersonCarDTO;
import com.glusco.app.service.mapper.PersonCarMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import java.util.Optional;
/**
 * Service Implementation for managing PersonCar.
 */
@Service
@Transactional
public class PersonCarServiceImpl implements PersonCarService {

    private final Logger log = LoggerFactory.getLogger(PersonCarServiceImpl.class);

    private final PersonCarRepository personCarRepository;

    private final PersonCarMapper personCarMapper;

    public PersonCarServiceImpl(PersonCarRepository personCarRepository, PersonCarMapper personCarMapper) {
        this.personCarRepository = personCarRepository;
        this.personCarMapper = personCarMapper;
    }

    /**
     * Save a personCar.
     *
     * @param personCarDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public PersonCarDTO save(PersonCarDTO personCarDTO) {
        log.debug("Request to save PersonCar : {}", personCarDTO);
        PersonCar personCar = personCarMapper.toEntity(personCarDTO);
        personCar = personCarRepository.save(personCar);
        return personCarMapper.toDto(personCar);
    }

    /**
     * Get all the personCars.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<PersonCarDTO> findAll(Pageable pageable) {
        log.debug("Request to get all PersonCars");
        return personCarRepository.findAll(pageable)
            .map(personCarMapper::toDto);
    }


    /**
     * Get one personCar by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<PersonCarDTO> findOne(Long id) {
        log.debug("Request to get PersonCar : {}", id);
        return personCarRepository.findById(id)
            .map(personCarMapper::toDto);
    }

    /**
     * Delete the personCar by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete PersonCar : {}", id);
        personCarRepository.deleteById(id);
    }
}
