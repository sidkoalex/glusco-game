package com.glusco.app.service.impl;

import com.glusco.app.domain.GamePerson;
import com.glusco.app.domain.User;
import com.glusco.app.repository.GamePersonRepository;
import com.glusco.app.service.GamePersonService;
import com.glusco.app.service.dto.GamePersonDTO;
import com.glusco.app.service.mapper.GamePersonMapper;
import com.glusco.app.web.rest.vm.GamePersonVM;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class GamePersonServiceImpl implements GamePersonService {

    private final GamePersonRepository gamePersonRepository;
    private final GamePersonMapper gamePersonMapper;

    @Override
    public GamePersonDTO savePerson(GamePersonVM gamePersonVM, Long userId) {
        GamePersonDTO dto = gamePersonMapper.toDto(gamePersonVM);
        GamePerson entity = gamePersonMapper.toEntity(dto);
        entity.setUser(new User().setId(userId));

        Optional<GamePerson> existingPersonByType = gamePersonRepository.findFirstByPersonTypeAndUserId(gamePersonVM.getPersonType(), userId);
        existingPersonByType.ifPresent(person -> entity.setId(person.getId()));

        GamePerson savedEntity = gamePersonRepository.save(entity);
        return gamePersonMapper.toDto(savedEntity);
    }

    @Override
    public Optional<GamePersonDTO> getPerson(Long userId, String personType) {
        Optional<GamePerson> entity = gamePersonRepository.findFirstByPersonTypeAndUserId(personType, userId);
        return entity.map(gamePersonMapper::toDto);
    }

    @Override
    public List<GamePersonDTO> getAllPersons(Long userId) {
        List<GamePerson> entities = gamePersonRepository.findAllByUserId(userId);
        return gamePersonMapper.toDto(entities);
    }

    @Override
    @Transactional
    public void deletePerson(Long personId, Long userId) {
        boolean exists = gamePersonRepository.existsByIdAndUserId(personId, userId);
        if (!exists) throw new EntityNotFoundException("Person not found");
        gamePersonRepository.deleteByIdAndUserId(personId, userId);
    }
}
