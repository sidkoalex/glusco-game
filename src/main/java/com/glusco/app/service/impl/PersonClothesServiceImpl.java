package com.glusco.app.service.impl;

import com.glusco.app.service.PersonClothesService;
import com.glusco.app.domain.PersonClothes;
import com.glusco.app.repository.PersonClothesRepository;
import com.glusco.app.service.dto.PersonClothesDTO;
import com.glusco.app.service.mapper.PersonClothesMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import java.util.Optional;
/**
 * Service Implementation for managing PersonClothes.
 */
@Service
@Transactional
public class PersonClothesServiceImpl implements PersonClothesService {

    private final Logger log = LoggerFactory.getLogger(PersonClothesServiceImpl.class);

    private final PersonClothesRepository personClothesRepository;

    private final PersonClothesMapper personClothesMapper;

    public PersonClothesServiceImpl(PersonClothesRepository personClothesRepository, PersonClothesMapper personClothesMapper) {
        this.personClothesRepository = personClothesRepository;
        this.personClothesMapper = personClothesMapper;
    }

    /**
     * Save a personClothes.
     *
     * @param personClothesDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public PersonClothesDTO save(PersonClothesDTO personClothesDTO) {
        log.debug("Request to save PersonClothes : {}", personClothesDTO);
        PersonClothes personClothes = personClothesMapper.toEntity(personClothesDTO);
        personClothes = personClothesRepository.save(personClothes);
        return personClothesMapper.toDto(personClothes);
    }

    /**
     * Get all the personClothes.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<PersonClothesDTO> findAll(Pageable pageable) {
        log.debug("Request to get all PersonClothes");
        return personClothesRepository.findAll(pageable)
            .map(personClothesMapper::toDto);
    }


    /**
     * Get one personClothes by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<PersonClothesDTO> findOne(Long id) {
        log.debug("Request to get PersonClothes : {}", id);
        return personClothesRepository.findById(id)
            .map(personClothesMapper::toDto);
    }

    /**
     * Delete the personClothes by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete PersonClothes : {}", id);
        personClothesRepository.deleteById(id);
    }
}
