package com.glusco.app.service.impl;

import com.glusco.app.service.ClothesService;
import com.glusco.app.domain.Clothes;
import com.glusco.app.repository.ClothesRepository;
import com.glusco.app.service.dto.ClothesDTO;
import com.glusco.app.service.mapper.ClothesMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import java.util.Optional;
/**
 * Service Implementation for managing Clothes.
 */
@Service
@Transactional
public class ClothesServiceImpl implements ClothesService {

    private final Logger log = LoggerFactory.getLogger(ClothesServiceImpl.class);

    private final ClothesRepository clothesRepository;

    private final ClothesMapper clothesMapper;

    public ClothesServiceImpl(ClothesRepository clothesRepository, ClothesMapper clothesMapper) {
        this.clothesRepository = clothesRepository;
        this.clothesMapper = clothesMapper;
    }

    /**
     * Save a clothes.
     *
     * @param clothesDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public ClothesDTO save(ClothesDTO clothesDTO) {
        log.debug("Request to save Clothes : {}", clothesDTO);
        Clothes clothes = clothesMapper.toEntity(clothesDTO);
        clothes = clothesRepository.save(clothes);
        return clothesMapper.toDto(clothes);
    }

    /**
     * Get all the clothes.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<ClothesDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Clothes");
        return clothesRepository.findAll(pageable)
            .map(clothesMapper::toDto);
    }


    /**
     * Get one clothes by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<ClothesDTO> findOne(Long id) {
        log.debug("Request to get Clothes : {}", id);
        return clothesRepository.findById(id)
            .map(clothesMapper::toDto);
    }

    /**
     * Delete the clothes by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Clothes : {}", id);
        clothesRepository.deleteById(id);
    }
}
