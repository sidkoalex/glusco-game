package com.glusco.app.service.impl;

import com.glusco.app.domain.UserCarSet;
import com.glusco.app.repository.UserCarSetRepository;
import com.glusco.app.service.UserCarSetService;
import com.glusco.app.service.UserPersonSetService;
import com.glusco.app.service.dto.UserCarSetDTO;
import com.glusco.app.service.mapper.UserCarSetMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing UserCarSet.
 */
@Service
@Transactional
@Log4j2
@RequiredArgsConstructor
public class UserCarSetServiceImpl implements UserCarSetService {

    private final UserCarSetRepository userCarSetRepository;

    private final UserCarSetMapper userCarSetMapper;

    private final UserPersonSetService userPersonSetService;


    /**
     * Save a userCarSet.
     *
     * @param userCarSetDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public UserCarSetDTO save(UserCarSetDTO userCarSetDTO) {
        log.debug("Request to save UserCarSet : {}", userCarSetDTO);

        userPersonSetService.findAndCheckOwnerCredentials(userCarSetDTO.getUserPersonSetId());

        UserCarSet userCarSet = userCarSetMapper.toEntity(userCarSetDTO);
        userCarSet = userCarSetRepository.save(userCarSet);

        return userCarSetMapper.toDto(userCarSet);
    }

    /**
     * Get all the userCarSets for user.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<UserCarSetDTO> findAll(Long userId, Pageable pageable) {
        log.debug("Request to get all UserCarSets");
        return userCarSetRepository.findAllByUserPersonSetUserId(userId, pageable)
            .map(userCarSetMapper::toDto);
    }


    /**
     * Get one userCarSet by id for user.
     *
     * @param id     the id of the entity
     * @param userId the id of the user
     * @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<UserCarSetDTO> findOne(Long id, Long userId) {
        log.debug("Request to get UserCarSet : {}", id);
        return userCarSetRepository.findFirstByIdAndUserPersonSetUserId(id, userId)
            .map(userCarSetMapper::toDto);
    }

    /**
     * Delete the userCarSet by id.
     *
     * @param id     the id of the entity
     * @param userId the id of the user
     */
    @Override
    public void delete(Long id, Long userId) {
        log.debug("Request to delete UserCarSet : {}", id);
        userCarSetRepository.deleteByIdAndUserPersonSetUserId(id, userId);
    }
}
