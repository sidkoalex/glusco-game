package com.glusco.app.service.impl;

import com.glusco.app.service.RaceLocationItemService;
import com.glusco.app.domain.RaceLocationItem;
import com.glusco.app.repository.RaceLocationItemRepository;
import com.glusco.app.service.dto.RaceLocationItemDTO;
import com.glusco.app.service.mapper.RaceLocationItemMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import java.util.Optional;
/**
 * Service Implementation for managing RaceLocationItem.
 */
@Service
@Transactional
public class RaceLocationItemServiceImpl implements RaceLocationItemService {

    private final Logger log = LoggerFactory.getLogger(RaceLocationItemServiceImpl.class);

    private final RaceLocationItemRepository raceLocationItemRepository;

    private final RaceLocationItemMapper raceLocationItemMapper;

    public RaceLocationItemServiceImpl(RaceLocationItemRepository raceLocationItemRepository, RaceLocationItemMapper raceLocationItemMapper) {
        this.raceLocationItemRepository = raceLocationItemRepository;
        this.raceLocationItemMapper = raceLocationItemMapper;
    }

    /**
     * Save a raceLocationItem.
     *
     * @param raceLocationItemDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public RaceLocationItemDTO save(RaceLocationItemDTO raceLocationItemDTO) {
        log.debug("Request to save RaceLocationItem : {}", raceLocationItemDTO);
        RaceLocationItem raceLocationItem = raceLocationItemMapper.toEntity(raceLocationItemDTO);
        raceLocationItem = raceLocationItemRepository.save(raceLocationItem);
        return raceLocationItemMapper.toDto(raceLocationItem);
    }

    /**
     * Get all the raceLocationItems.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<RaceLocationItemDTO> findAll(Pageable pageable) {
        log.debug("Request to get all RaceLocationItems");
        return raceLocationItemRepository.findAll(pageable)
            .map(raceLocationItemMapper::toDto);
    }


    /**
     * Get one raceLocationItem by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<RaceLocationItemDTO> findOne(Long id) {
        log.debug("Request to get RaceLocationItem : {}", id);
        return raceLocationItemRepository.findById(id)
            .map(raceLocationItemMapper::toDto);
    }

    /**
     * Delete the raceLocationItem by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete RaceLocationItem : {}", id);
        raceLocationItemRepository.deleteById(id);
    }
}
