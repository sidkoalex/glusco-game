package com.glusco.app.service;

import com.glusco.app.service.dto.PersonCarDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing PersonCar.
 */
public interface PersonCarService {

    /**
     * Save a personCar.
     *
     * @param personCarDTO the entity to save
     * @return the persisted entity
     */
    PersonCarDTO save(PersonCarDTO personCarDTO);

    /**
     * Get all the personCars.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    Page<PersonCarDTO> findAll(Pageable pageable);


    /**
     * Get the "id" personCar.
     *
     * @param id the id of the entity
     * @return the entity
     */
    Optional<PersonCarDTO> findOne(Long id);

    /**
     * Delete the "id" personCar.
     *
     * @param id the id of the entity
     */
    void delete(Long id);
}
