package com.glusco.app.repository;

import com.glusco.app.domain.RaceLocationItem;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the RaceLocationItem entity.
 */
@SuppressWarnings("unused")
@Repository
public interface RaceLocationItemRepository extends JpaRepository<RaceLocationItem, Long> {

}
