package com.glusco.app.repository;

import com.glusco.app.domain.UserPersonSet;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

import java.nio.channels.FileChannel;
import java.util.List;
import java.util.Optional;

/**
 * Spring Data  repository for the UserPersonSet entity.
 */
@SuppressWarnings("unused")
@Repository
public interface UserPersonSetRepository extends JpaRepository<UserPersonSet, Long> {

    @Query("select user_person_set from UserPersonSet user_person_set where user_person_set.user.login = ?#{principal.username}")
    List<UserPersonSet> findByUserIsCurrentUser();

    Page<UserPersonSet> findAllByUserId(Long userId, Pageable pageable);

    Optional<UserPersonSet> findByIdAndUserId(Long id, Long userId);

    void deleteByIdAndUserId(Long id, Long userId);
}
