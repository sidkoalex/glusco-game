package com.glusco.app.repository;

import com.glusco.app.domain.GamePerson;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface GamePersonRepository extends JpaRepository<GamePerson, Long> {
    Optional<GamePerson> findFirstByPersonTypeAndUserId(String personType, Long userId);

    List<GamePerson> findAllByUserId(Long userId);

    void deleteByIdAndUserId(Long personId, Long userId);

    boolean existsByIdAndUserId(Long personId, Long userId);
}
