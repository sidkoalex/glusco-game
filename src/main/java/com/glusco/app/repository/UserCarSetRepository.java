package com.glusco.app.repository;

import com.glusco.app.domain.UserCarSet;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;


/**
 * Spring Data  repository for the UserCarSet entity.
 */
@SuppressWarnings("unused")
@Repository
public interface UserCarSetRepository extends JpaRepository<UserCarSet, Long> {

    Page<UserCarSet> findAllByUserPersonSetUserId(Long userId, Pageable pageable);

    Optional<UserCarSet> findFirstByIdAndUserPersonSetUserId(Long id, Long userId);

    void deleteByIdAndUserPersonSetUserId(Long id, Long userId);
}
