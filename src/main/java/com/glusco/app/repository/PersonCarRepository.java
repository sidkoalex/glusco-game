package com.glusco.app.repository;

import com.glusco.app.domain.PersonCar;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the PersonCar entity.
 */
@SuppressWarnings("unused")
@Repository
public interface PersonCarRepository extends JpaRepository<PersonCar, Long> {

}
