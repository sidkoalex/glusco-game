package com.glusco.app.repository;

import com.glusco.app.domain.Clothes;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the Clothes entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ClothesRepository extends JpaRepository<Clothes, Long> {

}
