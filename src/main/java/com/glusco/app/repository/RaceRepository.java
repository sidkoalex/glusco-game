package com.glusco.app.repository;

import com.glusco.app.domain.Race;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * Spring Data  repository for the Race entity.
 */
@SuppressWarnings("unused")
@Repository
public interface RaceRepository extends JpaRepository<Race, Long> {

    @Query("select race from Race race where race.user.login = ?#{principal.username}")
    List<Race> findByUserIsCurrentUser();

    Page<Race> findAllByUserId(Long userId, Pageable pageable);

    Optional<Race> findFirstByIdAndUserId(Long id, Long userId);

    void deleteByIdAndUserId(Long id, Long userId);
}
