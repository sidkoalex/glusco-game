package com.glusco.app.repository;

import com.glusco.app.domain.PersonClothes;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the PersonClothes entity.
 */
@SuppressWarnings("unused")
@Repository
public interface PersonClothesRepository extends JpaRepository<PersonClothes, Long> {

}
