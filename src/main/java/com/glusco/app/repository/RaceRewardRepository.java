package com.glusco.app.repository;

import com.glusco.app.domain.RaceReward;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the RaceReward entity.
 */
@SuppressWarnings("unused")
@Repository
public interface RaceRewardRepository extends JpaRepository<RaceReward, Long> {

}
