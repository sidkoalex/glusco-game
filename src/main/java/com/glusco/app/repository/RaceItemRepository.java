package com.glusco.app.repository;

import com.glusco.app.domain.RaceItem;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the RaceItem entity.
 */
@SuppressWarnings("unused")
@Repository
public interface RaceItemRepository extends JpaRepository<RaceItem, Long> {

}
