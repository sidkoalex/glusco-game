package com.glusco.app.repository;

import com.glusco.app.domain.RaceLocation;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the RaceLocation entity.
 */
@SuppressWarnings("unused")
@Repository
public interface RaceLocationRepository extends JpaRepository<RaceLocation, Long> {

}
