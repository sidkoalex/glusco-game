package com.glusco.app.repository;

import com.glusco.app.domain.GameRaceStat;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.time.Instant;

@Repository
public interface GameRaceStatRepository extends CrudRepository<GameRaceStat, Long>, JpaSpecificationExecutor<GameRaceStat> {

    @Query("select new  com.glusco.app.domain.GameRaceStat(max(g.id), sum(g.metersCount),  max(g.createdAt), g.user) " +
        "from GameRaceStat g " +
        "group by g.user order by sum(g.metersCount) desc")
    Page<GameRaceStat> findByIntervalOrderByMetersCount(Instant from, Instant to, Pageable pageable);
}
