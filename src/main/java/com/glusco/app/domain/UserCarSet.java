package com.glusco.app.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.Objects;

/**
 * A UserCarSet.
 */
@Entity
@Table(name = "user_car_set")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class UserCarSet implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "color")
    private String color;

    @ManyToOne(optional = false)
    @NotNull
    @JsonIgnoreProperties("")
    private Car car;

    @ManyToOne(optional = false)
    @NotNull
    @JsonIgnoreProperties("")
    private UserPersonSet userPersonSet;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getColor() {
        return color;
    }

    public UserCarSet color(String color) {
        this.color = color;
        return this;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public Car getCar() {
        return car;
    }

    public UserCarSet car(Car car) {
        this.car = car;
        return this;
    }

    public void setCar(Car car) {
        this.car = car;
    }

    public UserPersonSet getUserPersonSet() {
        return userPersonSet;
    }

    public UserCarSet userPersonSet(UserPersonSet userPersonSet) {
        this.userPersonSet = userPersonSet;
        return this;
    }

    public void setUserPersonSet(UserPersonSet userPersonSet) {
        this.userPersonSet = userPersonSet;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        UserCarSet userCarSet = (UserCarSet) o;
        if (userCarSet.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), userCarSet.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "UserCarSet{" +
            "id=" + getId() +
            ", color='" + getColor() + "'" +
            "}";
    }
}
