package com.glusco.app.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.Objects;

/**
 * A RaceLocationItem.
 */
@Entity
@Table(name = "race_location_item")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class RaceLocationItem implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @ManyToOne(optional = false)
    @NotNull
    @JsonIgnoreProperties("")
    private RaceLocation raceLocation;

    @ManyToOne(optional = false)
    @NotNull
    @JsonIgnoreProperties("")
    private RaceItem raceItem;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public RaceLocation getRaceLocation() {
        return raceLocation;
    }

    public RaceLocationItem raceLocation(RaceLocation raceLocation) {
        this.raceLocation = raceLocation;
        return this;
    }

    public void setRaceLocation(RaceLocation raceLocation) {
        this.raceLocation = raceLocation;
    }

    public RaceItem getRaceItem() {
        return raceItem;
    }

    public RaceLocationItem raceItem(RaceItem raceItem) {
        this.raceItem = raceItem;
        return this;
    }

    public void setRaceItem(RaceItem raceItem) {
        this.raceItem = raceItem;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        RaceLocationItem raceLocationItem = (RaceLocationItem) o;
        if (raceLocationItem.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), raceLocationItem.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "RaceLocationItem{" +
            "id=" + getId() +
            "}";
    }
}
