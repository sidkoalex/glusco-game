package com.glusco.app.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import java.time.Instant;
import java.time.temporal.ChronoUnit;

@Entity
@Data
@EqualsAndHashCode(of = "id")
@org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@NoArgsConstructor
@AllArgsConstructor
public class GameRaceStat {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    private Long metersCount;

    private Instant createdAt = Instant.now().truncatedTo(ChronoUnit.SECONDS);

    @ManyToOne(fetch = FetchType.EAGER)
    private User user;

    public void setCreatedAt(Instant createdAt) {
        this.createdAt = createdAt != null
            ? createdAt.truncatedTo(ChronoUnit.SECONDS)
            : Instant.now().truncatedTo(ChronoUnit.SECONDS);
    }
}
