package com.glusco.app.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.Objects;

/**
 * A RaceReward.
 */
@Entity
@Table(name = "race_reward")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class RaceReward implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "reward_qr")
    private String rewardQr;

    @ManyToOne(optional = false)
    @NotNull
    @JsonIgnoreProperties("")
    private Race race;

    @ManyToOne(optional = false)
    @NotNull
    @JsonIgnoreProperties("")
    private Reward reward;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getRewardQr() {
        return rewardQr;
    }

    public RaceReward rewardQr(String rewardQr) {
        this.rewardQr = rewardQr;
        return this;
    }

    public void setRewardQr(String rewardQr) {
        this.rewardQr = rewardQr;
    }

    public Race getRace() {
        return race;
    }

    public RaceReward race(Race race) {
        this.race = race;
        return this;
    }

    public void setRace(Race race) {
        this.race = race;
    }

    public Reward getReward() {
        return reward;
    }

    public RaceReward reward(Reward reward) {
        this.reward = reward;
        return this;
    }

    public void setReward(Reward reward) {
        this.reward = reward;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        RaceReward raceReward = (RaceReward) o;
        if (raceReward.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), raceReward.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "RaceReward{" +
            "id=" + getId() +
            ", rewardQr='" + getRewardQr() + "'" +
            "}";
    }
}
