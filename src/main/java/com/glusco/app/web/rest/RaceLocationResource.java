package com.glusco.app.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.glusco.app.service.RaceLocationService;
import com.glusco.app.web.rest.errors.BadRequestAlertException;
import com.glusco.app.web.rest.util.HeaderUtil;
import com.glusco.app.web.rest.util.PaginationUtil;
import com.glusco.app.service.dto.RaceLocationDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing RaceLocation.
 */
@RestController
@RequestMapping("/api")
public class RaceLocationResource {

    private final Logger log = LoggerFactory.getLogger(RaceLocationResource.class);

    private static final String ENTITY_NAME = "raceLocation";

    private final RaceLocationService raceLocationService;

    public RaceLocationResource(RaceLocationService raceLocationService) {
        this.raceLocationService = raceLocationService;
    }

    /**
     * POST  /race-locations : Create a new raceLocation.
     *
     * @param raceLocationDTO the raceLocationDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new raceLocationDTO, or with status 400 (Bad Request) if the raceLocation has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/race-locations")
    @Timed
    public ResponseEntity<RaceLocationDTO> createRaceLocation(@Valid @RequestBody RaceLocationDTO raceLocationDTO) throws URISyntaxException {
        log.debug("REST request to save RaceLocation : {}", raceLocationDTO);
        if (raceLocationDTO.getId() != null) {
            throw new BadRequestAlertException("A new raceLocation cannot already have an ID", ENTITY_NAME, "idexists");
        }
        RaceLocationDTO result = raceLocationService.save(raceLocationDTO);
        return ResponseEntity.created(new URI("/api/race-locations/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /race-locations : Updates an existing raceLocation.
     *
     * @param raceLocationDTO the raceLocationDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated raceLocationDTO,
     * or with status 400 (Bad Request) if the raceLocationDTO is not valid,
     * or with status 500 (Internal Server Error) if the raceLocationDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/race-locations")
    @Timed
    public ResponseEntity<RaceLocationDTO> updateRaceLocation(@Valid @RequestBody RaceLocationDTO raceLocationDTO) throws URISyntaxException {
        log.debug("REST request to update RaceLocation : {}", raceLocationDTO);
        if (raceLocationDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        RaceLocationDTO result = raceLocationService.save(raceLocationDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, raceLocationDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /race-locations : get all the raceLocations.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of raceLocations in body
     */
    @GetMapping("/race-locations")
    @Timed
    public ResponseEntity<List<RaceLocationDTO>> getAllRaceLocations(Pageable pageable) {
        log.debug("REST request to get a page of RaceLocations");
        Page<RaceLocationDTO> page = raceLocationService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/race-locations");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /race-locations/:id : get the "id" raceLocation.
     *
     * @param id the id of the raceLocationDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the raceLocationDTO, or with status 404 (Not Found)
     */
    @GetMapping("/race-locations/{id}")
    @Timed
    public ResponseEntity<RaceLocationDTO> getRaceLocation(@PathVariable Long id) {
        log.debug("REST request to get RaceLocation : {}", id);
        Optional<RaceLocationDTO> raceLocationDTO = raceLocationService.findOne(id);
        return ResponseUtil.wrapOrNotFound(raceLocationDTO);
    }

    /**
     * DELETE  /race-locations/:id : delete the "id" raceLocation.
     *
     * @param id the id of the raceLocationDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/race-locations/{id}")
    @Timed
    public ResponseEntity<Void> deleteRaceLocation(@PathVariable Long id) {
        log.debug("REST request to delete RaceLocation : {}", id);
        raceLocationService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
