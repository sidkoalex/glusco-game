package com.glusco.app.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.glusco.app.service.PersonCarService;
import com.glusco.app.web.rest.errors.BadRequestAlertException;
import com.glusco.app.web.rest.util.HeaderUtil;
import com.glusco.app.web.rest.util.PaginationUtil;
import com.glusco.app.service.dto.PersonCarDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing PersonCar.
 */
@RestController
@RequestMapping("/api")
public class PersonCarResource {

    private final Logger log = LoggerFactory.getLogger(PersonCarResource.class);

    private static final String ENTITY_NAME = "personCar";

    private final PersonCarService personCarService;

    public PersonCarResource(PersonCarService personCarService) {
        this.personCarService = personCarService;
    }

    /**
     * POST  /person-cars : Create a new personCar.
     *
     * @param personCarDTO the personCarDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new personCarDTO, or with status 400 (Bad Request) if the personCar has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/person-cars")
    @Timed
    public ResponseEntity<PersonCarDTO> createPersonCar(@Valid @RequestBody PersonCarDTO personCarDTO) throws URISyntaxException {
        log.debug("REST request to save PersonCar : {}", personCarDTO);
        if (personCarDTO.getId() != null) {
            throw new BadRequestAlertException("A new personCar cannot already have an ID", ENTITY_NAME, "idexists");
        }
        PersonCarDTO result = personCarService.save(personCarDTO);
        return ResponseEntity.created(new URI("/api/person-cars/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /person-cars : Updates an existing personCar.
     *
     * @param personCarDTO the personCarDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated personCarDTO,
     * or with status 400 (Bad Request) if the personCarDTO is not valid,
     * or with status 500 (Internal Server Error) if the personCarDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/person-cars")
    @Timed
    public ResponseEntity<PersonCarDTO> updatePersonCar(@Valid @RequestBody PersonCarDTO personCarDTO) throws URISyntaxException {
        log.debug("REST request to update PersonCar : {}", personCarDTO);
        if (personCarDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        PersonCarDTO result = personCarService.save(personCarDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, personCarDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /person-cars : get all the personCars.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of personCars in body
     */
    @GetMapping("/person-cars")
    @Timed
    public ResponseEntity<List<PersonCarDTO>> getAllPersonCars(Pageable pageable) {
        log.debug("REST request to get a page of PersonCars");
        Page<PersonCarDTO> page = personCarService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/person-cars");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /person-cars/:id : get the "id" personCar.
     *
     * @param id the id of the personCarDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the personCarDTO, or with status 404 (Not Found)
     */
    @GetMapping("/person-cars/{id}")
    @Timed
    public ResponseEntity<PersonCarDTO> getPersonCar(@PathVariable Long id) {
        log.debug("REST request to get PersonCar : {}", id);
        Optional<PersonCarDTO> personCarDTO = personCarService.findOne(id);
        return ResponseUtil.wrapOrNotFound(personCarDTO);
    }

    /**
     * DELETE  /person-cars/:id : delete the "id" personCar.
     *
     * @param id the id of the personCarDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/person-cars/{id}")
    @Timed
    public ResponseEntity<Void> deletePersonCar(@PathVariable Long id) {
        log.debug("REST request to delete PersonCar : {}", id);
        personCarService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
