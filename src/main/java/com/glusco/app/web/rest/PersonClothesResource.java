package com.glusco.app.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.glusco.app.service.PersonClothesService;
import com.glusco.app.web.rest.errors.BadRequestAlertException;
import com.glusco.app.web.rest.util.HeaderUtil;
import com.glusco.app.web.rest.util.PaginationUtil;
import com.glusco.app.service.dto.PersonClothesDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing PersonClothes.
 */
@RestController
@RequestMapping("/api")
public class PersonClothesResource {

    private final Logger log = LoggerFactory.getLogger(PersonClothesResource.class);

    private static final String ENTITY_NAME = "personClothes";

    private final PersonClothesService personClothesService;

    public PersonClothesResource(PersonClothesService personClothesService) {
        this.personClothesService = personClothesService;
    }

    /**
     * POST  /person-clothes : Create a new personClothes.
     *
     * @param personClothesDTO the personClothesDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new personClothesDTO, or with status 400 (Bad Request) if the personClothes has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/person-clothes")
    @Timed
    public ResponseEntity<PersonClothesDTO> createPersonClothes(@Valid @RequestBody PersonClothesDTO personClothesDTO) throws URISyntaxException {
        log.debug("REST request to save PersonClothes : {}", personClothesDTO);
        if (personClothesDTO.getId() != null) {
            throw new BadRequestAlertException("A new personClothes cannot already have an ID", ENTITY_NAME, "idexists");
        }
        PersonClothesDTO result = personClothesService.save(personClothesDTO);
        return ResponseEntity.created(new URI("/api/person-clothes/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /person-clothes : Updates an existing personClothes.
     *
     * @param personClothesDTO the personClothesDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated personClothesDTO,
     * or with status 400 (Bad Request) if the personClothesDTO is not valid,
     * or with status 500 (Internal Server Error) if the personClothesDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/person-clothes")
    @Timed
    public ResponseEntity<PersonClothesDTO> updatePersonClothes(@Valid @RequestBody PersonClothesDTO personClothesDTO) throws URISyntaxException {
        log.debug("REST request to update PersonClothes : {}", personClothesDTO);
        if (personClothesDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        PersonClothesDTO result = personClothesService.save(personClothesDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, personClothesDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /person-clothes : get all the personClothes.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of personClothes in body
     */
    @GetMapping("/person-clothes")
    @Timed
    public ResponseEntity<List<PersonClothesDTO>> getAllPersonClothes(Pageable pageable) {
        log.debug("REST request to get a page of PersonClothes");
        Page<PersonClothesDTO> page = personClothesService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/person-clothes");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /person-clothes/:id : get the "id" personClothes.
     *
     * @param id the id of the personClothesDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the personClothesDTO, or with status 404 (Not Found)
     */
    @GetMapping("/person-clothes/{id}")
    @Timed
    public ResponseEntity<PersonClothesDTO> getPersonClothes(@PathVariable Long id) {
        log.debug("REST request to get PersonClothes : {}", id);
        Optional<PersonClothesDTO> personClothesDTO = personClothesService.findOne(id);
        return ResponseUtil.wrapOrNotFound(personClothesDTO);
    }

    /**
     * DELETE  /person-clothes/:id : delete the "id" personClothes.
     *
     * @param id the id of the personClothesDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/person-clothes/{id}")
    @Timed
    public ResponseEntity<Void> deletePersonClothes(@PathVariable Long id) {
        log.debug("REST request to delete PersonClothes : {}", id);
        personClothesService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
