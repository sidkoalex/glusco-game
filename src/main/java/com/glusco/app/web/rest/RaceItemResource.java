package com.glusco.app.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.glusco.app.service.RaceItemService;
import com.glusco.app.web.rest.errors.BadRequestAlertException;
import com.glusco.app.web.rest.util.HeaderUtil;
import com.glusco.app.web.rest.util.PaginationUtil;
import com.glusco.app.service.dto.RaceItemDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing RaceItem.
 */
@RestController
@RequestMapping("/api")
public class RaceItemResource {

    private final Logger log = LoggerFactory.getLogger(RaceItemResource.class);

    private static final String ENTITY_NAME = "raceItem";

    private final RaceItemService raceItemService;

    public RaceItemResource(RaceItemService raceItemService) {
        this.raceItemService = raceItemService;
    }

    /**
     * POST  /race-items : Create a new raceItem.
     *
     * @param raceItemDTO the raceItemDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new raceItemDTO, or with status 400 (Bad Request) if the raceItem has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/race-items")
    @Timed
    public ResponseEntity<RaceItemDTO> createRaceItem(@Valid @RequestBody RaceItemDTO raceItemDTO) throws URISyntaxException {
        log.debug("REST request to save RaceItem : {}", raceItemDTO);
        if (raceItemDTO.getId() != null) {
            throw new BadRequestAlertException("A new raceItem cannot already have an ID", ENTITY_NAME, "idexists");
        }
        RaceItemDTO result = raceItemService.save(raceItemDTO);
        return ResponseEntity.created(new URI("/api/race-items/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /race-items : Updates an existing raceItem.
     *
     * @param raceItemDTO the raceItemDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated raceItemDTO,
     * or with status 400 (Bad Request) if the raceItemDTO is not valid,
     * or with status 500 (Internal Server Error) if the raceItemDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/race-items")
    @Timed
    public ResponseEntity<RaceItemDTO> updateRaceItem(@Valid @RequestBody RaceItemDTO raceItemDTO) throws URISyntaxException {
        log.debug("REST request to update RaceItem : {}", raceItemDTO);
        if (raceItemDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        RaceItemDTO result = raceItemService.save(raceItemDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, raceItemDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /race-items : get all the raceItems.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of raceItems in body
     */
    @GetMapping("/race-items")
    @Timed
    public ResponseEntity<List<RaceItemDTO>> getAllRaceItems(Pageable pageable) {
        log.debug("REST request to get a page of RaceItems");
        Page<RaceItemDTO> page = raceItemService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/race-items");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /race-items/:id : get the "id" raceItem.
     *
     * @param id the id of the raceItemDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the raceItemDTO, or with status 404 (Not Found)
     */
    @GetMapping("/race-items/{id}")
    @Timed
    public ResponseEntity<RaceItemDTO> getRaceItem(@PathVariable Long id) {
        log.debug("REST request to get RaceItem : {}", id);
        Optional<RaceItemDTO> raceItemDTO = raceItemService.findOne(id);
        return ResponseUtil.wrapOrNotFound(raceItemDTO);
    }

    /**
     * DELETE  /race-items/:id : delete the "id" raceItem.
     *
     * @param id the id of the raceItemDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/race-items/{id}")
    @Timed
    public ResponseEntity<Void> deleteRaceItem(@PathVariable Long id) {
        log.debug("REST request to delete RaceItem : {}", id);
        raceItemService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
