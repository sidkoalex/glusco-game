package com.glusco.app.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.glusco.app.service.RewardService;
import com.glusco.app.web.rest.errors.BadRequestAlertException;
import com.glusco.app.web.rest.util.HeaderUtil;
import com.glusco.app.web.rest.util.PaginationUtil;
import com.glusco.app.service.dto.RewardDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing Reward.
 */
@RestController
@RequestMapping("/api")
public class RewardResource {

    private final Logger log = LoggerFactory.getLogger(RewardResource.class);

    private static final String ENTITY_NAME = "reward";

    private final RewardService rewardService;

    public RewardResource(RewardService rewardService) {
        this.rewardService = rewardService;
    }

    /**
     * POST  /rewards : Create a new reward.
     *
     * @param rewardDTO the rewardDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new rewardDTO, or with status 400 (Bad Request) if the reward has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/rewards")
    @Timed
    public ResponseEntity<RewardDTO> createReward(@Valid @RequestBody RewardDTO rewardDTO) throws URISyntaxException {
        log.debug("REST request to save Reward : {}", rewardDTO);
        if (rewardDTO.getId() != null) {
            throw new BadRequestAlertException("A new reward cannot already have an ID", ENTITY_NAME, "idexists");
        }
        RewardDTO result = rewardService.save(rewardDTO);
        return ResponseEntity.created(new URI("/api/rewards/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /rewards : Updates an existing reward.
     *
     * @param rewardDTO the rewardDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated rewardDTO,
     * or with status 400 (Bad Request) if the rewardDTO is not valid,
     * or with status 500 (Internal Server Error) if the rewardDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/rewards")
    @Timed
    public ResponseEntity<RewardDTO> updateReward(@Valid @RequestBody RewardDTO rewardDTO) throws URISyntaxException {
        log.debug("REST request to update Reward : {}", rewardDTO);
        if (rewardDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        RewardDTO result = rewardService.save(rewardDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, rewardDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /rewards : get all the rewards.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of rewards in body
     */
    @GetMapping("/rewards")
    @Timed
    public ResponseEntity<List<RewardDTO>> getAllRewards(Pageable pageable) {
        log.debug("REST request to get a page of Rewards");
        Page<RewardDTO> page = rewardService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/rewards");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /rewards/:id : get the "id" reward.
     *
     * @param id the id of the rewardDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the rewardDTO, or with status 404 (Not Found)
     */
    @GetMapping("/rewards/{id}")
    @Timed
    public ResponseEntity<RewardDTO> getReward(@PathVariable Long id) {
        log.debug("REST request to get Reward : {}", id);
        Optional<RewardDTO> rewardDTO = rewardService.findOne(id);
        return ResponseUtil.wrapOrNotFound(rewardDTO);
    }

    /**
     * DELETE  /rewards/:id : delete the "id" reward.
     *
     * @param id the id of the rewardDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/rewards/{id}")
    @Timed
    public ResponseEntity<Void> deleteReward(@PathVariable Long id) {
        log.debug("REST request to delete Reward : {}", id);
        rewardService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
