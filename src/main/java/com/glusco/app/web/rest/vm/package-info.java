/**
 * View Models used by Spring MVC REST controllers.
 */
package com.glusco.app.web.rest.vm;
