package com.glusco.app.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.glusco.app.service.RaceRewardService;
import com.glusco.app.web.rest.errors.BadRequestAlertException;
import com.glusco.app.web.rest.util.HeaderUtil;
import com.glusco.app.web.rest.util.PaginationUtil;
import com.glusco.app.service.dto.RaceRewardDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing RaceReward.
 */
@RestController
@RequestMapping("/api")
public class RaceRewardResource {

    private final Logger log = LoggerFactory.getLogger(RaceRewardResource.class);

    private static final String ENTITY_NAME = "raceReward";

    private final RaceRewardService raceRewardService;

    public RaceRewardResource(RaceRewardService raceRewardService) {
        this.raceRewardService = raceRewardService;
    }

    /**
     * POST  /race-rewards : Create a new raceReward.
     *
     * @param raceRewardDTO the raceRewardDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new raceRewardDTO, or with status 400 (Bad Request) if the raceReward has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/race-rewards")
    @Timed
    public ResponseEntity<RaceRewardDTO> createRaceReward(@Valid @RequestBody RaceRewardDTO raceRewardDTO) throws URISyntaxException {
        log.debug("REST request to save RaceReward : {}", raceRewardDTO);
        if (raceRewardDTO.getId() != null) {
            throw new BadRequestAlertException("A new raceReward cannot already have an ID", ENTITY_NAME, "idexists");
        }
        RaceRewardDTO result = raceRewardService.save(raceRewardDTO);
        return ResponseEntity.created(new URI("/api/race-rewards/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /race-rewards : Updates an existing raceReward.
     *
     * @param raceRewardDTO the raceRewardDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated raceRewardDTO,
     * or with status 400 (Bad Request) if the raceRewardDTO is not valid,
     * or with status 500 (Internal Server Error) if the raceRewardDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/race-rewards")
    @Timed
    public ResponseEntity<RaceRewardDTO> updateRaceReward(@Valid @RequestBody RaceRewardDTO raceRewardDTO) throws URISyntaxException {
        log.debug("REST request to update RaceReward : {}", raceRewardDTO);
        if (raceRewardDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        RaceRewardDTO result = raceRewardService.save(raceRewardDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, raceRewardDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /race-rewards : get all the raceRewards.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of raceRewards in body
     */
    @GetMapping("/race-rewards")
    @Timed
    public ResponseEntity<List<RaceRewardDTO>> getAllRaceRewards(Pageable pageable) {
        log.debug("REST request to get a page of RaceRewards");
        Page<RaceRewardDTO> page = raceRewardService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/race-rewards");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /race-rewards/:id : get the "id" raceReward.
     *
     * @param id the id of the raceRewardDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the raceRewardDTO, or with status 404 (Not Found)
     */
    @GetMapping("/race-rewards/{id}")
    @Timed
    public ResponseEntity<RaceRewardDTO> getRaceReward(@PathVariable Long id) {
        log.debug("REST request to get RaceReward : {}", id);
        Optional<RaceRewardDTO> raceRewardDTO = raceRewardService.findOne(id);
        return ResponseUtil.wrapOrNotFound(raceRewardDTO);
    }

    /**
     * DELETE  /race-rewards/:id : delete the "id" raceReward.
     *
     * @param id the id of the raceRewardDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/race-rewards/{id}")
    @Timed
    public ResponseEntity<Void> deleteRaceReward(@PathVariable Long id) {
        log.debug("REST request to delete RaceReward : {}", id);
        raceRewardService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
