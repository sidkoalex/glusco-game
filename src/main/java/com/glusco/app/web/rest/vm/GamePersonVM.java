package com.glusco.app.web.rest.vm;

import lombok.Data;

@Data
public class GamePersonVM {
    private String personType;

    private String personActiveSkin;

    private String personCar;

    private Float[] personActiveCarColor;
}
