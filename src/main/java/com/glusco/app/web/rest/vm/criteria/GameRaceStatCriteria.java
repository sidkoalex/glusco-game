package com.glusco.app.web.rest.vm.criteria;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.github.jhipster.service.filter.InstantFilter;
import io.github.jhipster.service.filter.LongFilter;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class GameRaceStatCriteria {
    @JsonIgnore
    private LongFilter userId;
    private InstantFilter createdAt;
}
