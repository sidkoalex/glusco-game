package com.glusco.app.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.glusco.app.service.UserCarSetService;
import com.glusco.app.service.dto.UserCarSetDTO;
import com.glusco.app.web.rest.errors.BadRequestAlertException;
import com.glusco.app.web.rest.util.HeaderUtil;
import com.glusco.app.web.rest.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import lombok.val;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

import static com.glusco.app.security.SecurityUtils.getCurrentUserId;

/**
 * REST controller for managing UserCarSet.
 */
@RestController
@RequestMapping("/api")
@RequiredArgsConstructor
@Log4j2
public class UserCarSetResource {

    private static final String ENTITY_NAME = "userCarSet";

    private final UserCarSetService userCarSetService;

    /**
     * POST  /user-car-sets : Create a new userCarSet.
     *
     * @param userCarSetDTO the userCarSetDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new userCarSetDTO, or with status 400 (Bad Request) if the userCarSet has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/user-car-sets")
    @Timed
    public ResponseEntity<UserCarSetDTO> createUserCarSet(@Valid @RequestBody UserCarSetDTO userCarSetDTO) throws URISyntaxException {
        log.debug("REST request to save UserCarSet : {}", userCarSetDTO);

        if (userCarSetDTO.getId() != null)
            throw new BadRequestAlertException("A new userCarSet cannot already have an ID", ENTITY_NAME, "idexists");

        val result = userCarSetService.save(userCarSetDTO);

        return ResponseEntity.created(new URI("/api/user-car-sets/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /user-car-sets : Updates an existing userCarSet.
     *
     * @param userCarSetDTO the userCarSetDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated userCarSetDTO,
     * or with status 400 (Bad Request) if the userCarSetDTO is not valid,
     * or with status 500 (Internal Server Error) if the userCarSetDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/user-car-sets")
    @Timed
    public ResponseEntity<UserCarSetDTO> updateUserCarSet(@Valid @RequestBody UserCarSetDTO userCarSetDTO) throws URISyntaxException {
        log.debug("REST request to update UserCarSet : {}", userCarSetDTO);

        if (userCarSetDTO.getId() == null)
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");

        val result = userCarSetService.save(userCarSetDTO);

        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, userCarSetDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /user-car-sets : get all the userCarSets.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of userCarSets in body
     */
    @GetMapping("/user-car-sets")
    @Timed
    public ResponseEntity<List<UserCarSetDTO>> getAllUserCarSets(Pageable pageable) {
        log.debug("REST request to get a page of UserCarSets");

        val page = userCarSetService.findAll(getCurrentUserId(), pageable);
        val headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/user-car-sets");

        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /user-car-sets/:id : get the "id" userCarSet.
     *
     * @param id the id of the userCarSetDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the userCarSetDTO, or with status 404 (Not Found)
     */
    @GetMapping("/user-car-sets/{id}")
    @Timed
    public ResponseEntity<UserCarSetDTO> getUserCarSet(@PathVariable Long id) {
        log.debug("REST request to get UserCarSet : {}", id);

        val userCarSetDTO = userCarSetService.findOne(id, getCurrentUserId());

        return ResponseUtil.wrapOrNotFound(userCarSetDTO);
    }

    /**
     * DELETE  /user-car-sets/:id : delete the "id" userCarSet.
     *
     * @param id the id of the userCarSetDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/user-car-sets/{id}")
    @Timed
    public ResponseEntity<Void> deleteUserCarSet(@PathVariable Long id) {
        log.debug("REST request to delete UserCarSet : {}", id);

        userCarSetService.delete(id, getCurrentUserId());

        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString()))
            .build();
    }
}
