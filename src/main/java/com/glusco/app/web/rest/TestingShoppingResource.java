package com.glusco.app.web.rest;

import com.glusco.app.entity.ShopStats;
import com.glusco.app.service.UserService;
import com.glusco.app.service.testing.TestingServiceImpl;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static com.glusco.app.security.SecurityUtils.getCurrentUserId;

/**
 * REST controller for test actions.
 */
@RestController
@RequestMapping("/api")
@RequiredArgsConstructor
@Log4j2
public class TestingShoppingResource {

    private final TestingServiceImpl testingService;
    private final UserService userService;

    /**
     * Add some liters, hotdogs, coffee to your account balance
     *
     * @return Info about added liters, hotdogs, coffee
     */
    @PostMapping("/testing/shopping/buy-random-goods")
    public ShopStats buyRandomGoods() {
        return testingService.doShopping(getCurrentUserId());
    }


    /**
     * Clean all counters: liters, hotdogs, coffee
     *
     * @return OK if success
     */
    @PostMapping("/testing/shopping/clean-buy-statistic")
    public ShopStats cleanBuyStatistic() {
        return userService.setShopStats(new ShopStats());
    }
}
