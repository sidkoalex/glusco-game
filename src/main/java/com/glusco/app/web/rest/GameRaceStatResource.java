package com.glusco.app.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.glusco.app.service.GameRaceStatService;
import com.glusco.app.service.dto.GameRaceStatDTO;
import com.glusco.app.web.rest.vm.GameRaceStatVM;
import com.glusco.app.web.rest.vm.criteria.GameRaceStatCriteria;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.SortDefault;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

import static com.glusco.app.security.SecurityUtils.getCurrentUserId;

/**
 * REST controller for managing RaceStatistic.
 */
@RestController
@RequiredArgsConstructor
@RequestMapping("/api/game")
@Log4j2
public class GameRaceStatResource {

    private final GameRaceStatService gameRaceStatService;

    @PostMapping("/races")
    @Timed
    public ResponseEntity<GameRaceStatDTO> saveRaceStatForCurrentUser(@Valid @RequestBody GameRaceStatVM gameRaceStatVM) {
        log.debug("REST request to save race statistic : {}", gameRaceStatVM);
        GameRaceStatDTO savedDTO = gameRaceStatService.save(gameRaceStatVM, getCurrentUserId());
        return ResponseEntity.ok(savedDTO);
    }

    @GetMapping("/races")
    @Timed
    public ResponseEntity<List<GameRaceStatDTO>> getAllRaceStatForCurrentUser(GameRaceStatCriteria criteria, @SortDefault(sort = "createdAt", direction = Sort.Direction.DESC) Pageable pageable) {
        Page<GameRaceStatDTO> page = gameRaceStatService.findByCriteria(criteria, pageable, getCurrentUserId());
        return ResponseEntity.ok(page.getContent());
    }

    @GetMapping("/races/top/today")
    @Timed
    public ResponseEntity<List<GameRaceStatDTO>> getAllRacesTopForToday(Pageable pageable) {
        Page<GameRaceStatDTO> page = gameRaceStatService.findTopForToday(pageable);
        return ResponseEntity.ok(page.getContent());
    }

    @GetMapping("/races/top/month")
    @Timed
    public ResponseEntity<List<GameRaceStatDTO>> getAllRacesTopForMonth(Pageable pageable) {
        Page<GameRaceStatDTO> page = gameRaceStatService.findTopForMonth(null, pageable);
        return ResponseEntity.ok(page.getContent());
    }


    @GetMapping("/races/top/month/{monthNo}")
    @Timed
    public ResponseEntity<List<GameRaceStatDTO>> getAllRacesTopForMonth(@PathVariable Integer monthNo, Pageable pageable) {
        Page<GameRaceStatDTO> page = gameRaceStatService.findTopForMonth(monthNo, pageable);
        return ResponseEntity.ok(page.getContent());
    }

}

