package com.glusco.app.web.rest.vm;

import lombok.Data;

@Data
public class GameRaceStatVM {
    private Long metersCount;
}
