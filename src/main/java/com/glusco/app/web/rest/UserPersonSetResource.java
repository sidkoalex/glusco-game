package com.glusco.app.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.glusco.app.service.UserPersonSetService;
import com.glusco.app.web.rest.errors.BadRequestAlertException;
import com.glusco.app.web.rest.util.HeaderUtil;
import com.glusco.app.web.rest.util.PaginationUtil;
import com.glusco.app.service.dto.UserPersonSetDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

import static com.glusco.app.security.SecurityUtils.getCurrentUserId;

/**
 * REST controller for managing UserPersonSet.
 */
@RestController
@RequestMapping("/api")
public class UserPersonSetResource {

    private final Logger log = LoggerFactory.getLogger(UserPersonSetResource.class);

    private static final String ENTITY_NAME = "userPersonSet";

    private final UserPersonSetService userPersonSetService;

    public UserPersonSetResource(UserPersonSetService userPersonSetService) {
        this.userPersonSetService = userPersonSetService;
    }

    /**
     * POST  /user-person-sets : Create a new userPersonSet.
     *
     * @param userPersonSetDTO the userPersonSetDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new userPersonSetDTO, or with status 400 (Bad Request) if the userPersonSet has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/user-person-sets")
    @Timed
    public ResponseEntity<UserPersonSetDTO> createUserPersonSet(@Valid @RequestBody UserPersonSetDTO userPersonSetDTO) throws URISyntaxException {
        log.debug("REST request to save UserPersonSet : {}", userPersonSetDTO);
        if (userPersonSetDTO.getId() != null) {
            throw new BadRequestAlertException("A new userPersonSet cannot already have an ID", ENTITY_NAME, "idexists");
        }
        UserPersonSetDTO result = userPersonSetService.save(userPersonSetDTO);
        return ResponseEntity.created(new URI("/api/user-person-sets/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /user-person-sets : Updates an existing userPersonSet.
     *
     * @param userPersonSetDTO the userPersonSetDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated userPersonSetDTO,
     * or with status 400 (Bad Request) if the userPersonSetDTO is not valid,
     * or with status 500 (Internal Server Error) if the userPersonSetDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/user-person-sets")
    @Timed
    public ResponseEntity<UserPersonSetDTO> updateUserPersonSet(@Valid @RequestBody UserPersonSetDTO userPersonSetDTO) throws URISyntaxException {
        log.debug("REST request to update UserPersonSet : {}", userPersonSetDTO);
        if (userPersonSetDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        UserPersonSetDTO result = userPersonSetService.save(userPersonSetDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, userPersonSetDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /user-person-sets : get all the userPersonSets.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of userPersonSets in body
     */
    @GetMapping("/user-person-sets")
    @Timed
    public ResponseEntity<List<UserPersonSetDTO>> getAllUserPersonSets(Pageable pageable) {
        log.debug("REST request to get a page of UserPersonSets");
        Page<UserPersonSetDTO> page = userPersonSetService.findAllByUserId(getCurrentUserId(), pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/user-person-sets");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /user-person-sets/:id : get the "id" userPersonSet.
     *
     * @param id the id of the userPersonSetDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the userPersonSetDTO, or with status 404 (Not Found)
     */
    @GetMapping("/user-person-sets/{id}")
    @Timed
    public ResponseEntity<UserPersonSetDTO> getUserPersonSet(@PathVariable Long id) {
        log.debug("REST request to get UserPersonSet : {}", id);
        Optional<UserPersonSetDTO> userPersonSetDTO = userPersonSetService.findOneByUserId(id, getCurrentUserId());
        return ResponseUtil.wrapOrNotFound(userPersonSetDTO);
    }

    /**
     * DELETE  /user-person-sets/:id : delete the "id" userPersonSet.
     *
     * @param id the id of the userPersonSetDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/user-person-sets/{id}")
    @Timed
    public ResponseEntity<Void> deleteUserPersonSet(@PathVariable Long id) {
        log.debug("REST request to delete UserPersonSet : {}", id);
        userPersonSetService.deleteOneByUserId(id, getCurrentUserId());
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
