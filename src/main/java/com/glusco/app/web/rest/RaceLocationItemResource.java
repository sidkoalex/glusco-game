package com.glusco.app.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.glusco.app.service.RaceLocationItemService;
import com.glusco.app.web.rest.errors.BadRequestAlertException;
import com.glusco.app.web.rest.util.HeaderUtil;
import com.glusco.app.web.rest.util.PaginationUtil;
import com.glusco.app.service.dto.RaceLocationItemDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing RaceLocationItem.
 */
@RestController
@RequestMapping("/api")
public class RaceLocationItemResource {

    private final Logger log = LoggerFactory.getLogger(RaceLocationItemResource.class);

    private static final String ENTITY_NAME = "raceLocationItem";

    private final RaceLocationItemService raceLocationItemService;

    public RaceLocationItemResource(RaceLocationItemService raceLocationItemService) {
        this.raceLocationItemService = raceLocationItemService;
    }

    /**
     * POST  /race-location-items : Create a new raceLocationItem.
     *
     * @param raceLocationItemDTO the raceLocationItemDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new raceLocationItemDTO, or with status 400 (Bad Request) if the raceLocationItem has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/race-location-items")
    @Timed
    public ResponseEntity<RaceLocationItemDTO> createRaceLocationItem(@Valid @RequestBody RaceLocationItemDTO raceLocationItemDTO) throws URISyntaxException {
        log.debug("REST request to save RaceLocationItem : {}", raceLocationItemDTO);
        if (raceLocationItemDTO.getId() != null) {
            throw new BadRequestAlertException("A new raceLocationItem cannot already have an ID", ENTITY_NAME, "idexists");
        }
        RaceLocationItemDTO result = raceLocationItemService.save(raceLocationItemDTO);
        return ResponseEntity.created(new URI("/api/race-location-items/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /race-location-items : Updates an existing raceLocationItem.
     *
     * @param raceLocationItemDTO the raceLocationItemDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated raceLocationItemDTO,
     * or with status 400 (Bad Request) if the raceLocationItemDTO is not valid,
     * or with status 500 (Internal Server Error) if the raceLocationItemDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/race-location-items")
    @Timed
    public ResponseEntity<RaceLocationItemDTO> updateRaceLocationItem(@Valid @RequestBody RaceLocationItemDTO raceLocationItemDTO) throws URISyntaxException {
        log.debug("REST request to update RaceLocationItem : {}", raceLocationItemDTO);
        if (raceLocationItemDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        RaceLocationItemDTO result = raceLocationItemService.save(raceLocationItemDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, raceLocationItemDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /race-location-items : get all the raceLocationItems.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of raceLocationItems in body
     */
    @GetMapping("/race-location-items")
    @Timed
    public ResponseEntity<List<RaceLocationItemDTO>> getAllRaceLocationItems(Pageable pageable) {
        log.debug("REST request to get a page of RaceLocationItems");
        Page<RaceLocationItemDTO> page = raceLocationItemService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/race-location-items");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /race-location-items/:id : get the "id" raceLocationItem.
     *
     * @param id the id of the raceLocationItemDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the raceLocationItemDTO, or with status 404 (Not Found)
     */
    @GetMapping("/race-location-items/{id}")
    @Timed
    public ResponseEntity<RaceLocationItemDTO> getRaceLocationItem(@PathVariable Long id) {
        log.debug("REST request to get RaceLocationItem : {}", id);
        Optional<RaceLocationItemDTO> raceLocationItemDTO = raceLocationItemService.findOne(id);
        return ResponseUtil.wrapOrNotFound(raceLocationItemDTO);
    }

    /**
     * DELETE  /race-location-items/:id : delete the "id" raceLocationItem.
     *
     * @param id the id of the raceLocationItemDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/race-location-items/{id}")
    @Timed
    public ResponseEntity<Void> deleteRaceLocationItem(@PathVariable Long id) {
        log.debug("REST request to delete RaceLocationItem : {}", id);
        raceLocationItemService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
