package com.glusco.app.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.glusco.app.service.GamePersonService;
import com.glusco.app.service.dto.GamePersonDTO;
import com.glusco.app.web.rest.vm.GamePersonVM;
import io.github.jhipster.web.util.ResponseUtil;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

import static com.glusco.app.security.SecurityUtils.getCurrentUserId;

/**
 * REST controller for managing RaceStatistic.
 */
@RestController
@RequiredArgsConstructor
@RequestMapping("/api/game")
@Log4j2
public class GamePersonResource {

    private final GamePersonService gamePersonService;

    @PostMapping("/persons")
    @Timed
    public ResponseEntity<GamePersonDTO> saveGamePerson(@Valid @RequestBody GamePersonVM gamePersonVM) {
        log.debug("REST request to save game person: {}", gamePersonVM);
        GamePersonDTO savedDTO = gamePersonService.savePerson(gamePersonVM, getCurrentUserId());
        return ResponseEntity.ok(savedDTO);
    }

    @GetMapping("/persons/{personType}")
    @Timed
    public ResponseEntity<GamePersonDTO> getGamePerson(@PathVariable String personType) {
        Long currentUserId = getCurrentUserId();
        log.debug("REST request to get game person for user: {}", currentUserId);
        Optional<GamePersonDTO> dto = gamePersonService.getPerson(currentUserId, personType);
        return ResponseUtil.wrapOrNotFound(dto);
    }

    @GetMapping("/persons")
    @Timed
    public ResponseEntity<List<GamePersonDTO>> getGamePersons() {
        Long currentUserId = getCurrentUserId();
        log.debug("REST request to get all game persons for user: {}", currentUserId);
        List<GamePersonDTO> dtos = gamePersonService.getAllPersons(currentUserId);
        return ResponseEntity.ok(dtos);
    }

    @DeleteMapping("/persons/{personId}")
    @Timed
    public ResponseEntity<Void> deleteGamePersons(@PathVariable Long personId) {
        Long currentUserId = getCurrentUserId();
        log.debug("REST request to delete person for user: {}", currentUserId);
        gamePersonService.deletePerson(personId, currentUserId);
        return ResponseEntity.ok().build();
    }
}

