/**
 * Data Access Objects used by WebSocket services.
 */
package com.glusco.app.web.websocket.dto;
