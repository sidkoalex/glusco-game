package com.glusco.app.config;

import io.github.jhipster.config.JHipsterProperties;
import io.github.jhipster.config.jcache.BeanClassLoaderAwareJCacheRegionFactory;
import org.ehcache.config.builders.CacheConfigurationBuilder;
import org.ehcache.config.builders.ExpiryPolicyBuilder;
import org.ehcache.config.builders.ResourcePoolsBuilder;
import org.ehcache.jsr107.Eh107Configuration;
import org.springframework.boot.autoconfigure.cache.JCacheManagerCustomizer;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.time.Duration;

@Configuration
@EnableCaching
public class CacheConfiguration {

    private final javax.cache.configuration.Configuration<Object, Object> jcacheConfiguration;

    public CacheConfiguration(JHipsterProperties jHipsterProperties) {
        BeanClassLoaderAwareJCacheRegionFactory.setBeanClassLoader(this.getClass().getClassLoader());
        JHipsterProperties.Cache.Ehcache ehcache =
            jHipsterProperties.getCache().getEhcache();

        jcacheConfiguration = Eh107Configuration.fromEhcacheCacheConfiguration(
            CacheConfigurationBuilder.newCacheConfigurationBuilder(Object.class, Object.class,
                ResourcePoolsBuilder.heap(ehcache.getMaxEntries()))
                .withExpiry(ExpiryPolicyBuilder.timeToLiveExpiration(Duration.ofSeconds(ehcache.getTimeToLiveSeconds())))
                .build());
    }

    @Bean
    public JCacheManagerCustomizer cacheManagerCustomizer() {
        return cm -> {
            cm.createCache(com.glusco.app.repository.UserRepository.USERS_BY_LOGIN_CACHE, jcacheConfiguration);
            cm.createCache(com.glusco.app.repository.UserRepository.USERS_BY_EMAIL_CACHE, jcacheConfiguration);
            cm.createCache(com.glusco.app.repository.UserRepository.USERS_BY_ID_CACHE, jcacheConfiguration);
            cm.createCache(com.glusco.app.domain.User.class.getName(), jcacheConfiguration);
            cm.createCache(com.glusco.app.domain.Authority.class.getName(), jcacheConfiguration);
            cm.createCache(com.glusco.app.domain.User.class.getName() + ".authorities", jcacheConfiguration);
            cm.createCache(com.glusco.app.domain.Clothes.class.getName(), jcacheConfiguration);
            cm.createCache(com.glusco.app.domain.Person.class.getName(), jcacheConfiguration);
            cm.createCache(com.glusco.app.domain.Reward.class.getName(), jcacheConfiguration);
            cm.createCache(com.glusco.app.domain.RaceLocation.class.getName(), jcacheConfiguration);
            cm.createCache(com.glusco.app.domain.RaceItem.class.getName(), jcacheConfiguration);
            cm.createCache(com.glusco.app.domain.RaceLocationItem.class.getName(), jcacheConfiguration);
            cm.createCache(com.glusco.app.domain.Car.class.getName(), jcacheConfiguration);
            cm.createCache(com.glusco.app.domain.PersonCar.class.getName(), jcacheConfiguration);
            cm.createCache(com.glusco.app.domain.PersonClothes.class.getName(), jcacheConfiguration);
            cm.createCache(com.glusco.app.domain.UserPersonSet.class.getName(), jcacheConfiguration);
            cm.createCache(com.glusco.app.domain.UserCarSet.class.getName(), jcacheConfiguration);
            cm.createCache(com.glusco.app.domain.Race.class.getName(), jcacheConfiguration);
            cm.createCache(com.glusco.app.domain.RaceReward.class.getName(), jcacheConfiguration);
            cm.createCache(com.glusco.app.domain.GameRaceStat.class.getName(), jcacheConfiguration);
            cm.createCache(com.glusco.app.domain.GamePerson.class.getName(), jcacheConfiguration);
            // jhipster-needle-ehcache-add-entry
        };
    }
}
