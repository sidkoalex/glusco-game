package com.glusco.app.entity;

import com.glusco.app.domain.User;
import lombok.Data;
import lombok.experimental.Accessors;
import lombok.extern.log4j.Log4j2;
import lombok.val;

import javax.validation.constraints.Size;
import java.util.Random;

@Data
@Accessors(chain = true)
@Log4j2
public class ShopStats {
    @Size(min = 0)
    private Integer litersCount = 0;

    @Size(min = 0)
    private Integer hotdogsCount = 0;

    @Size(min = 0)
    private Integer coffeeCount = 0;

    public static ShopStats createRandom() {
        val rand = new Random();
        return new ShopStats()
            .setLitersCount(rand.nextInt(10))
            .setCoffeeCount(rand.nextInt(2))
            .setHotdogsCount(rand.nextInt(2));
    }

    public void applyToUser(User user) {
        log.debug("Increase user shop stats (liters: +{}, hotdogs: +{}, coffee: +{})", litersCount, hotdogsCount, coffeeCount);
        user.setLitersCount(user.getLitersCount() + litersCount);
        user.setHotdogsCount(user.getHotdogsCount() + hotdogsCount);
        user.setCoffeeCount(user.getCoffeeCount() + coffeeCount);
    }

    public void setToUser(User user) {
        log.debug("Set user shop stats (liters: +{}, hotdogs: +{}, coffee: +{})", litersCount, hotdogsCount, coffeeCount);
        user.setLitersCount(litersCount);
        user.setHotdogsCount(hotdogsCount);
        user.setCoffeeCount(coffeeCount);
    }
}
